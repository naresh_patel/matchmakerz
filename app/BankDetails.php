<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BankDetails extends Model
{
    protected $table = 'bank_details';

    protected $fillable = ['matchmaker_id','bank_name','ifsc_code','account_number','account_name','paytm','upi'];

    public function getBankDetailsAttribute()
    {
    	return BankDetails::where('matchmaker_id', $this->id)->get();
    }
}
