<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlankLead extends Model
{
    protected $table = 'matchmaker_blanklead';

    public $timestamps = false;

    protected $fillable = ['phone_number', 'id_no', 'matchmaker', 'is_full', 'lead_type'];

    protected $attributes = [
    	'is_full' => false,
    	'lead_type' => 0,
    ];

    public function getIsFullAttribute($is_full)
    {
    	if($is_full == 1)
    		return true;
    	else
    		return false;
    }
}
