<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Caste extends Model
{
    protected $table = 'client_caste';
    protected $fillable = ['caste'];
}
