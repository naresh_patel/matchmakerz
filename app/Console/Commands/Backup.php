<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Process\Process;
use Illuminate\Support\Facades\Storage;

class Backup extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'backup:database';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to take backup of the database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $filename = 'Weekly_database_backup';

        $user = env('DB_USERNAME');
        $pass = env('DB_PASSWORD');
        $database = env('DB_DATABASE');
        $host = env('DB_HOST');

        $command = "mysqldump -u {$user} {$database} > {$filename}.sql";

        $process = new Process($command);
        $process->start();

        while($process->isRunning()){
            $s3 = \Storage::disk('s3');
            $s3->put('database_backup/'. $filename .'.sql', file_get_contents("{$filename}.sql"), 'public');
        }
        unlink($filename.'.sql');
    }
}
