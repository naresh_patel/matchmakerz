<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use Carbon\Carbon;
use App\ClientProfile;
use App\Preference;
use App\PreferenceCaste;
use App\Compatibility;
use App\Caste;
use App\MatchmakerUser;
use App\Degree;

class CompatibilityCalculation extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'calculate:compatibility';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Compatibility Calculation for clients';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      app('App\Http\Controllers\BotController')->generateAuthToken();
/*      $clients = ClientProfile::where('matchmaker_id', '!=', 82)->get();
      $clients_id = Compatibility::pluck('client_id')->toArray();
      $i = 1;
      foreach ($clients as $client) {
        if(!in_array($client->id, $clients_id)){
          echo "Client-Id: ".$i++."=>".$client->id."------------>";
          $this->calculate($client->id);
        }
      }

	
      $clients = ClientProfile::where('matchmaker_id', '!=', 82)->get();
      foreach($clients as $client) {
		echo "Running for client: ".$client->id."\n";
	      $this->calculate($client->id);
      }
*/    }

    public function calculate($id){
      $my_profile = ClientProfile::where('id', $id)->first();
      if($my_profile == null)
        return;

      $height=$my_profile->height;
      $last_id = (DB::table('compatibilities')->where('client_id', $my_profile->id)->first() == null)? 0 : DB::table('compatibilities')->where('client_id', $my_profile->id)->first()->last_id;
      $last_id = $last_id != null ? $last_id : 0;
      $birth_date = $my_profile->birth_date;
      $myPreferences = Preference::where('client_id', $id)->first();
      if($myPreferences == null)
        return;

      if(PreferenceCaste::where('preferences_id', $myPreferences->id)->exists())
			$my_profile->prefered_caste = PreferenceCaste::where('preferences_id', $myPreferences->id)->pluck('caste_id');
		else
			$my_profile->prefered_caste = explode(',', $myPreferences->caste_id);
      $my_profile->preferred_manglik = $myPreferences->manglik;
      $my_profile->preferred_min_income = $myPreferences->min_income;

      if($my_profile->preferred_manglik == null) {
        if($my_profile->manglik == 1 ) {
          $mangliks = [1, 2];
        }
        elseif ($my_profile->manglik == 0) {
          $mangliks = [0, 2];
        }
        else{
          $mangliks = [0, 1, 2];
        }
      }
      else
        $mangliks[] = $my_profile->preferred_manglik;
    // min. income for male profiles when calculating for female profiles
      if ($my_profile->preferred_min_income == null) {
        $min_income = $my_profile->yearly_income + 200000;
      }
      else 
        $min_income = $my_profile->preferred_min_income;

      $defaultCastes = 0;
      if($myPreferences->caste_id == null) {
        if(!($my_profile->caste_id == null)) {
          $my_profile_caste = Caste::where('id', $my_profile->caste_id)->first()->caste;
          $mapping_id = json_decode(file_get_contents('https://partner.hansmatrimony.com/api/caste_map_mm?caste='.$my_profile_caste));

          if($mapping_id != null && $mapping_id != 0) {
            $castes = file_get_contents('https://partner.hansmatrimony.com/api/caste_mapping');
            $castes = json_decode($castes)[$mapping_id-1];
            $my_profile->prefered_caste = explode(',', $castes->castes);

            foreach ($my_profile->prefered_caste as $caste) {
              if(Caste::where('caste', $caste)->exists())
                $preferred_caste_id[] = Caste::where('caste', $caste)->first()->id;
            }

          }
          else $defaultCastes = 1; 
        }
        else
          $defaultCastes = 1;
      }
      else {
        if ($myPreferences->caste_id == 0)
          $defaultCastes = 1;
        else
          $preferred_caste_id = $my_profile->prefered_caste;
      }
//dd($preferred_caste_id);
      $compatibility_exist = Compatibility::where('client_id', $id)->exists();
      if($compatibility_exist) {
        $compatibility = Compatibility::where('client_id', $id)->first();
        $extra = $compatibility->extra;
        $profile_status = json_decode($compatibility->profile_status) ? json_decode($compatibility->profile_status) : [];
        $sent_profiles_id = [];
        if($profile_status) {
          foreach ($profile_status as $key) {
            array_push($sent_profiles_id, $key->client_id);
          }
        }
      }

      if($my_profile->gender== 0){
        $gender= 1;
        if($defaultCastes == 1){
          $query = DB::table('client_clientprofile')
          ->where('gender',$gender)
          ->whereRaw("(birth_date >= '".$birth_date."' && birth_date < '".Carbon::parse($birth_date)->addYears(5)."')");
          if($compatibility_exist) {
            if($extra == 0)
              $query = $query->where('id','>=',$last_id);
            $query = $query->whereNotIn('id', $sent_profiles_id);
          }

          $query = $query->whereBetween('height',array($height-9,$height-1))
          ->where('marital_status',$my_profile->marital_status)
          ->where('yearly_income','<',$my_profile->yearly_income)
          ->whereIN('manglik', $mangliks)
          ->get();

        } else {
        //echo 'not_open_bar ';
          $query = DB::table('client_clientprofile')
          ->where('gender',$gender)
          ->whereRaw("(birth_date >= '".$birth_date."' && birth_date < '".Carbon::parse($birth_date)->addYears(5)."')");
          if($compatibility_exist) {
            if($extra == 0)
              $query = $query->where('id','>=',$last_id);
            $query = $query->whereNotIn('id', $sent_profiles_id);
          }

          $query = $query->whereBetween('height',array($height-9,$height-1))
          ->where('marital_status',$my_profile->marital_status)
          ->where('yearly_income','<',$my_profile->yearly_income)
          ->whereIN('caste_id', $preferred_caste_id)
          ->whereIN('manglik', $mangliks)
          ->get();
        }
        echo 'HF-'.$query->count().'<br>';
        if($query->count() == 0)
        {
        $query = DB::table('client_clientprofile') // Soft filter
        ->where('gender',$gender)
        ->where('id','>=',$last_id)
        ->whereRaw("(birth_date >= '".$birth_date."' && birth_date < '".Carbon::parse($birth_date)->addYears(5)."')");
        if($compatibility_exist) {
          if($extra == 0)
            $query = $query->where('id','>=',$last_id);
          $query = $query->whereNotIn('id', $sent_profiles_id);
        }
        $query = $query->where('marital_status',$my_profile->marital_status);
        if(isset($preferred_caste_id))
          $query = $query->whereIN('caste_id', $preferred_caste_id);
        $query = $query->get();
        echo 'SF-'.$query->count().'<br>';

      }
    }
    else if($my_profile->gender== 1){
      $gender= 0;
      if($defaultCastes == 1){
        $query = DB::table('client_clientprofile')
        ->where('gender',$gender)
        ->whereRaw("(birth_date <= '".$birth_date."' && birth_date > '".Carbon::parse($birth_date)->subYears(5)."')");
        if($compatibility_exist) {
          if($extra == 0)
            $query = $query->where('id','>=',$last_id);
          $query = $query->whereNotIn('id', $sent_profiles_id);
        }
        $query = $query->where('height','>',$height+1)
        ->where('marital_status',$my_profile->marital_status)
        ->where('yearly_income','>',$min_income)
        ->whereIN('manglik', $mangliks)
        ->get();
      } else {
        //echo 'not_open_bar ';
        $query = DB::table('client_clientprofile')
        ->where('gender',$gender)
        ->whereRaw("(birth_date <= '".$birth_date."' && birth_date > '".Carbon::parse($birth_date)->subYears(5)."')");
        if($compatibility_exist) {
          if($extra == 0)
            $query = $query->where('id','>=',$last_id);
          $query = $query->whereNotIn('id', $sent_profiles_id);
        }
        
        $query = $query->where('height','>',$height+1)
        ->where('marital_status',$my_profile->marital_status)
        ->where('yearly_income','>',$min_income)
        ->whereIN('caste_id', $preferred_caste_id)
        ->whereIN('manglik', $mangliks)
        ->get();
      }
      echo 'HF-'.$query->count().'<br>';

      if($query->count() == 0) // Soft filter
      {
        $query = DB::table('client_clientprofile')
        ->where('gender',$gender)
        ->whereRaw("(birth_date <= '".$birth_date."' && birth_date > '".Carbon::parse($birth_date)->subYears(5)."')");
        if($compatibility_exist) {
          if($extra == 0)
            $query = $query->where('id','>=',$last_id);
          $query = $query->whereNotIn('id', $sent_profiles_id);
        }
        $query = $query->where('marital_status',$my_profile->marital_status);
        if(isset($preferred_caste_id))
          $query = $query->whereIN('caste_id', $preferred_caste_id);
        $query = $query->get();
        echo 'SF-'.$query->count().'<br>';

      }

    }
    // $profile_status = json_decode($profile_status);
    //create a json object for blank compatibility
    $data = [];
    foreach ($query as $label)
    { 
      $age_values =$this->age_compatibility($id,$label->id);
      $hei_values =$this->height_compatibility($id,$label->id);
      $bmi_values = $this->bmi_compatibility($id,$label->id);
      $freshness = $this->freshness($id, $label->id);
      // echo "For= => ".$label->id."-----".number_format(110*$age_values+100*$hei_values+100*$bmi_values+$freshness/180, 2, '.', '')."----";
      $data[] = [
        'client_id' => $label->id,
        'value' => number_format(110*$age_values+100*$hei_values+100*$bmi_values+$freshness/180, 2, '.', ''),
      ];

    }

    $c_array =  collect($data)->sortBy('value')->reverse()->take(300);
    $data = [];
    foreach ($c_array as $val)
    {
      $data[] = [
        'client_id' => $val['client_id'],
        'value' => $val['value'],
      ];
    }
    $data =json_encode($data);
    $last_id = $query->max('id');
    if($query->count() > 300)
      $extra = 1;
    else 
      $extra = 0;

    if(DB::table('compatibilities')->where('client_id','=',$id)->exists()){
      DB::table('compatibilities')->where('client_id',$id)->update([
        'compatibility' => $data,
        'last_id'=> $last_id,
        'extra' => $extra,
      ]);
    }
    else{
      DB::table('compatibilities')->insert([
        'client_id' => $id,
        'compatibility' =>$data,
        'last_id'=> $last_id,
        'extra' => $extra,
      ]);
    }
    echo "Inserted!\n";
  }

  public function age_compatibility($my_id,$your_id){
    $myage = ClientProfile::where('id', $my_id)->first();
    $your_age = ClientProfile::where('id', $your_id)->first();
    $myage1 = strtotime($myage->birth_date) ;
    $yourage1 = strtotime($your_age->birth_date) ;
    $age_diff = ($yourage1-$myage1)/86400;
    if($age_diff < 0)
      $age_diff = ($myage1 - $yourage1)/86400;
    $points=0;
    if($age_diff > 730 && $age_diff <1460){
      $points = 3;
    }
    elseif($age_diff >365 && $age_diff < 1825){
      $points = 2;
    }
    elseif ($age_diff >0 && $age_diff < 2190){
      $points = 1;
    }
    else{
      $points = 0;
    }
    //normalisation
    $points = number_format(((float)$points-2.2)/0.76, 2, '.', '');
    return $points;
  }

  public function height_compatibility($my_id,$your_id){
    $me = ClientProfile::where('id', $my_id)->first();
    $you = ClientProfile::where('id', $your_id)->first();
    $myheight=$me->height;
    $yourheight = $you->height;
    $h_diff = $myheight-$yourheight;
    if ($h_diff < 0)
      $h_diff = $yourheight - $myheight;
    $points =0;
    if($h_diff == 5){
      $points=4;
    }
    elseif ($h_diff ==4 || $h_diff ==6){
      $points = 3;
    }
    elseif ($h_diff==3 || $h_diff == 7){
      $points = 2;
    }
    elseif ($h_diff == 2 || $h_diff ==8 || $h_diff == 1){
      $points = 1;
    }
    else{
      $points = 0;
    }
    $points = number_format(((float)$points-1.87)/1.34, 2, '.', '');
    return $points;
  }

  public function bmi_compatibility($my_id,$your_id){
    $me = ClientProfile::where('id', $my_id)->first();
    $you = ClientProfile::where('id', $your_id)->first();
    $myheight=$me->height;
    $myweight = $me->weight;
    $yourheight =$you->height;
    $yourweight = $you->weight;

    if($myheight == 0 || $yourheight==0)
    {
      $mybmi = 17;
      $yourbmi = 20;
    }
    else{
      $mybmi = $myweight/(($myheight*0.0254)*($myheight*0.0254));
      $yourbmi = $yourweight/(($yourheight*0.0254)*($yourheight*0.0254));
    }
    if($me->gender == 0)
      $bmi_diff = $mybmi-$yourbmi;
    elseif ($me->gender == 1)
      $bmi_diff = $yourbmi-$mybmi;
    if ($bmi_diff >0 && $bmi_diff <1) {
      $points = 1;
    }
    elseif ($bmi_diff >=1 && $bmi_diff<2){
      $points =2;
    }
    elseif ($bmi_diff >=2 && $bmi_diff<3){
      $points =3;
    }
    elseif ($bmi_diff >=3 && $bmi_diff<4){
      $points =3;
    }
    elseif($bmi_diff>=4 && $bmi_diff <= 5){
      $points =1;
    }
    else{
      $points =0;
    }
    $points = number_format(((float)$points-0.08)/0.42, 2, '.', '');
    return $points;
  }

  public function freshness($my_id, $your_id)
  {
    $you = ClientProfile::where('id', $your_id)->first();
    $diff = date_diff(date_create($you->created_at), date_create(date('Y-m-d')));
    $y = -($diff->format("%R%a"));
    $points = number_format(((float)$y + 80.07)/37.88, 2, '.', '');
    return $points;
  }

}
