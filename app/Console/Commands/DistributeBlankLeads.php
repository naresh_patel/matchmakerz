<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\BlankLead;

class DistributeBlankLeads extends Command
{
   /**
   * The name and signature of the console command.
   *
   * @var string
   */
   protected $signature = 'distribute:blanklead';

  /**
   * The console command description.
   *
   * @var string
   */
  protected $description = 'Taking Leads from Hans, distributing to matchmakers';

  /**
   * Create a new command instance.
   *
   * @return void
   */
  public function __construct()
  {
    parent::__construct();
  }

  /**
   * Execute the console command.
   *
   * @return mixed
   */
  public function handle()
  {
    $url = "https://partner.hansmatrimony.com/api/getUsers";
    $last_id = BlankLead::where('lead_type', 0)->orderBy('id_no', 'desc')->first()->id_no;
    $data = array('index' => $last_id);
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $leads = curl_exec($ch);
    curl_close($ch);
    $leads = json_decode($leads);
    foreach ($leads as $lead) {
      if(!BlankLead::where('phone_number', substr($lead->mobile, 0, 15))->exists()) {
        BlankLead::create([
          'id_no' => $lead->id,
          'phone_number' => $lead->mobile,
          'lead_type' => 0,
          'is_full' => 0
        ]);
      }
    }
  }
}
