<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Hiring;
use App\IncompleteHiringLeads;
use DB;

class FacebookLeads extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'facebook:leads';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to fetch leads from facebook';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $fb_channel_exists = DB::table('channels')->where('type','Facebook')->exists();
      if($fb_channel_exists){
        $fb_channels = DB::table('channels')->where('type','Facebook')->latest()->get();
        foreach ($fb_channels as $fb_channel) {
          if(strpos($fb_channel->name, "Hiring") == false) {

          }
          else
            $this->storeFacebookHiringLeads($fb_channel->name);
        }
      }
      $this->info('Fb Leads added succesfully!');
    }

    public function storeFacebookHiringLeads($fb_channel_name)
    {
      $fb_channel = DB::table('channels')->where('name',$fb_channel_name)->first();
      $fb_ad_id = $fb_channel->token;
      $lastExecutionTime = $fb_channel->last_execution_time;
      $access_token = 'EAAGYxS92S9MBAEVJXsmqLGHuT6cCQEeSa3YKuWH8pypxuqUorIkXcsPplZC0dUmgkq2v6ZA5gM2ZAyjL5nd21FDFcSX4Y0y2KDtqRyfZB5rBRNUk4i2tAZBK8MjzeudkbeF4NYVCkf0QdNytxRsUKRprjgV1ZAaHuq6eaShq2XJJOPR5Dr8WXbZAnWrG1AoLB8ZD';
      $url = 'https://graph.facebook.com/v4.0/'.$fb_ad_id.'/leads?access_token='.$access_token.'&limit=500';
      $ctr = 0;
      $latestLeadTime = $lastExecutionTime;
      do {
        $fb_leads = $this->_post_fb_api($url);
        echo $fb_channel_name."   <br>";  
        $fb_leads = json_decode($fb_leads);
        if(isset($fb_leads->data)){
          foreach ($fb_leads->data as $leads) {
            //print_r($leads);
            if($ctr == 0)
            {
              $latestLeadTime = strtotime($leads->created_time) + 19800; // As first one is the latest lead
            }
            $ctr++;
            if(strtotime($leads->created_time) + 19800 > $lastExecutionTime){
              $incompleteLead = new IncompleteHiringLeads();
              foreach ($leads->field_data as $data) {
                if($data->name == 'phone_number'){
                  $incompleteLead->user_phone = $data->values[0];
                  $incompleteLead->parent_id = $data->values[0];
                }
              }

              $incompleteLead->call_time_stamp = strtotime($leads->created_time) + 19800;

              $incompleteLead->call_time = date('Y-m-d H:i:s', $incompleteLead->call_time_stamp);
              // dd($incompleteLead->call_time);
              $incompleteLead->status = 'blank';
              $incompleteLead->recording_link = 'N/A';
              $incompleteLead->channel = $fb_channel->name;
    //          $incompleteLead->temple_phoneno = $fb_channel->blank_lead_handler;
              $incompleteLead->token = $fb_channel->token;
              $incompleteLead->save();
            }
          }
        }
        if(isset($fb_leads->paging->next))
          $url = $fb_leads->paging->next;
      } while (isset($fb_leads->paging->next));
      // echo $ctr.'<br><br><br>_______________________________________________________';
      $lastExecutionTime = $latestLeadTime;
      $fb_channel->last_execution_time = $lastExecutionTime;
      DB::table('channels')->where('name', $fb_channel->name)->update(['last_execution_time' => $lastExecutionTime]);
      //$fb_channel->save();

    }

    private function _post_fb_api($url){
      try {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        $result = curl_exec($ch);
      } catch (Exception $e) {
        print_r($e);
        return false;
      }

      curl_close($ch);
      if ($result) {
        return($result);
      } else {
        print_r($result);
        die('Unable to get data from facebook');
      }
    }
}
