<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Compatibility;
use DB;

class UpdateDailyQuota extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:quota';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Daily updating quota of profiles to send';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
       DB::table('compatibilities')->update(['daily_quota' => 0]); 
       Compatibility::where('current', 6)->update(['current' => 0]);
    }
}
