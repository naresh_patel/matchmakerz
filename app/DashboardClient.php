<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DashboardClient extends Model
{
        protected $table = 'client_clientprofile';

  protected $fillable = ['matchmaker_id', 'name', 'phone_number', 'id_no', 'profile_photo', 'gender', 'birth_date', 'current_city', 'weight', 'height', 'whatsapp_number', 'birth_place', 'birth_time', 'food_choice', 'disability', 'disabled', 'marital_status', 'children', 'mother_tongue_id', 'religion', 'zodiac', 'manglik', 'caste_id', 'citizenship', 'want_horoscope_match', 'is_working', 'education', 'degree_id', 'college', 'occupation', 'sub_occupation', 'office_address', 'yearly_income', 'family_type', 'house_type', 'home_town', 'home_address', 'gotra', 'mother_status', 'father_status', 'father_occupation', 'mother_occupation', 'family_income', 'landline', 'married_son', 'unmarried_son', 'married_daughter', 'unmarried_daughter', 'matchmaker_type', 'profile_type', 'is_active', 'amount_fix', 'payment_status'];

  protected $appends = ['age', 'mother_tongue', 'cast_name', 'degree_name', 'matchmaker_name', 'active_till'];

  protected $attributes = [
   'food_choice' => 1,
   'disability' => 0,
   'marital_status' => 0,
   'children' => 0,
   'mother_tongue_id' => 1,
   'religion' => 0,
   'caste_id' => 1,
   'citizenship' => 0,
   'want_horoscope_match' => false,
   'is_working' => true,
   'yearly_income' => 0,
   'family_type' => 0,
   'house_type' => 0,
   'mother_status' => 0,
   'father_status' => 0,
   'family_income' => 0,
   'married_daughter' => 0,
   'married_son' => 0,
   'unmarried_daughter' => 0,
   'unmarried_son' => 0,
   'profile_type' => 1,
   'is_active' => true,
   'amount_fix' => 0,
   'payment_status' => 0];

  public function getAgeAttribute()
  {
    return date("Y", time()+19800) - date("Y", strtotime($this->birth_date));
  }

  public function getProfilePhotoAttribute($photo)
  {
    if($photo) {
      if(strpos($photo, 'hansmatrimony') !== false)
        return 'https://s3.ap-south-1.amazonaws.com/'.$photo;
      elseif(strpos($photo, 'client') !== false)
        return 'https://matchmakerz.s3.ap-south-1.amazonaws.com/'.$photo;
      elseif(strpos($photo, 'client') === false && strpos($photo, 'hansmatrimony') === false)
        return 'https://s3.ap-south-1.amazonaws.com/hansmatrimony/uploads/'.$photo;
    }
    else
      return ;
  }

  public function getActiveTillAttribute()
  {
  	try {
  		return $this->active_till;
  	} catch(\Exception $e) {
  		return "Free";
  	}
  }

  public function getMotherTongueAttribute()
  {
    $language = MotherTongue::where('id', $this->mother_tongue_id)->first();
    if($language)
      return $language->id;
    else 
      return ;
  }

  public function getCastNameAttribute()
  {
    $caste = Caste::where('id', $this->caste_id)->first();
    if($caste)
      return $caste->caste;
    else 
      return ;
  }

  public function getDegreeNameAttribute()
  {
    $degree = Degree::where('id', $this->degree_id)->first();
    if($degree)
      return $degree->name;
    else 
      return ;
  }

  public function getMatchmakerNameAttribute()
  {
    $matchmaker = MatchmakerUser::where('id', $this->matchmaker_id)->first();
    if($matchmaker)
      return $matchmaker->first_name. ' '. $matchmaker->last_name;
    else
      return ; 
  }
}
