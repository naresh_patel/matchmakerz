<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DashboardInteraction extends Model
{
  protected $table = 'client_showinterest';

	protected $fillable = ['approved_by_receiver', 'matched_for_id', 'matched_to_id', 'receiver_matchmaker_id', 'sender_matchmaker_id', 'delete', 'reconnect_limit'];

	protected $appends = ['matched_to', 'matched_for', 'receiver_matchmaker', 'sender_matchmaker'];

	protected $attributes = [
		'reconnect_limit' => 0,
		'approved_by_receiver' => 2,
		'delete' => false,

	];

	public function getMatchedToAttribute()
	{
		$matched_to = ClientProfile::where('id', $this->matched_to_id)->first();
		return $matched_to;
	}

	public function getDeleteAttribute($delete)
	{
		if($delete == 1)
			return true;
		else
			return false;
	}

	public function getMatchedForAttribute()
	{
		$matched_for = ClientProfile::where('id', $this->matched_for_id)->first();
		return $matched_for;
	}

	public function getReceiverMatchmakerAttribute()
	{
		return MatchmakerUser::where('id', $this->receiver_matchmaker_id)->first();
	}

	public function getSenderMatchmakerAttribute()
	{
		return MatchmakerUser::where('id', $this->sender_matchmaker_id)->first();
	}
}
