<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DashboardPayment extends Model
{
	public $timestamps = false;

	protected $table = 'client_clientpayment';

	protected $fillable = ['client_id', 'matchmaker_id', 'plan_id', 'meeting_charge', 'upfront_charge', 'before_roka_charge', 'payment_for', 'payment_status', 'payment_amount', 'payment_id', 'payment_gateway_status', 'payment_gateway', 'bank_ref_num', 'pg_type', 'pay_mode', 'paid_on', 'expire_on', 'verification_code', 'amount_settled'];

	protected $appends = ['plan', 'matchmaker', 'client'];

	protected $attributes = [
		'upfront_charge' => 0.0,
		'meeting_charge' => 0.0,
		'before_roka_charge' => 0.0,
		'payment_amount' => 0,
		'payment_status' => 0,
		'payment_id' => '',
		'bank_ref_num' => '',
		'pg_type' => '',
		'payment_for' => '',
		'verification_code' => '',
		'amount_settled' => false,
		'payment_gateway' => '',
		'payment_gateway_status' => '',
	];

	public function getPlanAttribute()
	{
		return Plans::where('id', $this->plan_id)->first();
	}

	public function getClientAttribute()
	{
		return ClientProfile::where('id', $this->client_id)->select(['id', 'name', 'phone_number'])->first();
	}

	public function getMatchmakerAttribute()
	{
		return MatchmakerUser::where('id', $this->matchmaker_id)
		->select(['id', 'first_name', 'last_name', 'unique_referral_code', 'phone_number'])
		->first();
	}
}
