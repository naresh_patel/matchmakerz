<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DashboardShortlist extends Model
{
  protected $table = 'client_shortlist';

	protected $fillable = ['shortlist_by_matchmaker_id', 'shortlist_to_matchmaker_id', 'shortlist_to_id', 'shortlist_for_id', 'delete'];

	protected $attributes = [
		'delete' => false,
	];	

	protected $appends = ['shortlist_to', 'shortlist_for', 'shortlist_to_matchmaker', 'shortlist_by_matchmaker'];

	public function getShortlistToAttribute()
	{
		return ClientProfile::where('id', $this->shortlist_to_id)->first();
	}

	public function getShortlistForAttribute()
	{
		return ClientProfile::where('id', $this->shortlist_for_id)->first();
	}

	public function getShortlistToMatchmakerAttribute()
	{
		return MatchmakerUser::where('id', $this->shortlist_to_matchmaker_id)->first();
	}

	public function getShortlistByMatchmakerAttribute()
	{
		return MatchmakerUser::where('id', $this->shortlist_by_matchmaker_id)->first();
	}

	public function getDeleteAttribute($delete)
	{
		if($delete == 1)
			return true;
		else
			return false;
	}
}
