<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Degree extends Model
{
    protected $table = 'client_degree';

    protected $fillable = ['name'];
}
