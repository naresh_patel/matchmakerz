<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hiring extends Model
{
	protected $fillable = [
		'name',
		'mobile',
		'enquiry_date',
		'followup_call_on',
		'comments',
		'is_done',
	];
}
