<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Token;
use App\MatchmakerUser;

class ApiTokenController extends Controller
{
	public function authenticate($request)
	{
		$token = $request->header('Authorization');
		$token = explode(" ", $token);
		if(sizeof($token) == 1 || sizeof($token) > 2)
			return ;

		else {	
			try {
				$matchmaker_token = Token::where('key', $token[1])->first();
				$matchmaker = MatchmakerUser::where('id', $matchmaker_token->user_id)->first();
				if(!empty($matchmaker)) {
					return $matchmaker;
				}
				else
					return ;
			} catch(\Exception $e) {
				return ;
			}
		}
	}
}
