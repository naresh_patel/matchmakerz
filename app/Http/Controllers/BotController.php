<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ClientProfile;
use App\Compatibility;

class BotController extends Controller
{
	public function generateAuthToken()
	{
		$clients = ClientProfile::where('matchmaker_id', '!=', 82)->get();
		foreach ($clients as $client) {
			$len = rand(35, 40);
			$token = '';
			$chars = "abcdefghijklmnopqrstuwxyz0123456789";
			$alphaLength = strlen($chars) - 1;
			for($i = 1; $i <= $len; $i++) {
				$token .= $chars[rand(0, $alphaLength)];
			}
			$token = bcrypt($token.$client->name);
			$client->token = $token;
			$client->save();
		}
	}

	public function getUrl(Request $request)
	{
		$phone_number = $request->phone_number;
		$client = ClientProfile::where('phone_number', 'LIKE', '%'.substr($phone_number, -10))->first();
		if ($client) {
			$url = 'https://hansmatrimony.com/chat?mobile='.$client->phone_number;
		} else {
			$url = '';
		}
		return response($url, 200);
	}

	public function checkRegistered(Request $request)
	{
		$phone_number = $request->phone_number;
		$client = ClientProfile::where('phone_number', $phone_number)->first();
		if ($client) {
			$message = 'Please say Show to start conversation!';
			return response()->json(['registered' => 1, 'phone_number' => $client->phone_number, 'apiwha_autoreply' => $message, 'buttons' => 'Show'], 200);
		}
		else {
			$message = 'You are not registered with us. Please register first to start using services!';
			return response()->json(['registered' => 0, 'apiwha_autoreply' => $message], 200);
		}
	}

	public function reset(Request $request)
	{
		$compatibility = Compatibility::where('id', $request->id)->first();
		if ($compatibility) {
			if ($compatibility->current == 6)
				$compatibility->current = 0;
			$compatibility->daily_quota = 0;
			$compatibility->read_message = 1;
			$compatibility->save();
		}
		return response('Ok');
	}

	public function changeLanguage(Request $request)
	{
		$phone_number = $request->phone_number;
		$client = ClientProfile::where('phone_number', $phone_number)->first();
		if ($client) {
			$client->bot_language = $request->language;
			$client->save();
			return response()->json(['changed' => 1], 200);
		}
		else {
			return response()->json(['changed' => 0], 200);
		}
	}
}
