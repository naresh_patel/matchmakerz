<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MatchmakerUser;
use Hash;

class DashboardAuthenticationController extends Controller
{
    public function authenticate($phone_number, $password, $is_staff, $is_active)
    {
    	try {
    		$user = MatchmakerUser::where('phone_number', $phone_number)
    		->where('is_staff', $is_staff)
    		->where('is_active', $is_active)
    		->first();
    	} catch(\Exception $e) {
    		return ;
    	}
//dd($user->password);
	if(Hash::check($password, $user->password))
            return $user;
        else
            return ;
    }
}
