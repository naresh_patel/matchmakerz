<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ClientProfile;
use App\Compatibility;

class FbBotController extends Controller
{
	public function checkNumber(Request $request)
	{
		$number = $request->mobile;
		$profile_exists = ClientProfile::where('phone_number', 'LIKE', '%'.substr($number, -10))->exists();
		if(!$profile_exists)
			return response()->json(['exists' => 0, 'message' => 'Phone number does not exists'], 200);
		else {
			$client = ClientProfile::where('phone_number', 'LIKE', '%'.substr($number, -10))->first();
			try {
				$compatibility = Compatibility::where('client_id', $client->id)->first();
				$sentProfiles = json_decode($compatibility->profile_status);
				if($sentProfiles == null) {
					$messageToSent = 'nothing';
				}
				else {
					$lastData = $sentProfiles[sizeof($sentProfiles)-1];
					if($lastData->status == 'P') {
						$lastSentProfile = ClientProfile::where('id', $lastData->client_id)->first();
						$messageToSent = "👉 Say 'YES' or 'NO' to ".$lastSentProfile->name. "'s profile.";
					}
					else
						$messageToSent = 'nothing';
				}
				$credits = $compatibility->credits;
				$no_of_profile_seen = $compatibility->daily_quota == null ? 0 : $compatibility->daily_quota;

				return response()->json(['exists' => 1,
					'message' => 'Phone number already exists',
					'credits' => $credits,
					'profileSeen' => $no_of_profile_seen,
					'userProfileCompletionStatus' => 1,
					'messageToSent' => $messageToSent,
				], 200);
			} catch(\Exception $e) {

				return response()->json(['exists' => 1,
					'message' => 'Phone number already exists',
					'credits' => 0,
					'profileSeen' => 0,
					'userProfileCompletionStatus' => 1,
					'messageToSent' => 'nothing',
				], 200);
			}
		}
	}

	public function credits(Request $request)
	{
		$number = $request->mobile;
		$profile_exists = ClientProfile::where('phone_number', 'LIKE', '%'.substr($number, -10))->exists();

		if(!$profile_exists)
			return response()->json(['exists' => 0, 'message' => 'Phone number does not exists'], 200);
		else {
			$client = ClientProfile::where('phone_number', 'LIKE', '%'.substr($number, -10))->first();
//			try {
				$credits = Compatibility::where('client_id', $client->id)->first()->whatsapp_point;
				return response()->json(['credits' => $credits], 200);
//			} catch(\Exception $e) {
//				return response()->json(['credits' => 'No credits Found'], 200);
//			}
		}
	}

	public function sendOtp(Request $request)
	{
		$number = $request->mobile;
		$profile = ClientProfile::where('phone_number', 'LIKE', '%'.substr($number, -10))->first();
		$otp = rand(100000, 999999);
		$text = $otp." is your OTP(One Time Password) to login.";
		$username = env('USER_NAME');
		$password = env('PASSWORD');
		$message = urlencode($text);
		$mobile = "+91".substr($number, -10);
		$url = "https://dqell.api.infobip.com/sms/1/text/query?username=".$username."&password=".$password."&to=".$mobile."&text=".$message;
		try {
			$response = file_get_contents($url);
			return response()->json(['message' => 'OTP Sent', 'otp' => $otp], 200);
		}
		catch (\Exception $e) {
			return response()->json(['message' => 'Some Error Occurred', "error" => $e], 400);
		}
	}

	public function reset(Request $request)
	{
		$compatibility = Compatibility::where('id', $request->id)->first();
		if ($compatibility) {
			if ($compatibility->current == 6)
				$compatibility->current = 0;
			$compatibility->daily_quota = 0;
			$compatibility->read_message = 1;
			$compatibility->save();
		}
		return response('Ok');
	}
}
