<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use App\Hiring;
use App\IncompleteHiringLeads;
use App\MatchmakerUser;
use App\Channel;

class HiringController extends Controller
{
	public function index(Request $request)
	{
		// if (Auth::user()->temple_id == 'admin' || Auth::user()->role == 5) {
			$no_of_open_leads = Hiring::where('is_done', 0)->count();
			$open_leads = Hiring::orderBy('followup_call_on', 'asc')
			->where('is_done', 0)
			->paginate(10);
			$converted_leads = Hiring::where('is_done', 1)->orderBy('followup_call_on', 'asc')->paginate(10);
			$rejected_leads = Hiring::where('is_done', 2)->orderBy('followup_call_on', 'asc')->paginate(10);

			if($request->phone_number) {
				$phone_number = $request->phone_number;
				$matchmaker = MatchmakerUser::where('phone_number', 'LIKE', '%'.substr($phone_number, -10).'%')->first();
				$open_lead = Hiring::where('mobile', 'LIKE', '%'.substr($phone_number, -10).'%')->where('is_done', 0)->first();
			}
		// } else {
		// 	return back();
		// }

		return view('hiring.hiringhome', compact('edit_assign_to_name', 'open_leads', 'no_of_open_leads', 'converted_leads', 'rejected_leads', 'phone_number', 'matchmaker', 'open_lead'));
	}

	public function store(Request $request) {
		$incomplete_lead = IncompleteHiringLeads::where('user_phone', 'LIKE', '%'.substr($request->mobile, -10).'%')->delete();
		Hiring::create([
			'name' => $request->name,
			'mobile' => substr($request->mobile, -10),
			'is_done' => 0,
			'followup_call_on' => $request->followup_call_on,
			'enquiry_date' => $request->enquiry_date,
			'gender' => $request->gender,
			'comments' => $request->comments
		]);

		$text = " डाउनलोड करें हमारी ऐप इस लिंक पर जाकर https://play.google.com/store/apps/details?id=com.hansPartner ";
		$message = urlencode($text);
		$mobiles = '+91'.substr($request->mobile, -10);
		$username=env('USER_NAME');
		$password=env('PASSWORD');
		$smsurl='https://dqell.api.infobip.com/sms/1/text/query?username=MY_USERNAME&password=MY_PASSWORD&to=PHONE&text=MESSAGE';
		$smsurl=str_replace('MY_USERNAME',$username,$smsurl);
		$smsurl=str_replace('MY_PASSWORD',$password,$smsurl);
		$smsurl=str_replace('PHONE',$mobiles,$smsurl);
		$smsurl=str_replace('MESSAGE',$message,$smsurl);
		try{
			$result=file_get_contents($smsurl);
			return redirect()->route('hiring');
		}
		catch(\Exception $e){
			return response()->json(['status' => 0, 'message' => 'Some Error Occurred'], 400);
		}
	}

	public function markLeadDone(Request $request)
	{
		$data = $request->all();
		$lead = Hiring::find($data['lead_id']);
		$lead->is_done = 1;
		$lead->save();
		return back();
	}

	public function rejectLead(Request $request)
	{
		$data = $request->all();
		$lead = Hiring::find($data['lead_id']);
		$lead->is_done = 2;
		$lead->save();

		return redirect()->route('hiring');
	}

	public function rejectToOpenLead(Request $request)
	{
		$data = $request->all();
		$lead = Hiring::find($data['lead_id']);
		$lead->is_done = 0;
		$lead->save();

		return redirect()->route('hiring');
	}

	public function deleteLead(Request $request)
	{
		$data = $request->all();
		$lead = Hiring::find($data['lead_id']);
		$lead->delete();

		return redirect()->route('hiring');
	}

	public function updateLead(Request $request)
	{
		$com = Hiring::where('id', $request->lead_id)->first()->comments;
		$comm = $com.'  '.date('Y-m-d', time()).'--'.$request->comments;
		Hiring::where('id', $request->lead_id)
		->update([
			'followup_call_on' => $request->followup_call_on,
			'comments' => $comm,
		]);

		return back();
	}

	public function incomplete()
	{
		$status = null;
		if (isset($_GET['status']) && strlen($_GET['status'])) {
			$status = $where['status'] = $_GET['status'];
		}

		// if (Auth::user()->role === 5 || Auth::user()->role === 9) {
			$incomplete_leads = IncompleteHiringLeads::orderBy('call_time', 'desc');
			if (isset($_GET['status']) && strlen($_GET['status']))
				$incomplete_leads = $incomplete_leads->where('status', $_GET['status']);
			$incomplete_leads = $incomplete_leads->groupBy('call_time_stamp', 'token')
			->paginate(20);
		// } 
		// else {
		// 	return back();
		// }

		return view('hiring.incomplete', compact('incomplete_leads', 'status'));
	}

	public function deleteIncompleteLead(Request $request)
	{
		$lead = IncompleteHiringLeads::where('parent_id', $request->parent_id)->delete();
		return back();
	}
}
