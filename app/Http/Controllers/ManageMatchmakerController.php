<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\MatchmakerUser;
use App\ClientProfile;
use App\ClientPayment;
use App\MatchmakersOnboarded;
use App\SharedProfile;
use App\MatchmakerFollowup;
use App\Otp;
use DateTime;
use Storage;
use DateInterval;
use DatePeriod;


class ManageMatchmakerController extends Controller
{
	protected $url = 'https://matchmakerz.s3.ap-south-1.amazonaws.com/static/matchmakerz/profile_pic/';
	public function matchmakersOnboarded(Request $request)
	{
		$is_approved = 1;
		$is_staff = 0;
		$working_status=1;
		if($request->is_approved != null)
			$is_approved = $request->is_approved;
		if($request->is_staff != null)
			$is_staff = $request->is_staff;
		if($request->working_status !=null)
			$working_status=$request->working_status;

		$matchmakers = MatchmakerUser::leftJoin('client_clientprofile', 'client_clientprofile.matchmaker_id', '=', 'matchmaker_user.id')
		->whereNull('client_clientprofile.id')
		->leftJoin('matchmakers_onboarded', 'matchmakers_onboarded.matchmaker_id', '=', 'matchmaker_user.id')
		->where('matchmaker_type', 0)
		->where('matchmaker_user.is_active', $is_approved)
		->where('is_staff', $is_staff)
		->where('working_status',$working_status);

		if($request->name) {
			$name = $request->name;
			$matchmakers = $matchmakers->whereRaw("(first_name LIKE '%".$name."%' OR last_name LIKE '%".$name."%')");
		}
		if($request->phone_number) {
			$phone_number = $request->phone_number;
			$matchmakers = $matchmakers->where('matchmaker_user.phone_number', 'LIKE', '%'.$phone_number.'%');
		}

		$matchmakers = $matchmakers->select('first_name', 'last_name', 'matchmaker_user.phone_number', 'matchmaker_user.is_active', 'is_staff', 'matchmaker_user.id', 'matchmaker_user.gender', 'matchmaker_user.age', 'followup_date', 'comments', 'matchmaker_user.created_at','working_status','wallet')
		->orderBy('followup_date', 'asc')
		->orderBy('matchmaker_user.id', 'desc')
		->paginate(10);

		$no_of_matchmakers_onboarded = MatchmakerUser::leftJoin('client_clientprofile', 'client_clientprofile.matchmaker_id', '=', 'matchmaker_user.id')
		->whereNull('client_clientprofile.id')
		->where('matchmaker_type', 0)
		->where('matchmaker_user.is_active', $is_approved)
		->where('is_staff', $is_staff)
		->where('working_status',$working_status)
		->count();

		return view('matchmakerz.home', compact('matchmakers', 'no_of_matchmakers_onboarded', 'is_approved', 'is_staff', 'name', 'phone_number','working_status'));
	}

			public function wallet(){

			$matchmakers= MatchmakerUser::all();

			foreach($matchmakers as $user){
				try{
					$upfront_charge = ClientPayment::where('matchmaker_id', $user->id)
				->where('payment_status', 1)
				->sum('upfront_charge');
				$user = MatchmakerUser::where('id',$user->id)->first();
				$bonus_amount = $user->client_bonus;
	
				// $roka_charges = ClientPayment::where('matchmaker_id', $user->id)
				// ->where('payment_status', 1)
				// ->sum('before_roka_charge');
				$outstanding_amount = ClientPayment::where('matchmaker_id', $user->id)
				->where('payment_status', 0)
				->sum('upfront_charge');
	
				if($user->parent) {
					$upfront_charge = (float)($upfront_charge*.30);
					// $roka_charges = (float)($roka_charges*.40);
				}
				else {
					$upfront_charge = (float)($upfront_charge*.40);
					// $roka_charges = (float)($roka_charges*.50);
				}
				$total_wallet_balance = (float)($upfront_charge);
	
				//first child of parent
				$referred_child_id = MatchmakerUser::where('parent_id', $user->id)->pluck('id');
				$referred_child = MatchmakerUser::where('parent_id', $user->id)->get();
				foreach ($referred_child as $child) {
					$child_upfront_charge = ClientPayment::where('matchmaker_id', $child->id)->where('payment_status', 1)
					->sum('upfront_charge');
					// $child_roka_charge = ClientPayment::where('matchmaker_id', $child->id)->where('payment_status', 1)
					// ->sum('before_roka_charge');
	
					// if($child->parent_id == $user->id && $child_roka_charge)
					// 	$roka_charges += (float)($child_roka_charges * .07);
	
					if($child->parent_id == $user->id && $child_upfront_charge)
						$upfront_charge += (float)($child_upfront_charges * .07);
				}
	
				//child of childs of parent matchmaker
				$child_of_childs = MatchmakerUser::whereIn('parent_id', $referred_child_id)->get();
				foreach ($child_of_childs as $child) {
					$child_upfront_charge = ClientPayment::where('matchmaker_id', $child->id)->where('payment_status', 1)
					->sum('upfront_charge');
					// $child_roka_charge = ClientPayment::where('matchmaker_id', $child->id)->where('payment_status', 1)
					// ->sum('before_roka_charge');
	
					// if($child->parent_id == $user->id && $child_roka_charge)
					// 	$roka_charges += (float)($child_roka_charges * .03);
	
					if($child->parent_id == $user->id && $child_upfront_charge)
						$upfront_charge += (float)($child_upfront_charges * .03);
				}
	
				if($total_wallet_balance == null)
					$total_wallet_balance = 0;
				if($upfront_charge == null)
					$upfront_charge = 0;
				if($outstanding_amount == null)
					$outstanding_amount = 0;
				if($bonus_amount == null)
					$bonus_amount = 0;
				
					$total_wallet_balance = $total_wallet_balance + $bonus_amount;
					MatchmakerUser::where('id',$user->id)->update(['wallet' => $total_wallet_balance]);
				}catch(\Exception $e){
					dd ($e->getmessage());
				}

			}
	}


		public function requestedWithdrawl(){
		$matchmakers= MatchmakerUser::where('withdrawlrequest',1)
				->select('matchmaker_user.id','matchmaker_user.first_name' ,'matchmaker_user.last_name','matchmaker_user.age','matchmaker_user.gender','matchmaker_user.wallet','matchmaker_user.phone_number','bank_details.bank_name','bank_details.ifsc_code','bank_details.account_number','bank_details.paytm','bank_details.upi','bank_details.account_name')
				->join('bank_details','bank_details.matchmaker_id','=','matchmaker_user.id')->groupby('matchmaker_user.id')
				->get();
		$count = MatchmakerUser::where('withdrawlrequest',1)
		->count();
		return view('matchmakerz.withdrawl',compact('matchmakers','count'));
	}

		public function updatewallet(Request $request){
		 $matchmaker = MatchmakerUser::where('id',$request->matchmaker_id)->first();
       		 $wallet = $matchmaker['wallet']-$request->amount;
		 MatchmakerUser::where('id',$request->matchmaker_id)->update(['wallet' => $wallet]);
//		 dd($request->all());
		 return back()->with('message');
		}

		public function profilesSharedBySelf(Request $request)
	{

		try {
			$shared_profiles = SharedProfile::where('sender_matchmaker_id', $request->matchmaker_id);
			if($request->client_id)
				$shared_profiles = $shared_profiles->where('shared_to_id', $request->client_id);
			$shared_profiles = $shared_profiles->get()->toArray();
			$shared_profiles = array_column($shared_profiles, 'shared_profile');
			return view('matchmakerz.shared',compact('shared_profiles'));
		} catch(\Exception $e) {
			return response()->json(['status' => 0, 'message' => $e->getmessage()], 200);
		}
	}

		public function imageupload(Request $request){
				try {
			if($request->file('image') == null) {
				throw new RaiseException("Empty Content", 200);
			}
			else {
				$image = $request->file('image');
				$extension = $image->extension();
				if($extension != 'jpg' && $extension != 'jpeg' && $extension != 'png')
					return response()->json(['status' => 0, 'message' => 'Please upload only jpg/jpeg/png file'], 200);
				else {
					$filename = $image->getClientOriginalName().'image'.'.jpg';
					Storage::disk('s3')->put('static/matchmakerz/profile_pic/'.$filename, file_get_contents($image), 'public');
					return back()->with('message', $this->url.$filename);
				}
			}
		} 
		catch (\Exception $e) {
			return response()->json(['status' => 0, 'message' => $e->getmessage()], 200);
		}
		}


	public function hashcode(Request $request){
		try {
			$otp_expire = date("Y-m-d H:i:s", strtotime("+10 minute")+19800);
			$followups = Otp::updateOrCreate(
				['mobile_number' => 'hash'],
				['otp' => $request->hash, 'otp_verify' => 0, 'otp_expire' => $otp_expire]
			);
			return back()->with('message','Updated Successfully!!');
		} catch (\Exception $e) {
			return response()->json(['status'=>0,'message'=>$e->getmessage()],200);
		}
		
	}



	public function firstClient(Request $request)
	{
		$matchmakers = MatchmakerUser::leftJoin('client_clientprofile', 'client_clientprofile.matchmaker_id', '=', 'matchmaker_user.id')
		->whereNotNull('client_clientprofile.id')
		->where('client_clientprofile.amount_fix', 0)
		->where('matchmaker_type', 0)
		->where('is_staff', 0);

		$no_of_free_clients = $matchmakers->count();
		if($request->name) {
			$name = $request->name;
			$matchmakers = $matchmakers->whereRaw("(first_name LIKE '%".$name."%' OR last_name LIKE '%".$name."%')");
		}
		if($request->phone_number) {
			$phone_number = $request->phone_number;
			$matchmakers = $matchmakers->where('matchmaker_user.phone_number', 'LIKE', '%'.$phone_number.'%');
		}

		$matchmakers = $matchmakers->leftJoin('matchmaker_followups', 'matchmaker_followups.matchmaker_id', '=', 'matchmaker_user.id');
		if($request->assigned_to) {
			$assigned_to = $request->assigned_to;
			$matchmakers = $matchmakers->where('assigned_to', 'LIKE', '%'.$assigned_to.'%');
		}

		$no_of_matchmakers = $matchmakers->selectRaw(' DISTINCT client_clientprofile.matchmaker_id')->get()->count();

		$matchmakers = $matchmakers->groupBy('client_clientprofile.matchmaker_id')
		->select('first_name', 'last_name', 'matchmaker_user.phone_number', 'matchmaker_user.is_active', 'is_staff', 'matchmaker_user.id', 'matchmaker_user.gender', 'matchmaker_user.age', DB::raw('count(*) as no_of_clients'), 'followup_date', 'comments', 'assigned_to','wallet')
		->orderBy(DB::raw('count(*)'), 'desc')
		->orderBy('followup_date', 'asc')
		->paginate(10);

		return view('matchmakerz.firstClient', compact('matchmakers', 'name', 'phone_number', 'assigned_to', 'no_of_matchmakers', 'no_of_free_clients'));
	}

	public function updateActive(Request $request){
		$matchmaker_id=$request->id;
		$matchmaker=MatchmakerUser::where('id',$matchmaker_id)->first();
		if($matchmaker->working_status==0)
			$matchmaker->working_status=1;
		else
			$matchmaker->working_status=0;
		$matchmaker->save();
		return ("Updated Status");
	}

	public function clientConverted()
	{
		//clients who have paid for a plan
		$payment_clients = $verified_payment_clients = ClientPayment::join('client_clientprofile', 'client_clientprofile.id', '=', 'client_clientpayment.client_id')
		->where('client_clientpayment.payment_status', 1)
		->where('client_clientprofile.payment_status', 1)
		->orderBy('client_clientpayment.id', 'desc')
		->paginate(10);

		//$not_verified_payment_clients = ClientPayment::join('client_clientprofile', 'client_clientprofile.id', '=', 'client_clientpayment.client_id')
		//->where('client_clientpayment.payment_status', 0)
		//->orderBy('client_clientpayment.id', 'desc')
		//->paginate(10);

		return view('matchmakerz.clientConverted', compact('payment_clients', 'verified_payment_clients'));
	}

	public function sendWhatsapp(Request $request)
	{
		$phone_number = substr($request->phone_number, -10);
		$phone_number = "91".$phone_number;
		$message = $request->message;
		app('App\Http\Controllers\WhatsAppController')->sendTextMessage($message, $phone_number);
		return back();
	}

	public function updateFollowUp(Request $request)
	{
		$comment = $request->comment;
		$followup_date = $request->followup_date;
		$matchmaker_id = $request->matchmaker_id;
		$type = $request->type;
		$matchmaker = MatchmakerUser::where('id', $matchmaker_id)->first();
		if($type == 'onboarded')
			$followups = MatchmakersOnboarded::where('matchmaker_id', $matchmaker_id)->first();
		elseif ($type == 'firstClient')
			$followups = MatchmakerFollowup::where('matchmaker_id', $matchmaker_id)->first();
		if($followups) {
			$followups->followup_date = $followup_date;
			$followups->comments = $followups->comments.date("Y-m-d H:i:s", time()+19800).' : '.$comment.";";
			$followups->save();
		}
		else {
			if($type == 'onboarded')
				$followups = new MatchmakersOnboarded();
			elseif ($type == 'firstClient')
				$followups = new MatchmakerFollowup();
			$followups->matchmaker_id = $matchmaker_id;
			$followups->followup_date = $followup_date;
			$followups->comments = date("Y-m-d H:i:s", time()+19800).' : '.$comment.";";
			$followups->save();
		}

		return back()->with('message', 'Updated Follow-up for Matchmaker : '.$matchmaker->first_name.' '.$matchmaker->last_name);
	}

	public function overview(Request $request)
	{
		if(!$request->from) {
			$now = new DateTime(date("Y-m-d", time()));
			$start = new DateTime(date("Y-m-d", time()));
			$end = $now->add(new DateInterval('P1D'));
		}
		else {
			$start = new DateTime($request->from);
			$end = new DateTime($request->to);
			$end->add(new DateInterval('P1D'));
		}
		if($start > $end)
			return back()->with('message', '"From" should be less than "To"');
		$interval = new DateInterval('P1D');
		$data_range = new DatePeriod($start, $interval, $end);
		$i = 0;
		foreach ($data_range as $date) {
			$from = $date->format('Y-m-d H:i:s');
			$to = $date->add(new DateInterval('P1D'));
			$to = $to->format('Y-m-d H:i:s');
			$stats[$i]['date'] = $from;

			//matchmakers onboarded date wise
			$stats[$i]['matchmakers_onboarded']['data'] = MatchmakerUser::leftJoin('client_clientprofile', 'client_clientprofile.matchmaker_id', '=', 'matchmaker_user.id')
			->whereNull('client_clientprofile.id')
			->whereBetween('matchmaker_user.created_at', [$from, $to])
			->select('first_name', 'last_name', 'matchmaker_user.phone_number', 'matchmaker_user.is_active', 'is_staff', 'matchmaker_user.id', 'matchmaker_user.gender', 'matchmaker_user.age')
			->get();
			$stats[$i]['matchmakers_onboarded']['count'] = $stats[$i]['matchmakers_onboarded']['data']->count();

			//matchmakers added first clients date wise
			$stats[$i]['matchmakers_first_client']['data'] = MatchmakerUser::leftJoin('client_clientprofile', 'client_clientprofile.matchmaker_id', '=', 'matchmaker_user.id')
			->whereNotNull('client_clientprofile.id')
			->whereBetween('client_clientprofile.created_at', [$from, $to])
			->orderBy('client_clientprofile.created_at')
			->groupBy('client_clientprofile.matchmaker_id')
			->having(DB::raw('count(*)'), 1)
			->select('first_name', 'last_name', 'matchmaker_user.phone_number', 'matchmaker_user.is_active', 'is_staff', 'matchmaker_user.id', 'matchmaker_user.gender', 'matchmaker_user.age')
			->get();
			$stats[$i]['matchmakers_first_client']['count'] = $stats[$i]['matchmakers_first_client']['data']->count();

			//clients added date wise
			$stats[$i]['clients_added']['data'] = ClientProfile::whereBetween('client_clientprofile.created_at', [$from, $to])
			->leftJoin('matchmaker_user', 'matchmaker_user.id', '=', 'client_clientprofile.matchmaker_id')
			->select('client_clientprofile.*', 'matchmaker_user.first_name', 'matchmaker_user.last_name')
			->orderBy('client_clientprofile.created_at')
			->get();
			$stats[$i]['clients_added']['count'] = $stats[$i]['clients_added']['data']->count();

			//clients converted from free to converted date wise
			$stats[$i]['clients_converted']['data'] = ClientPayment::where('amount_fix', 1)->whereBetween('client_clientpayment.paid_on', [$from, $to])
			->join('client_clientprofile', 'client_clientprofile.id', '=', 'client_clientpayment.client_id')
			->where('client_clientpayment.payment_status', 1)
			->leftJoin('matchmaker_user', 'matchmaker_user.id', '=', 'client_clientprofile.matchmaker_id')
			->select('client_clientprofile.*', 'matchmaker_user.first_name', 'matchmaker_user.last_name', 'client_clientpayment.payment_status', 'client_clientpayment.upfront_charge', 'client_clientpayment.paid_on')
			->orderBy('client_clientpayment.paid_on', 'desc')
			->get();
			$stats[$i]['clients_converted']['count'] = $stats[$i]['clients_converted']['data']->count();
			$i++;
		}

		$start = $start->format('Y-m-d H:i:s');
		$end->modify('-1 day');
		$end = $end->format('Y-m-d H:i:s');
		return view('overview.home', compact('stats', 'start', 'end'));
	}

	public function assignTo(Request $request) {
		$matchmaker = MatchmakerUser::where('id', $request->matchmaker_id)->first();
		try {
			$followups = MatchmakerFollowup::updateOrCreate(
				['matchmaker_id' => $request->matchmaker_id],
				['assigned_to' => $request->assign_to]
			);
			return back()->with('message', 'Assigned '.$request->assign_to.' to Matchmaker : '.$matchmaker->first_name.' '.$matchmaker->last_name);
		} catch (\Exception $e) {
			return back()->with('message', 'Not Updated');
		}
	}
}
