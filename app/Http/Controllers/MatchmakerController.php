<?php

namespace App\Http\Controllers;

use Intervention\Image\ImageManagerStatic as Image;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Storage;
use App\MatchmakerUser;
use App\Token;
use App\Otp;
use App\ClientProfile;
use App\ClientPayment;
use App\Transaction;
use App\BlankLead;
use App\Lead;
use App\IdCard;
use App\Incentive;
use App\Plans;
use App\Offer;
use App\Tip;
use App\PlansForExisting;
use App\BankDetails;
use App\MatchmakerBlankLead;
use Razorpay\Api\Api;
use App\Tutorial;
use Exception;
use App\Hiring;
use Exception as RaiseException;

class MatchmakerController extends Controller
{
	protected $url = 'https://matchmakerz.s3.ap-south-1.amazonaws.com/static/matchmakerz/profile_pic/';
	protected $url2 ='https://matchmakerz.s3.ap-south-1.amazonaws.com/static/matchmakerz/id_card/';
	// protected $url3 ='https://matchmakerz.s3.ap-south-1.amazonaws.com/static/matchmakerz/fonts/';


	// Generate IdCards For MatchMakers

	public function generateIdCard(Request $request){
		$user = app('App\Http\Controllers\ApiTokenController')->authenticate($request);
		if($user == null)
			return response()->json(['message' => 'Token Invalid','status'=>0], 200);

		//$user = (MatchmakerUser::where('id', $user->id));
		if($user){
			if(!$user->profile_pic){
				return response()->json(['status' => 0, 'message' => 'Please upload profile picture first'], 200);
			}
		$img=Image::make($this->url2.'2.png');
		$filename = 'mmProfilePic_'.$user->id.'.jpg';
		$profilepicurl=$this->url.$filename;
		$img2=Image::make($profilepicurl)->resize(82,82);
		$img->insert($img2,'top-left',22,24);
		$img3=Image::make($this->url2.'logo1.png')->resize(40,37);
		$img->insert($img3,'top-right',0,0);

		$name=$user->first_name."  ".$user->last_name;

		$name=strtoupper($name);
		$img->text($name,115, 50, function($font) {
			$font->file(public_path('fonts/Poppins-Bold.ttf'));
			$font->size(18);
			$font->color('#fff');
			$font->valign('top');
		});
		$designation=$user->matchmaker_type;
		$img->text($designation,115, 73, function($font) {
			$font->file(public_path('fonts/Poppins-Medium.ttf'));
			$font->size(10);
			$font->color('#fff');
			$font->valign('top');
		});
		$phone=$user->phone_number;
		$img->text($phone,31, 129, function($font) {
			$font->file(public_path('fonts/Poppins-Medium.ttf'));
			$font->size(10);
			$font->color('#4e4e4e');
			$font->valign('top'); 
		});
		$location=$user->location;
		$img->text($location,33, 148, function($font) {
			$font->file(public_path('fonts/Poppins-Medium.ttf'));
			$font->size(10);
			$font->color('#4e4e4e');
			$font->valign('top'); 
		});
		$website="www.hansmatrimony.com/matchmaker";
		$img->text($website,33, 167, function($font) {
			$font->file(public_path('fonts/Poppins-Medium.ttf'));
			$font->size(9);
			$font->color('#4e4e4e');
			$font->valign('top'); 
		});

		//$img->save('https://matchmakerz.s3.ap-south-1.amazonaws.com/static/matchmakerz/id_card/id_card'.$user->id.'.jpg',80);
		Storage::disk('s3')->put('static/matchmakerz/id_card/'.'Id_card'.$user->id.'.jpg',$img->stream() , 'public');
		Storage::disk('s3')->put('static/matchmakerz/id_card/'.'Id_card'.$user->id, $img->stream(), 'public');

		IdCard::create([
			'id_card'=>'static/matchmakerz/id_card/'.'Id_card'.$user->id.'.jpg',
			'matchmaker_id'=>$user->id
			]);
			return response(['matchmaker_id_card'=>$this->url2.'Id_card'.$user->id.'.jpg','name'=>$name,'status'=>1,'message'=>'success'], 200);

			return response(['matchmaker_id_card'=>$this->url2.'Id_card'.$user->id,'name'=>$name,'status'=>1,'message'=>'success'], 200);

		}
			
		elseif($user == null)
			return response()->json(['status' => 0, 'message' => 'No User details found'], 200);
		else
			return response()->json(['status' => 0, 'message' => 'Some Error Occurred'], 200);
	}
         public function getOffers(Request $request){
		$user=app('App\Http\Controllers\ApiTokenController')->authenticate($request);
		if($user==null){
			return response()->json(['message'=>'Token Invalid'],200);
		}
		try {
			$offers = Offer::all();
			return response()->json(['offers'=>$offers,'status'=>1], 200);
		} catch(\Exception $e) {
			return response()->json(['status' => 0, 'message' => $e->getMessage()], 200);
		}
	}

	public function withdrawlrequest(Request $request){
		$user = app('App\Http\Controllers\ApiTokenController')->authenticate($request);
		if($user == null)
			return response()->json(['message' => 'Token Invalid'], 200);

		try{
		$matchmaker = MatchmakerUser::where('id',$user->id)
		->update(['withdrawlrequest' => 1]);
		return response()->json(['status' => 1, 'message' => 'Done'],200);
		}
		catch(\Exception $e){
			return response()->json(['status' => 0, 'message' => $e->getmessage()],200);
		}
	}

	public function getTips (Request $request){
		$user=app('App\Http\Controllers\ApiTokenController')->authenticate($request);
		if($user==null){
			return response()->json(['message'=>'Token Invalid'],200);
		}
		try {
			$offers = Tip::all();
			return response()->json(['tips'=>$offers,'status'=>1], 200);
		} catch(\Exception $e) {
			return response()->json(['status' => 0, 'message' => 'Some Error Occurred'], 200);
		}
	}


	//Bank Details of Matchmaker

	public function bankdetails(Request $request){
		$user = app('App\Http\Controllers\ApiTokenController')->authenticate($request);
		if($user == null)
			return response()->json(['message' => 'Token Invalid'], 200);
		$bank_name=$request->bank_name;
		$ifsc_code=$request->ifsc_code;
		$account_number=$request->account_number;
		$accout_name=$request->name;
		$paytm = $request->paytm;
		$upi = $request->upi;
		try {
			$details = BankDetails::create([
				'matchmaker_id' => $user->id,
				'bank_name' => $bank_name,
				'ifsc_code' => $ifsc_code,
				'account_number' => $account_number,
				'account_name' => $accout_name,
				'paytm' => $paytm,
				'upi' => $upi
			]);
			if($details)
				return response()->json(['status' => 1, 'message' => 'Bank Details Added', 'data' => $details], 200);
			else
				return response()->json(['status' => 0, 'message' => 'Details Not Added'], 200);
		} catch(\Exception $e) {
			return response()->json(['status' => 0, 'message' => 'Error Occurred', "stats" => $e], 200);
		}
	}


		public function updatebankdetails(Request $request){
		$user = app('App\Http\Controllers\ApiTokenController')->authenticate($request);
		if($user == null)
			return response()->json(['message' => 'Token Invalid'], 200);
			$user = BankDetails::where('matchmaker_id', $user->id)->first();
			if($user) {
				$user->bank_name = $request->bank_name;
				$user->ifsc_code = $request->ifsc_code;
				$user->account_number = $request->account_number;
				$user->account_name = $request->name;
				$user->paytm = $request->paytm;
				$user->upi = $request->upi;
				$user->save();
				return response()->json(['status' => 1, 'message' => 'Details Updated'], 200);
			}
			else
				return response()->json(['status' => 0, 'message' => 'Coudn\'t Update'], 200);
	}

	public function getbankdetails(Request $request){
		$user = app('App\Http\Controllers\ApiTokenController')->authenticate($request);
		if($user == null)
			return response()->json(['message' => 'Token Invalid'], 200);
		$bank_details= BankDetails::where('matchmaker_id',$user->id)->first();
		if($bank_details){
			return response()->json(['details'=>$bank_details,'status' => 'success'],200);
		}
		else
			return response()->json(['message' => 'no details found','status' => 'failed'],200);

	}

	//check mobile number for matchmaker
	public function numberCheck(Request $request)
	{  
		$phone_number = $request->phone_number;
		if($phone_number == null)
			return response()->json(['status' => 0, 'message' => 'Phone Number Required', 'auth_token' => null], 200);
		else {
			$user = MatchmakerUser::where('phone_number', $phone_number)
			->where('is_active', 1)->where('is_deleted', 0)
			->first();
			if($user) {
				if($user->is_active) {
					$auth_token = Token::where('user_id', $user->id)->first();
					return response()->json(['status' => 1, 'message' => 'Account Approved', 'auth_token' => $auth_token->key], 200);
				}
				else
					return response()->json(['status' => 0, 'message' => 'Account Not Approved', 'auth_token' => null], 200);
			}
			elseif($user == null)
				return response()->json(['status' => 0, 'message' => 'Number not registered', 'auth_token' => null], 200);
			else
				return response()->json(['status' => 0, 'message' => 'Some error occurred', 'auth_token' => null], 200);
		}
	}

	//check email
	public function emailCheck(Request $request)
	{
		$email = $request->email;
		if($email) {
			$user = MatchmakerUser::where('email', $email)->first();
			if(!$user)
				return response()->json(['status' => 0, 'message' => 'Email doesn\'t exist '], 200);
			else
				return response()->json(['status' => 1, 'message' => 'Account with this email already exist'], 200);
		}
		else
			return response()->json(['status' => 2, 'message' => 'Email Required'], 200);
	}

	//function to send otp when attempting login
	public function loginOtp(Request $request)
	{
		$type = $request->type;
		$phone_number = $request->phone_number;
		$otp = rand(100000, 999999);
		$otp_expire = date("Y-m-d H:i:s", strtotime("+10 minute")+19800);
		$hashed= Otp::where('mobile_number','hash')->first();
		$hash = $hashed->otp;
		if($type == 0)
			$otp_text = "<#> ".$otp." is your OTP(One Time Password) for logging into the Matchmaker App.".$hash;
		else
			$otp_text = "<#> ".$otp." is your OTP(One Time Password) for logging into the Matchmaker App.".$hash;
		$matchmaker = DB::table('matchmaker_user')->where('phone_number', $phone_number)->first();
		if($matchmaker) {
			DB::table('matchmaker_user')->where('phone_number', $phone_number)
			->update([
				'otp' => $otp,
				'otp_expire' => $otp_expire,
			]);
			$response = $this->sendOtp($phone_number, $otp_text);
			if($response == '') {
				return response()->json(['status' => 0, 'message' => 'Some Technical error occured'], 200);
			}
			else
				return response()->json(['status' => 1, 'message' => 'OTP Sent'], 200);
		}
		else {
			Otp::updateOrCreate(
				['mobile_number' => $phone_number],
				['otp' => $otp, 'otp_verify' => 0, 'otp_expire' => $otp_expire]
			);
			$response = $this->sendOtp($phone_number, $otp_text);
			if($response == '') {
				return response()->json(['status' => 0, 'message' => 'Some Technical error occured'], 200);
			}
			else
				return response()->json(['status' => 2, 'message' => 'No User Registered, Verify OTP for registration'], 200);
		}
	}

	public function sendOtp($number, $text)
	{
		$message = urlencode($text);
		$mobiles = '+91'.substr($number, -10);
		$username=env('USER_NAME');
		$password=env('PASSWORD');
		$smsurl='https://dqell.api.infobip.com/sms/1/text/query?username=MY_USERNAME&password=MY_PASSWORD&to=PHONE&text=MESSAGE';
		$smsurl=str_replace('MY_USERNAME',$username,$smsurl);
		$smsurl=str_replace('MY_PASSWORD',$password,$smsurl);
		$smsurl=str_replace('PHONE',$mobiles,$smsurl);
		$smsurl=str_replace('MESSAGE',$message,$smsurl);
		try{
			$result=file_get_contents($smsurl);
			return response()->json(['status' => 1, 'message' => $smsurl], 200);
		}
		catch(\Exception $e){
			return response()->json(['status' => 0, 'message' => 'Some Error Occurred'], 400);
		}
	}

		public function trueLogin(Request $request){
		$phone_number = $request->phone_number;
		if($phone_number == null){
			return response()->json(['status' => 0, 'message' =>'Null field encountered'],200);
		}
		else{
			$user_check = DB::table('matchmaker_user')->where('phone_number', $phone_number)
			->where('is_deleted', False)->first();
			if($user_check) {
				$user = DB::table('matchmaker_user')->where('phone_number', $phone_number)
				->where('is_deleted', 0)
				->first();
				if($user) {
					if($user->is_active == 0)
						return response()->json(['status' => 2, "message" => "User Not Verified, contact admin for verification"], 200);

					elseif($user->is_active == 1){
						$token = Token::firstOrCreate(
							['user_id' => $user->id],
							['key' => $this->generateToken(), 'created' => date("Y-m-d H:i:s", time()+19800)]
						);
						DB::table('matchmaker_user')->where('id', $user->id)
						->update(['last_login' => date('Y-m-d H:i:s', time()+19800)]);
						return response()->json(['status' => 1, 'token' => $token->key, "type" => $user->matchmaker_type], 200);
					}

					else
						return response()->json(['status' => 0, 'message' => 'Some Error Occurred'], 200);
				}
				else
					return response()->json(['status' => 3, 'message' => 'OTP Expired'], 200);
			}

			else
				return response()->json(['status' => 3, 'message' => 'Some Error Occured'],200);
		}
	}


	//function tp login the user
	public function login(Request $request)
	{
		$phone_number = $request->phone_number;
		$otp = $request->otp;
		if($phone_number == null || $otp == null) {
			return response()->json(['status' => 0, 'message' => 'Please provide both phone number and OTP(One Time Password)'], 200);
		}
		else {
			$user_check = DB::table('matchmaker_user')->where('phone_number', $phone_number)
			->where('is_deleted', False)->first();
			if($user_check) {
				$user = DB::table('matchmaker_user')->where('phone_number', $phone_number)
				->where('otp', $otp)
				->where('otp_expire', '>=', date("Y-m-d H:i:s", time()+19800))
				->where('is_deleted', 0)
				->first();
				if($user) {
					if($user->is_active == 0)
						return response()->json(['status' => 2, "message" => "User Not Verified, contact admin for verification"], 200);

					elseif($user->is_active == 1){
						$token = Token::firstOrCreate(
							['user_id' => $user->id],
							['key' => $this->generateToken(), 'created' => date("Y-m-d H:i:s", time()+19800)]
						);
						DB::table('matchmaker_user')->where('id', $user->id)
						->update(['last_login' => date('Y-m-d H:i:s', time()+19800)]);
						return response()->json(['status' => 1, 'token' => $token->key, "type" => $user->matchmaker_type], 200);
					}

					else
						return response()->json(['status' => 0, 'message' => 'Some Error Occurred'], 200);
				}
				else
					return response()->json(['status' => 0, 'message' => 'OTP Expired'], 200);
			}

			elseif ($user_check == null) {
				$otp = Otp::where('mobile_number', $phone_number)
				->where('otp', $otp)
				->where('otp_expire', '>=', date("Y-m-d H:i:s", time()+19800))
				->where('otp_verify', 0)
				->first();
				if($otp) {
					Otp::where('id', $otp->id)->update(['otp_verify' => 1]);
					return response()->json(['status' => 3, 'message' => 'OTP Verified'], 200);
				}
				elseif($otp == null)
					return response()->json(['status' => 0, 'message' => 'OTP Does not Match'], 200);
				else
					return response()->json(['status' => 0, 'message' => 'Some Error Occured'], 200);
			}

			else
				return response()->json(['status' => 0, 'message' => 'Some Error Occured'], 200);
		}
	}

	//generating random token
	public function generateToken()
	{
		$len = rand(35, 40);
		$token = '';
		$chars = "abcdefghijklmnopqrstuwxyz0123456789";
		$alphaLength = strlen($chars) - 1;
		for($i = 1; $i <= $len; $i++) {
			$token .= $chars[rand(0, $alphaLength)];
		}
		return $token;
	}

	public function createMatchmaker(Request $request)
	{
		$message = '';
		$matchmaker_create = '';
		$user = $request->all();
		$unique_referral_code = $this->generateReferralCode($user['first_name']);

		try {
			$is_active = $is_staff = 0;
			if($user['password']) {
				if(strpos($user['password'], "XXX") == 0) {
					Otp::where('mobile_number', $user['phone_number'])->update(['otp_verify' => 1]);
					$is_active = $is_staff = 1;
				}
			}
			$otp = Otp::where('mobile_number', $user['phone_number'])->where('otp_verify', 1)->first();

			if(MatchmakerUser::where('phone_number', $user['phone_number'])->doesntExist()) {	

			if(Hiring::where('mobile',$user['phone_number'])->exists()){
					$hire = Hiring::where('mobile', $user['phone_number'])->update(['is_done' => 1]);
				}

				if(strtoupper($user['referred_by']) != 'NA') {
					$parent = MatchmakerUser::where('unique_referral_code', $user['referred_by'])->get();
					if($parent->count() == 1)
						$parent_id = $parent[0]->id;
					else {
						return response()->json(['status' => 0, 'message' => 'invalid referral code'], 200);
					}
				}

				else
					$parent_id = null;

				$matchmaker_create = MatchmakerUser::create([
					'phone_number' => $user['phone_number'],
					'email' => '',
					'first_name' => $user['first_name'],
					'last_name' => $user['last_name'],
					'password' => '',
					'unique_referral_code' => $unique_referral_code,
					'about' => $user['about'],
					'unique_about' => $user['unique_about'],
					'specialization' => $user['specialization'],
					'gender' => $user['gender'],
					'age' => $user['age'],
					'location' => '',
					'latitude' => '',
					'longitude' => '',
					'parent_id' => $parent_id,
					'is_active' => $is_active,
					'is_staff' => $is_staff,
					'is_superuser' => 0,
					'referred_by' => $user['referred_by'],
					'whatsapp_number' => $user['whatsapp_number'],
					'matchmaker_type' => $user['matchmaker_type'],
				]);

				$user_data = MatchmakerUser::where('phone_number', $user['phone_number'])->first();
				$token = Token::firstOrCreate(
					['user_id' => $user_data->id],
					['key' => $this->generateToken(), 'created' => date("Y-m-d H:i:s", time()+19800)]
				);

				//deleting from the hiring lead
				if(Hiring::where('mobile', 'LIKE', '%'.substr($user_data->phone_number, -10).'%')->exists()) {
					Hiring::where('mobile', 'LIKE', '%'.substr($user_data->phone_number, -10).'%')->delete();
				}

				if($user['matchmaker_type'] == 0) {
					$plans_hans_matrimony = MatchmakerUser::where('id', 82)->first();
					$plans = Plans::where('matchmaker_id', $plans_hans_matrimony->id)->get();
					foreach ($plans as $plan) {
						Plans::create([
							'matchmaker_id' => $user_data->id,
							'plan_name' => $plan->plan_name,
							'meeting_charge' => $plan->meeting_charge,
							'before_roka_charge' => $plan->before_roka_charge,
							'meetings' => $plan->meetings,
							'validity' => $plan->validity,
							'min_family_income' => $plan->min_family_income,
							'max_family_income' => $plan->max_family_income,
						]);
					}
				}

				if($parent_id!=null) {
					$childs = explode(',', $parent[0]->child);
					array_push($childs, $user_data->id);
					$parent[0]->child = implode(',', $childs);
					if($parent[0]->matchmaker_type == 1) {
						$parent[0]->credits += 200;
						Transaction::create([
							'matchmaker_id' => $parent[0]->id,
							'referr_Credits' => $unique_referral_code,
							'created_at' => Carbon::now(),
						]);
					}
					$parent[0]->save();
				}

				$message = 'Account Created';
			}
			else
				$message = 'Mobile Number already exist';
			if($matchmaker_create != '')
				return response()->json(['status' => 1, 'message' => $message, 'auth_token' => $token->key], 200);
			else
				return response()->json(['status' => 0, 'message' => $message], 200);

		} catch(\Exception $e) {
			return response()->json(['status' => 2, 'message' => $e->getMessage()], 200);
		}
	}

	//function to provide number based on matchmaker level
	public function phoneNumberBasedOnLevel(Request $request){
		$user = app('App\Http\Controllers\ApiTokenController')->authenticate($request);
		if($user == null)
			return response()->json(['message' => 'Token Invalid'], 200);
		else{
			return response()->json(['phone_no'=>'+918178918190'],200);
		}	
	}

	public function generateReferralCode($name)
	{
		$i = 0;
		$len = strlen($name) + 3;
		$unique = false;
		$max_tries = 32;
		$char = '0123456789';
		$unique_referral_code = '';
		while(!$unique) {
			if($i < $max_tries) {
				$new_code = $name;
				for($j=0; $j<3; $j++)
					$new_code .= $char[rand(0, strlen($char)-1)];
				if(MatchmakerUser::where('unique_referral_code', $new_code)->doesntExist()){
					$unique_referral_code = $new_code;
					$unique = True;
				}
				$i++;
			}
		}
		return $unique_referral_code;

	}

	//checking if a valid referral code
	public function referralCodeCheck(Request $request)
	{
		$referral_code = $request->referral_code;
		if($referral_code) {
			$user = MatchmakerUser::where('unique_referral_code', $referral_code)->where('is_deleted', 0)->first();
			if($user)
				return response()->json(['status' => 1, 'message' => 'Valid Referral Code'], 200);
			else
				return response()->json(['status' => 0, 'message' => 'No User Exist'], 200);
		}
		else
			return response()->json(['status' => 0, 'message' => 'Referral Code cannot be empty'], 200);
	}

	//deleting token on logout attempt
	public function logout(Request $request)
	{
		$user = app('App\Http\Controllers\ApiTokenController')->authenticate($request);
		if($user == null)
			return response()->json(['message' => 'Token Invalid'], 200);

		$token = $request->auth_token;
		Token::where('token', $token)->delete();
		return response()->json(['status' => 1, 'message' => 'Logged Out'], 200);
	}

	//function to send matchmaker data
	public function getProfile(Request $request)
	{
		$user = app('App\Http\Controllers\ApiTokenController')->authenticate($request);
		if($user == null)
			return response()->json(['message' => 'Token Invalid'], 200);

		$user = MatchmakerUser::where('id', $user->id)->where('phone_number', $user->phone_number)->first();
		if($user)
			return response($user, 200);
		elseif($user == null)
			return response()->json(['status' => 0, 'message' => 'No User details found'], 200);
		else
			return response()->json(['status' => 0, 'message' => 'Some Error Occurred'], 200);
	}

	//function to update matchmaker profile
	public function updateProfile(Request $request)
	{
		$user = app('App\Http\Controllers\ApiTokenController')->authenticate($request);
		if($user == null)
			return response()->json(['message' => 'Token Invalid'], 200);

		if($request->upfront_charge == "")
			$upfront_charge = 3100;
		else
			$upfront_charge = $request->upfront_charge;

		$user = MatchmakerUser::where('id', $user->id)->first();
		if($user) {
			$user->first_name = $request->first_name;
			$user->last_name = $request->last_name;
			$user->phone_number = $request->phone_number;
			$user->email = $request->email;
			$user->about = $request->about;
			$user->unique_about = $request->unique_about;
			$user->specialization = $request->specialization;
			$user->gender = $request->gender;
			$user->age = $request->age;
			$user->experience = $request->experience;
			$user->whatsapp_number = $request->whatsapp_number;
			$user->upfront_charge = $upfront_charge;
			$user->save();
			return response()->json(['status' => 1, 'message' => 'Profile Updated'], 200);
		}
		else
			return response()->json(['status' => 0, 'message' => 'Coudn\'t Update'], 200);
	}

	//delete matchmaker profile
	public function deleteProfile(Request $request)
	{
		$user = app('App\Http\Controllers\ApiTokenController')->authenticate($request);
		if($user == null)
			return response()->json(['message' => 'Token Invalid'], 200);

		$phone_number = $request->phone_number;
		try {
			if($user->is_staff) {
				MatchmakerUser::where('phone_number', $phone_number)->delete();
				return response()->json(['status' => 1, 'message' => 'User Deleted'], 200);
			}
			else
				return response()->json(['status' => 0, 'message' => 'You don\'t have permission to perform this operation.'], 200);
		} catch (\Exception $e) {
			return response()->json(['status' => 0, 'message' => 'Couldn\'t Delete', 'phone_number' => $phone_number], 200);
		}
	}

	public function totalClients(Request $request)
	{
		$user = app('App\Http\Controllers\ApiTokenController')->authenticate($request);
		if($user == null)
			return response()->json(['message' => 'Token Invalid'], 200);

		try {
			$count = ClientProfile::where('matchmaker_id', $user->id)->count();
			return response()->json(['status' => 1, 'clients' => $count], 200);
		} catch (\Exception $e) {
			return response()->json(['status' => 0, 'clients' => 0], 200);
		}
	}

	//return matchmaker tutorials
	public function tutorials()
	{
		$tutorials = Tutorial::all();
		return response($tutorials, 200);
	}

	//update location of matchmaker
	public function updateLocation(Request $request)
	{
		$user = app('App\Http\Controllers\ApiTokenController')->authenticate($request);
		if($user == null)
			return response()->json(['message' => 'Token Invalid'], 200);

		$location = $request->location;
		$latitude = $request->latitude;
		$longitude = $request->longitude;
		try {
			MatchmakerUser::where('id', $user->id)->update([
				'location' => $location,
				'latitude' => $latitude,
				'longitude' => $longitude
			]);
			return response()->json(['status' => 1, 'message' => 'Location updated'], 200);
		} catch(\Exception $e) {
			return response()->json(['status' => 0, 'message' => 'Couldn\'t update'], 200);
		}
	}

	//upload profile pic for matchmaker
	public function uploadProfilePic(Request $request)
	{
		$user = app('App\Http\Controllers\ApiTokenController')->authenticate($request);
		if($user == null)
			return response()->json(['message' => 'Token Invalid'], 200);

		try {
			if($request->file('profile_pic') == null) {
				throw new RaiseException("Empty Content", 200);
			}
			else {
				$profile_pic = $request->file('profile_pic');
				$extension = $profile_pic->extension();
				if($extension != 'jpg' && $extension != 'jpeg' && $extension != 'png')
					return response()->json(['status' => 0, 'message' => 'Please upload only jpg/jpeg/png file'], 200);
				else {
					$filename = 'mmProfilePic_'.$user->id.'.jpg';
					Storage::disk('s3')->put('static/matchmakerz/profile_pic/'.$filename, file_get_contents($profile_pic), 'public');
					MatchmakerUser::where('id', $user->id)->update(['profile_pic' => 'static/matchmakerz/profile_pic/'.$filename]);
					return response()->json(['status' => 1, 'message' => 'Profile Picture uploaded',
						'profile_pic' => $this->url.$filename], 201);
				}
			}
		} 
		catch (\Exception $e) {
			return response()->json(['status' => 0, 'message' => 'Some error occurred'], 200);
		}
	}

	// return all the plans for existing matchmaker
	public function getPlansForExisting(Request $request)
	{
		$user = app('App\Http\Controllers\ApiTokenController')->authenticate($request);
		if($user == null)
			return response()->json(['message' => 'Token Invalid'], 200);

		try {
			$plans = PlansForExisting::all();
			return response()->json(['status' => 1, 'data' => $plans], 200);
		} catch(\Exception $e) {
			return response()->json(['status' => 0, 'message' => 'Some error occurred'], 200);
		}
	}

	//create plan for existing matchmaker
	public function createPlansForExisting(Request $request)
	{
		$user = app('App\Http\Controllers\ApiTokenController')->authenticate($request);
		if($user == null)
			return response()->json(['message' => 'Token Invalid'], 200);

		$plan_name = $request->plan_name;
		$no_of_clients = $request->number_of_clients;
		$description = $request->description;
		$amount = $request->amount;
		$credits = $request->credits;
		$additional_features = $request->additional_features;
		$validity = $request->validity;
		$platforms = $request->platforms;
		try {
			$plan = PlansForExisting::create([
				'plan_name' => $plan_name,
				'number_of_clients' => $no_of_clients,
				'description' => $description,
				'amount' => $amount,
				'credits' => $credits,
				'additional_features' => $additional_features,
				'validity' => $validity,
				'platforms' => $platforms,
			]);
			if($plan)
				return response()->json(['status' => 1, 'message' => 'Plan Created', 'data' => $plan], 200);
			else
				return response()->json(['status' => 0, 'message' => 'Plan not created'], 200);
		} catch(\Exception $e) {
			return response()->json(['status' => 0, 'message' => 'Plans Error Occurred', "stats" => $e], 200);
		}
	}

	//update plans for existing matchmaker
	public function updatePlansForExisting(Request $request)
	{
		$user = app('App\Http\Controllers\ApiTokenController')->authenticate($request);
		if($user == null)
			return response()->json(['message' => 'Token Invalid'], 200);

		$plan_id = $request->id;
		$plan_name = $request->plan_name;
		$no_of_clients = $request->number_of_clients;
		$description = $request->description;
		$amount = $request->amount;
		$credits = $request->credits;
		$additional_features = $request->additional_features;
		$validity = $request->validity;
		$platforms = $request->platforms;

		try {
			$plan = PlansForExisting::where('id', $plan_id)->update([
				'plan_name' => $plan_name,
				'number_of_clients' => $no_of_clients,
				'description' => $description,
				'amount' => $amount,
				'credits' => $credits,
				'additional_features' => $additional_features,
				'validity' => $validity,
				'platforms' => $platforms,
			]);
			if($plan)
				return response()->json(['status' => 1, 'message' => 'Plan Updated'], 200);
			else
				return response()->json(['status' => 0, 'message' => 'Plan Not Updated'], 200);
		} catch(\Exception $e) {
			return response()->json(['status' => 0, 'message' => 'Some Error Occurred'], 200);
		}
	}

	public function getPlans(Request $request)
	{
		$user = app('App\Http\Controllers\ApiTokenController')->authenticate($request);
		if($user == null)
			return response()->json(['message' => 'Token Invalid'], 200);

		$matchmaker = MatchmakerUser::where('id', $user->id)->first();
		if($matchmaker->matchmaker_type == 0) {
			//$plans = Plans::where('matchmaker_id', $user->id)->orderBy('meeting_charge')->get()->toArray();
			$plans=Plans::all();
		}
		else {
//			$plans = PlansForExisting::all();
			$plans=Plans::all();
		}
		return response($plans, 200);
	}

	public function createPlans(Request $request)
	{
		$user = app('App\Http\Controllers\ApiTokenController')->authenticate($request);
		if($user == null)
			return response()->json(['message' => 'Token Invalid'], 200);

		$current_user = MatchmakerUser::where('id', $user->id)->first();
		if($current_user->matchmaker_type == 0) {
			$plan_name = $request->plan_name;
			$meeting_charge = $request->meeting_charge;
			$before_roka_charge = $request->before_roka_charge;
			$meetings = $request->meetings;
			$validity = $request->validity;
			$min_family_income = $request->min_family_income;
			$max_family_income = $request->max_family_income;

			try {
				$plan = Plans::create([
					'matchmaker_id' => $user->id,
					'plan_name' => $plan_name,
					'meeting_charge' => $meeting_charge,
					'before_roka_charge' => $before_roka_charge,
					'meetings' => $meetings,
					'validity' => $validity,
					'min_family_income' => $min_family_income,
					'max_family_income' => $max_family_income,
				]);
				if($plan)
					return response()->json(['status' => 1, 'message' => 'Plan Created'], 200);
				else
					return response()->json(['status' => 0, 'message' => 'Plan not created'], 200);
			} catch(\Exception $e) {
				return response()->json(['status' => 0, 'message' => 'Some Error Occurred'], 200);
			}
		}
	}

	public function updatePlans(Request $request)
	{
		$user = app('App\Http\Controllers\ApiTokenController')->authenticate($request);
		if($user == null)
			return response()->json(['message' => 'Token Invalid'], 200);

		$current_user = MatchmakerUser::where('id', $user->id)->first();
		if($current_user->matchmaker_type == 0) {
			$plan_id = $request->plan_id;

			try {
				$plan = Plans::where('id', $plan_id)->update([
					'matchmaker_id' => $user->id,
					'plan_name' => $request->plan_name,
					'meeting_charge' => $request->meeting_charge,
					'before_roka_charge' => $request->before_roka_charge,
					'meetings' => $request->meetings,
					'validity' => $request->validity,
					'min_family_income' => $request->min_family_income,
					'max_family_income' => $request->max_family_income,
				]);
				if($plan)
					return response()->json(['status' => 1, 'message' => 'Plan Updated'], 200);
				else
					return response()->json(['status' => 0, 'message' => 'Plan not updated'], 200);
			} catch(\Exception $e) {
				return response()->json(['status' => 0, 'message' => 'Some Error Occurred'], 200);
			}
		}
	}

	public function checkSuspension(Request $request)
	{
		$user = app('App\Http\Controllers\ApiTokenController')->authenticate($request);
		if($user == null)
			return response()->json(['message' => 'Token Invalid'], 200);

		$grace_time = Carbon::now()->subdays(7);
		$payment = ClientPayment::where('matchmaker_id', $user->id)
		->where('payment_status', 0)
		->where('paid_on', '<=', $grace_time)
		->count();

		if($payment == 0)
			return response()->json(['status' => 1, 'message' => 'Account is Active'], 200);
		else
			return response()->json(['status' => 0, 'message' => 'Acount Has been Suspended'], 200);
	}

	public function outstandingAmount(Request $request)
	{
		$user = app('App\Http\Controllers\ApiTokenController')->authenticate($request);
		if($user == null)
			return response()->json(['message' => 'Token Invalid'], 200);

//		$grace_time = Carbon::now()->subdays(7);
		$outstanding_amount = ClientPayment::where('matchmaker_id', $user->id)
		->where('payment_status', 0)
		->sum('upfront_charge');

		if($outstanding_amount == 0)
			return response()->json(['status' => 1, 'message' => 'No Outstanding Amount', 'amount' => 0], 200);
		else {
			$days_remaining = ClientPayment::where('matchmaker_id', $user->id)
			->where('payment_status', 1)
			->orderBy('paid_on', 'desc')
			->first();
			$diff = date("Y-m-d", time()+19800) - date("Y-m-d", strtotime($days_remaining->paid_on));
			$days_left = 7 - date("d", $diff);
			return response()->json(['status' => 1, 'message' => 'Outstanding Amount', 'amount' => $outstanding_amount/2, 'days_left' => $days_left]);
		}
	}

	public function addPayment(Request $request)
	{
		$user = app('App\Http\Controllers\ApiTokenController')->authenticate($request);
		if($user == null)
			return response()->json(['message' => 'Token Invalid'], 200);

		$client_id = $request->client_id;
		$plan_id = $request->plan_id;
		$payment_for = $request->payment_for;
		$payment_amount = $request->payment_amount;
		$payment_gateway = 'razorpay';
		$payment_status = 0;
		$payment_gateway_status = 'failure';
		$pay_mode = 'offline';
		$upfront_charge = $request->upfront_charge;
		$meeting_charge = $request->meeting_charge;
		$before_roka_charge = $request->before_roka_charge;

		try {
			$client = ClientProfile::where('id', $client_id)->first();
			$plan = Plans::where('id', $plan_id)->first();
			$create_payment = ClientPayment::create([
				'client_id' => $client->id,
				'plan_id' => $plan->id,
				'matchmaker_id' => $user->id,
				'payment_for' => $payment_for,
				'payment_amount' => $payment_amount,
				'payment_gateway' => $payment_gateway,
				'payment_status' => $payment_status,
				'payment_gateway_status' => $payment_gateway_status,
				'pay_mode' => $pay_mode,
				'upfront_charge' => $upfront_charge,
				'meeting_charge' => $meeting_charge,
				'before_roka_charge' => $before_roka_charge,
				'paid_on' => Carbon::now()->addSeconds(19800),
				'expire_on' => Carbon::now()->addSeconds(19800)->addYear(),
			]);

			if($create_payment) {
				$link = $this->createPlanSubscription($plan, $upfront_charge, $meeting_charge, $create_payment->id);
				ClientProfile::where('id', $client_id)->update([
					'is_active' => 1,
					'amount_fix' => 1,
					'payment_status' => $payment_status
				]);
				$this->sendTextMessage($client->phone_number, $plan, $upfront_charge, $meeting_charge, $before_roka_charge, $link);
				return response()->json(['status' => 1, 'message' => 'Reciept details added successfully'], 200);
			}
			else
				return response()->json(['status' => 0, 'message' => 'Payment Add Not Successful'], 200);
		} catch(\Exception $e) {
			if(ClientProfile::where('id', $client_id)->doesntExist())
				return response()->json(['status' => 0, 'message' => 'No Client Found'], 200);
			else
				return response()->json(['status' => 0, 'message' => 'Some error occurred'], 200);
		}
	}

	public function sendTextMessage($reciever, $plan, $upfront_charge, $meeting_charge, $before_roka_charge, $link)
	{
		$sms = "Thank you for registering with us.\nYou have opted ".$plan->plan_name." Plan wherein Upfront Charge, Marriage Fixing Charge and Monthly Subscription Charge are Rs.". $upfront_charge.", Rs.".$before_roka_charge." and Rs.".$meeting_charge. " respectively.\nBelow is your subscription link, click to complete the payment\n".$link;
		$mobiles = '+91'.substr($reciever, -10);
		$username=env('USER_NAME');
		$password=env('PASSWORD');
		$smsurl='https://dqell.api.infobip.com/sms/1/text/query?username=MY_USERNAME&password=MY_PASSWORD&to=PHONE&text=MESSAGE';
		$smsurl=str_replace('MY_USERNAME',$username,$smsurl);
		$smsurl=str_replace('MY_PASSWORD',$password,$smsurl);
		$smsurl=str_replace('PHONE',$mobiles,$smsurl);
		$smsurl=str_replace('MESSAGE',$sms,$smsurl);
		try{
			$result=file_get_contents($smsurl);
		}
		catch(\Exception $e){
		}
	}

	public function addbonus(Request $request){
		$user = app('App\Http\Controllers\ApiTokenController')->authenticate($request);
		if($user == null)
			return response()->json(['message' => 'Token Invalid'], 200);
		try{
			$count = ClientProfile::where('matchmaker_id',$user->id)->count();
			if($count == 0){
				$bonus=15;
			}
			else{
			$bonus = rand(7,10);
			}
			$client = ClientProfile::where('id', $request->id)->first();
			$matchmaker = MatchmakerUser::where('id',$client->matchmaker_id)->first();
			if($matchmaker->phone_number == '8178918190'){
			   $bonus = rand(100,200);
			}
			$client->bonus = $bonus;
			$client->save();
			return response()->json(['status' => 1,'bonus'=>$bonus, 'message' => 'Bonus Added','count'=>$count], 200);
		} catch(\Exception $e) {
			return response()->json(['status' => 0, 'message' => $e->getmessage()], 200);
		}
	}

	public function wallet(Request $request)
	{
		$user = app('App\Http\Controllers\ApiTokenController')->authenticate($request);
		if($user == null)
			return response()->json(['message' => 'Token Invalid'], 200);

		try {
			if($user->matchmaker_type == 0) {
				$upfront_charge = ClientPayment::where('matchmaker_id', $user->id)
				->where('payment_status', 1)
				->sum('upfront_charge');
				// $roka_charges = ClientPayment::where('matchmaker_id', $user->id)
				// ->where('payment_status', 1)
				// ->sum('before_roka_charge');
				$outstanding_amount = ClientPayment::where('matchmaker_id', $user->id)
				->where('payment_status', 0)
				->sum('upfront_charge');

				$user = MatchmakerUser::where('id',$user->id)->first();
				$bonus_amount = $user->client_bonus;

				if($user->parent) {
					$upfront_charge = (float)($upfront_charge*.30);
					// $roka_charges = (float)($roka_charges*.40);
				}
				else {
					$upfront_charge = (float)($upfront_charge*.40);
					// $roka_charges = (float)($roka_charges*.50);
				}
				$total_wallet_balance = (float)($upfront_charge);

				//first child of parent
				$referred_child_id = MatchmakerUser::where('parent_id', $user->id)->pluck('id');
				$referred_child = MatchmakerUser::where('parent_id', $user->id)->get();
				foreach ($referred_child as $child) {
					$child_upfront_charge = ClientPayment::where('matchmaker_id', $child->id)->where('payment_status', 1)
					->sum('upfront_charge');
					// $child_roka_charge = ClientPayment::where('matchmaker_id', $child->id)->where('payment_status', 1)
					// ->sum('before_roka_charge');

					// if($child->parent_id == $user->id && $child_roka_charge)
					// 	$roka_charges += (float)($child_roka_charges * .07);

					if($child->parent_id == $user->id && $child_upfront_charge)
						$upfront_charge += (float)($child_upfront_charges * .07);
				}

				//child of childs of parent matchmaker
				$child_of_childs = MatchmakerUser::whereIn('parent_id', $referred_child_id)->get();
				foreach ($child_of_childs as $child) {
					$child_upfront_charge = ClientPayment::where('matchmaker_id', $child->id)->where('payment_status', 1)
					->sum('upfront_charge');
					// $child_roka_charge = ClientPayment::where('matchmaker_id', $child->id)->where('payment_status', 1)
					// ->sum('before_roka_charge');

					// if($child->parent_id == $user->id && $child_roka_charge)
					// 	$roka_charges += (float)($child_roka_charges * .03);

					if($child->parent_id == $user->id && $child_upfront_charge)
						$upfront_charge += (float)($child_upfront_charges * .03);
				}

				if($total_wallet_balance == null)
					$total_wallet_balance = 0;
				if($upfront_charge == null)
					$upfront_charge = 0;
				if($outstanding_amount == null)
					$outstanding_amount = 0;
				
				if($bonus_amount == null)
					$bonus_amount = 0;

				$total_wallet_balance = $total_wallet_balance + $bonus_amount;

				$user->wallet=$total_wallet_balance;
				$user->save();

				return response()->json(['status' => 1, 'message' => 'Wallet Details', 'upfront_charge' => $upfront_charge,
					'payment_amount' => $total_wallet_balance, 'outstanding_amount' => $outstanding_amount], 200);
			}
			else
				return response()->json(['status' => 0, 'message' => 'Wallet is not for existing matchmakers.'], 200);
		} catch(\Exception $e) {
			return response()->json(['status' => 0, 'message' => $e->getmessage()], 200);
		}
	}

	public function updateDeviceID(Request $request)
	{
		$device_id = $request->device_id;
		$phone_number = $request->phone_number;
		$user =	MatchmakerUser::where('phone_number', $phone_number)->first();
		if($user) {
			$user->device_id = $device_id;
			$user->save();
			return response()->json(['status' => 1, 'message' => 'Device ID updated'], 200);
		}
		elseif(!$user)
			return response()->json(['status' => 0, 'message' => 'User not update'], 200);
		else
			return response()->json(['status' => 0, 'message' => 'Device ID Could Not update'], 200);
	}

	public function myReferrals(Request $request)
	{
		$user = app('App\Http\Controllers\ApiTokenController')->authenticate($request);
		if($user == null)
			return response()->json(['message' => 'Token Invalid'], 200);

		try {
			$referrals = MatchmakerUser::where('referred_by', $user->unique_referral_code)->get();
			return response($referrals, 200);
		} catch(\Exception $e) {
			return response()->json(['status' => 0, 'message' => 'Some error occurred'], 200);
		}
	}

	public function reviewCredits(Request $request)
	{
		$user = app('App\Http\Controllers\ApiTokenController')->authenticate($request);
		if($user == null)
			return response()->json(['message' => 'Token Invalid'], 200);

		$message = 'Thanks for your review.';
		$current_user = MatchmakerUser::where('id', $user->id)->first();
		if($current_user) {
			if($current_user->review == 0) {
				$current_user->review = 1;
				$current_user->save();
				if($current_user->matchmaker_type == 0)
					return response()->json(['status' => 1, 'message' => $message, 'User' => $current_user], 200);
				else {
					$current_user->credits += 200;
					$current_user->save();
					Transaction::create([
						'matchmaker_id' => $user->id,
						'review_Credits' => 1,
						'credits' => $current_user->credits,
					]);
					$message .= ' 200 credits have been added to your account.';
					return response()->json(['status' => 1, 'message' => $message, 'User' => $current_user], 200);
				}
			}
			else
				return response()->json(['status' => 1, 'message' => 'Already reviewed', 'User' => $current_user], 200);
		}

		elseif(!$current_user)
			return response()->json(['status' => 0, 'message' => 'User error occurred'], 200);

		else
			return response()->json(['status' => 0, 'message' => 'Some error occurred'], 200);
	}

	public function userData(Request $request)
	{
		$phone_number = $request->phone_number;
		$user = MatchmakerUser::where('phone_number', $phone_number)->first();
		$response = Token::where('user_id', $user->id)->first();
		return response($response->key, 200);
	}

	public function getBlankLeads(Request $request)
	{
		$user = app('App\Http\Controllers\ApiTokenController')->authenticate($request);
		if($user == null)
			return response()->json(['message' => 'Token Invalid'], 200);

		try {
			$matchmaker_blanklead = MatchmakerBlankLead::where('matchmaker_id', $user->id)->first();
			if(!$matchmaker_blanklead) {
				$matchmaker_blanklead = MatchmakerBlankLead::create([
					'matchmaker_id' => $user->id,
					'is_full' => false,
				]);
			}
			$blankleads_id = explode(',', $matchmaker_blanklead->leads);
			if(sizeof($blankleads_id) > 0) {
				if(sizeof($blankleads_id) < 40) {
					if($matchmaker_blanklead->leads == null)
						$blankleads = BlankLead::where('is_full', false)->take(2)->get();
					else
						$blankleads = BlankLead::where('is_full', false)->take(40-sizeof($blankleads_id))->get();

					foreach ($blankleads as $blanklead) {
						if($matchmaker_blanklead->leads == null)
							$matchmaker_blanklead->leads = $blanklead->id;
						else
							$matchmaker_blanklead->leads .= ','.$blanklead->id;
						if($blanklead->matchmaker == null)
							$blanklead->matchmaker = $matchmaker_blanklead->matchmaker_id;
						else
							$blanklead->matchmaker  .= ','.$matchmaker_blanklead->matchmaker_id;

					//	$no_of_matchmakers = sizeof(explode(',', $blanklead->matchmaker));
					//	if($no_of_matchmakers >= 2)
						$blanklead->is_full = true;
						$blanklead->save();
					}
				}
				$blankleads_id = explode(',', $matchmaker_blanklead->leads);
				if(sizeof($blankleads_id) > 40)
					$matchmaker_blanklead->is_full = true;
				$matchmaker_blanklead->save();

				$blank_lead = BlankLead::whereIn('id', $blankleads_id)->get();
				return response()->json(['status' => 1, 'blank_lead' => $blank_lead], 200);
			}
			else
				return response()->json(['status' => 0, 'message' => 'No Lead Added'], 200);
		} catch(\Exception $e) {
			return response()->json(['status' => 0, 'message' => 'Some error occurred'], 200);
		}
	}

	public function deleteBlankLead(Request $request)
	{
		$user = app('App\Http\Controllers\ApiTokenController')->authenticate($request);
		if($user == null)
			return response()->json(['message' => 'Token Invalid'], 200);
		try {
			$lead_id = $request->id;
			$blanklead = BlankLead::where('id', $lead_id)->first();
			$matchmakers_id = explode(',', $blanklead->matchmaker);
			foreach ($matchmakers_id as $matchmaker_id) {
				$matchmaker_blanklead = MatchmakerBlankLead::where('matchmaker_id', $matchmaker_id)->first();
				$blankleads_id = explode(',', $matchmaker_blanklead->leads);

				array_splice($blankleads_id, array_search($lead_id, $blankleads_id), 1);
				$matchmaker_blanklead->leads = implode(',', $blankleads_id);
				$matchmaker_blanklead->save();
			}

			$blanklead->delete();
			return response()->json(['status' => 1], 200);
		} catch(\Exception $e) {
			return response()->json(['status' => 0, 'message' => 'Some error occurred'], 200);
		}
	}

	public function createLead(Request $request)
	{
		$user = app('App\Http\Controllers\ApiTokenController')->authenticate($request);
		if($user == null)
			return response()->json(['message' => 'Token Invalid'], 200);

		$lead_id = $request->id;
		$phone_number = $request->phone_number;
		$name = $request->name;
		$comment = $request->comment;
		$follow_up_date = $request->date;
		$lead = Lead::where('phone_number', $phone_number)->first();
		if(!$lead) {
			$lead = Lead::create([
				'matchmaker_id' => $user->id,
				'name' => $name,
				'phone_number' => $phone_number,
				'leads' => $follow_up_date.":".$comment,
			]);
		}
		else
			return response()->json(['status' => 0, 'message' => 'already exists this lead'], 200);

		$blanklead = BlankLead::where('id', $lead_id)->first();
		$matchmakers_id = explode(',', $blanklead->matchmaker);
		foreach ($matchmakers_id as $id) {
			$matchmaker_blanklead = MatchmakerBlankLead::where('matchmaker_id', $id)->first();
			if(!$matchmaker_blanklead)
				$matchmaker_blanklead = MatchmakerBlankLead::where('id', $id)->first();
			$blankleads_id = explode(',', $matchmaker_blanklead->leads);

			array_splice($blankleads_id, array_search($lead_id, $blankleads_id), 1);
			$matchmaker_blanklead->leads = implode(',', $blankleads_id);
			$matchmaker_blanklead->save();
		}
		BlankLead::where('id', $lead_id)->delete();

//		$this->ReassignLeads($lead_id);
		return response()->json(['status' => 1], 200);
	}

	public function ReassignLeads($lead_id)
	{
		$blank_leads = BlankLead::where('is_full', 0)->where('id', '!=', $lead_id)->get();
		//assign new lead to matchmaker
		foreach ($matchmakers_id as $id) {
			$matchmaker_blanklead = MatchmakerBlankLead::where('matchmaker_id', $id)->first();
			foreach ($blank_leads as $blank_lead) {
				$blankleads_id = explode(',', $matchmaker_blanklead->leads);
				$matchmakers = explode(',', $blank_lead->matchmaker);
				if(!in_array($blank_lead->id, $blankleads_id) && !in_array($matchmaker_blanklead->matchmaker_id, $matchmakers) && sizeof($blankleads_id) < 40) {
					$blankleads_id[] = $blank_lead->id;
					if(sizeof($blankleads_id) == 40)
						$matchmaker_blanklead->is_full = true;

					$matchmaker_blanklead->leads = implode(',', $blankleads_id);
					$matchmaker_blanklead->save();

					$matchmakers[] = $matchmaker_blanklead->matchmaker_id;
					$blank_lead->matchmaker = implode(',', $matchmakers);
					$blank_lead->save();
				}

				if(sizeof($matchmakers) >= 2) {
					$blank_lead->is_full = 1;
					$blank_lead->save();
				}
			}
		}
	}

	public function matchmakerProfile(Request $request)
	{
		$id = $request->id;
		$user = MatchmakerUser::where('id', $id)->first();
		if($user)
			return response($user, 200);
		elseif($user == null)
			return response()->json(['status' => 0, 'message' => 'No User details found'], 200);
		else
			return response()->json(['status' => 0, 'message' => 'Some Error Occurred'], 200);
	}

	public function getMyleads(Request $request)
	{
		$user = app('App\Http\Controllers\ApiTokenController')->authenticate($request);
		if($user == null)
			return response()->json(['message' => 'Token Invalid'], 200);

		try {
			$my_lead = Lead::where('matchmaker_id', $user->id)->get();
			foreach ($my_lead as $lead) {
				$lead->leads = str_replace(',', "\n", $lead->leads);
			}
			return response($my_lead, 200);
		} catch(\Exception $e) {
			return response()->json(['status' => 0, 'message' => 'Some error occurred'], 200);
		}
	}

	//add lead if blank lead is not present
	public function createMyLead(Request $request)
	{
		$user = app('App\Http\Controllers\ApiTokenController')->authenticate($request);
		if($user == null)
			return response()->json(['message' => 'Token Invalid'], 200);

		try {
			$phone_number = $request->phone_number;
			$name = $request->name;
			$comment = $request->comment;
			$follow_up_date = $request->date;
			Lead::create([
				'matchmaker_id' => $user->id,
				'name' => $name,
				'phone_number' => $phone_number,
				'leads' => $follow_up_date.":".$comment,
			]);
			return response()->json(['status' => 1, 'message' => 'successfully start new lead'], 200);
		} catch(\Exception $e) {
			return response()->json(['status' => 0, 'message' => 'Some Error Occurred'], 200);
		}
	}

	public function getLead(Request $request, $lead_id)
	{
		$user = app('App\Http\Controllers\ApiTokenController')->authenticate($request);
		if($user == null)
			return response()->json(['message' => 'Token Invalid'], 200);

		try {
			$lead = Lead::where('id', $lead_id)->first();
			$lead->leads = str_replace(',', "\n", $lead->leads);
			return response()->json(['status' => 1, 'lead' => $lead], 200);
		} catch(\Exception $e) {
			return response()->json(['status' => 0, 'message' => 'Some Error Occurred'], 200);
		}
	}

	public function deleteLead(Request $request, $lead_id)
	{
		$user = app('App\Http\Controllers\ApiTokenController')->authenticate($request);
		if($user == null)
			return response()->json(['message' => 'Token Invalid'], 200);

		try {
			$lead = Lead::where('id', $lead_id)->delete();
			return response()->json(['status' => 1, 'message' => 'deleted'], 200);
		} catch(\Exception $e) {
			return response()->json(['status' => 0, 'message' => 'Couldn\'t Delete'], 200);
		}
	}

	public function updateAppointmentInLead	(Request $request, $lead_id)
	{
		$user = app('App\Http\Controllers\ApiTokenController')->authenticate($request);
		if($user == null)
			return response()->json(['message' => 'Token Invalid'], 200);

		try {
			$appointment_date = $request->date;
			Lead::where('id', $lead_id)->where('matchmaker_id', $user->id)->update(['appoinment_date' => $appointment_date]);
			return response()->json(['status' => 1, 'message' => "Successfully add Appoinment Date"], 200);
		} catch(\Exception $e) {
			return response()->json(['status' => 0, 'message' => 'Some Error Occurred'], 200);
		}
	}

	public function updateLead(Request $request, $lead_id)
	{
		$user = app('App\Http\Controllers\ApiTokenController')->authenticate($request);
		if($user == null)
			return response()->json(['message' => 'Token Invalid'], 200);

		try {
			$follow_up_date = $request->date;
			$comment = $request->comment;
			$lead = Lead::where('id', $lead_id)->where('matchmaker_id', $user->id)->first();
			$lead->leads .= ','.$follow_up_date.':'.$comment;
			$lead->save();
			return response()->json(['status' => 1, 'message' => 'Successfully add comment', 'lead' => $lead], 200);
		} catch(\Exception $e) {
			return response()->json(['status' => 0, 'message' => 'Some Error Occurred'], 200);
		}
	}

	public function getIncentives(Request $request)
	{
		$user = app('App\Http\Controllers\ApiTokenController')->authenticate($request);
		if($user == null)
			return response()->json(['message' => 'Token Invalid'], 200);

		try {
			$incentives = Incentive::all();
			return response()->json(['status' => 1, 'incentives' => $incentives], 200);
		} catch(\Exception $e) {
			return response()->json(['status' => 0, 'message' => 'Some Error Occurred'], 200);
		}
	}

	public function updateIncentive(Request $request)
	{
		$user = app('App\Http\Controllers\ApiTokenController')->authenticate($request);
		if($user == null)
			return response()->json(['message' => 'Token Invalid'], 200);

		try {
			$sale = $request->sale;
			$marriage_fix = $request->marriage_fix;
			$first_level_down = $request->first_level_down;
			$second_level_down = $request->second_level_down;
			$incentives = Incentive::where('id', 1)->update([
				'Sale' => $sale,
				'Marriage_fix' => $marriage_fix,
				'first_level_down' => $first_level_down,
				'second_level_down' => $second_level_down
			]);
			return response()->json(['status' => 1, 'message' => 'updated successfully'], 200);
		} catch(\Exception $e) {
			return response()->json(['status' => 0, 'message' => 'Some Error Occurred'], 200);
		}
	}

	public function updatePlan()
	{
		$plans_matchmaker = MatchmakerUser::where('id', 82)->first();
		$plans = Plans::where('matchmaker_id', 82)->first();
		$matchmakers_id = Plans::pluck('matchmaker_id')->toArray();
		$matchmakers_id = array_unique($matchmakers_id);
		$matchmakers = MatchmakerUser::where('matchmaker_type', 0)->whereNotIn('id', $matchmakers_id)->get();

		foreach ($matchmakers as $matchmaker) {
			foreach ($plans as $plan) {
				Plans::create([
					'matchmaker_id' => $matchmaker->id,
					'plan_name' => $plan->name,
					'meeting_charge' => $plan->meeting_charge,
					'before_roka_charge' => $plan->before_roka_charge,
					'meetings' => $plan->meetings,
					'validity' => $plan->validity,
					'min_family_income' => $plan->min_family_income,
					'max_family_income' => $plan->max_family_income,
				]);
			}
		}
	}

	public function freshMatchmaker(Request $request)
	{
		$id = $request->id;
		$matchmaker = MatchmakerUser::where('id', $id)->where('matchmaker_type', '0')->first();
		if($matchmaker)
			return response($matchmaker, 200);
		else
			return response()->json(['message' => 'User does not exist'], 200);
	}
	public function offlinePayment(Request $request)
	{
		$user = app('App\Http\Controllers\ApiTokenController')->authenticate($request);
		if($user == null)
			return response()->json(['message' => 'Token Invalid'], 200);

		try {
			$client_id = $request->client_id;
			$matchmaker_id = $request->matchmaker_id;
			$payment_id = $request->payment_id;
			$client = ClientProfile::where('id', $client_id)->first();
			$update_payment = ClientPayment::where('client_id', $client_id)
			->where('matchmaker_id', $matchmaker_id)->where('id', $payment_id)
			->where('payment_status', 0)->update([
				'payment_status' => 1,
				'payment_gateway_status' => 'success',
				'pay_mode' => 'offline',
			]);
			if($update_payment) {
				$payment = ClientPayment::where('id', $payment_id)->first();
				$this->sendPaymentConfirmationMessage($client->phone_number, $payment);
				return response()->json(['status' => 1, 'message' => 'Successfully paid offline'], 200);
			}
			else
				return response()->json(['status' => 0, 'message' => 'Some Error Occurred'], 200);
		} catch(\Exception $e) {
			return response()->json(['status' => 0, 'message' => 'Some Error Occurred'], 200);
		}
	}

	public function sendPaymentConfirmationMessage($receiver, $payment)
	{
		$sms = "Thank you for registering with us.\n You have successfully paid an amount of Rs.". $payment->upfront_charge;
		$mobiles = '+91'.substr($receiver, -10);
		$username=env('USER_NAME');
		$password=env('PASSWORD');
		$smsurl='https://dqell.api.infobip.com/sms/1/text/query?username=MY_USERNAME&password=MY_PASSWORD&to=PHONE&text=MESSAGE';
		$smsurl=str_replace('MY_USERNAME',$username,$smsurl);
		$smsurl=str_replace('MY_PASSWORD',$password,$smsurl);
		$smsurl=str_replace('PHONE',$mobiles,$smsurl);
		$smsurl=str_replace('MESSAGE',$sms,$smsurl);
		try{
			$result=file_get_contents($smsurl);
			return response()->json(['status' => 1, 'message' => $smsurl], 200);
		}
		catch(\Exception $e){
			return response()->json(['status' => 0, 'message' => 'Some Error Occurred'], 400);
		}
	}

	public function chain(Request $request)
	{
		$user = app('App\Http\Controllers\ApiTokenController')->authenticate($request);
		if($user == null)
			return response()->json(['message' => 'Token Invalid'], 200);

		try {
			if($user->child) {
				//first level child
				$childs_id = explode(',', $user->child);
				$childs = MatchmakerUser::whereIn('id', $childs_id)
				->select('id', 'first_name', 'last_name', 'phone_number', 'unique_referral_code', 'profile_pic')
				->get()->toArray();

				//second level child
				$childs_of_child = MatchmakerUser::whereIn('parent_id', $childs_id)
				->select('id', 'first_name', 'last_name', 'phone_number', 'unique_referral_code', 'profile_pic')
				->get()->toArray();
			}
			else {
				$childs = [];
				$childs_of_child = [];
			}
			$parent = MatchmakerUser::where('id', $user->parent_id)
			->select('id', 'first_name', 'last_name', 'phone_number', 'unique_referral_code', 'profile_pic')
			->first();
			try {
				$parent_of_parent = MatchmakerUser::where('id', $parent->parent_id)
				->select('id', 'first_name', 'last_name', 'phone_number', 'unique_referral_code', 'profile_pic')
				->first();
				return response([$parent_of_parent, $parent, $childs, $childs_of_child], 200);
			} catch(\Exception $e) {
				return response([[], $parent, $childs, $childs_of_child], 200);
			}
		} catch(\Exception $e) {
			return response()->json(['status' => 0, 'message' => 'Some Error Occurred'], 200);
		}
	}

	public function paymentConfirmation(Request $request)
	{
		$user = app('App\Http\Controllers\ApiTokenController')->authenticate($request);
		if($user == null)
			return response()->json(['message' => 'Token Invalid'], 200);

		$order_id = $request->order_id;
		$gateway = $request->gateway;

		if($gateway == 'paytm')
			return response()->json(['status' => 0, 'message' => 'Yet to be developed'], 200);
		elseif($gateway == 'razorpay') {
			$api = new Api(env('RAZORPAY_TEST_KEY'), env('RAZORPAY_SECRET_KEY'));
			$order = $api->payment->fetch($order_id);
			$payment_amount = ClientPayment::where('matchmaker_id', $user->id)->where('payment_status', 0)
			->sum('payment_amount');
			if($order['status'] == 'paid' && $payment_amount == $order['amount_paid']) {
				$update_payment = ClientPayment::where('matchmaker_id', $user->id)
				->where('payment_status', 0)
				->update([
					'payment_gateway' => 'razorpay',
					'payment_status' => 1,
					'payment_gateway_status' => 'success',
					'payment_id' => $order_id,
				]);
				if($update_payment) {
					# on first successful payment of referree, add credit/money to the account of referror
					if(ClientPayment::where('matchmaker_id', $user->id)->where('payment_status', 1)->count() == 1) {
						try {
							$referred_by = MatchmakerUser::where('id', $user->referred_by)->first();
							if($referred_by->matchmaker_type == 0) {
								$payment_for = 'referral to '. $user->id;
								$create_payment = ClientPayment::create([
									'matchmaker_id' => $referred_by->id,
									'payment_for' => $payment_for,
									'payment_amount' => 100,
									'payment_status' => 1,
									'pay_mode' => 'virtual',
								]);
								$referred_by->credits += 100;
							}

							else {
								$payment_for = 'referral to '. $user->id;
								$create_payment = ClientPayment::create([
									'matchmaker_id' => $referred_by->id,
									'payment_for' => $payment_for,
									'payment_amount' => 200,
									'payment_status' => 1,
									'pay_mode' => 'virtual',
								]);
								$referred_by->credits += 200;
							}
							$referred_by->save();
						} catch(\Exception $e) {
						}
					}

					if($user->matchmaker_type == 1) {
						try {
							$plan = PlansForExisting::where('amount', $order['amount_paid'])->first();
							$user->plan_id = $plan->id;
							$user->credits += $plan->credits;
							$user->validity = max($user->validity, $plan->validity);
							$user->save();
						} catch(\Exception $e) {
							return response()->json(['status' => 0, 'message' => 'Payment Successful, but credits aren\'t added. Contact customer support.'], 200);
						}
					}

					return response()->json(['status' => 1, 'message' => 'Successfully Paid'], 200);
				}

				else
					return response()->json(['status' => 0, 'message' => 'Some Error Occurred'], 200);
			}
			else
				return response()->json(['status' => 0, 'message' => 'Details Mismatched'], 200);
		}
		else
			return response()->json(['status' => 0, 'message' => 'Payment Gateway not Found'], 200);
	}

	public function buyPlanConfirmation(Request $request)
	{
		$user = app('App\Http\Controllers\ApiTokenController')->authenticate($request);
		if($user == null)
			return response()->json(['message' => 'Token Invalid'], 200);

		$order_id = $request->order_id;
		$gateway = $request->gateway;
		$matchmaker_id = $request->matchmaker_id;
		$payment_id = $request->payment_id;
		$pay_mode = $request->pay_mode;
		$plan_id = $request->plan_id;

		if($gateway == 'razorpay') {
			$plan = PlansForExisting::where('id', $plan_id)->first();
			$api = new Api(env('RAZORPAY_TEST_KEY'), env('RAZORPAY_SECRET_KEY'));
			$order = $api->order->fetch($order_id);
			if($order['status'] == 'paid' && $plan->amount == $order['amount_paid']) {
				$user->credits += $plan->credits;
				$user->save();
				$payment = BuyCreditPayment::create([
					'payment_id' => $order_id,
					'payment_status' => 1,
					'pay_mode' => 'online',
					'matchmaker_id' => $user->id,
					'credits' => $user->credits,
					'plan_id' => $plan->id,
					'gateway' => $gateway,
					'payment_amount' => $plan->amount,
				]);
				if($payment) {
					$transaction = Transaction::create([
						'matchmakers_id' => $user->id,
						'credits' => $user->credits,
						'buy_credit_id' => $plan->id,
					]);
					$this->pushNotification("Payment","Payment Successfully paid by you and you have subscibre ".$plan->plan_name." plan and your total credits"+$user->credits, $user->device_id);
					return response()->json(['status' => 1, 'message' => 'Successfully Paid'], 200);
				}
				else
					return response()->json(['status' => 0, 'message' => 'Some Error Occurred'], 200);
			}
			else
				return response()->json(['status' => 0, 'message' => 'Details Mismatched'], 200);
		}
		else
			return response()->json(['status' => 0, 'message' => 'Payment Gateway not Found'], 200);
	}

	public function clientPaymentConfirmation(Request $request)
	{
		$payment_id = $request->payment_id;
		$gateway = $request->gateway;
		$matchmaker_id = $request->matchmaker_id;
		$id = $request->id;
		$phone_number = $request->phone_number;
		$matchmaker = MatchmakerUser::where('id', $matchmaker_id)->first();
		if(!$matchmaker)
			return response()->json(['status' => 0, 'message' => 'Matchmaker Not Found'], 200);

		$client = ClientProfile::where('phone_number', $phone_number)->where('matchmaker_id', $matchmaker->id)->first();
		if(!$client)
			return response()->json(['status' => 0, 'message' => 'Client Not Found'], 200);
		$client_id = $client->id;
		if($gateway == 'paytm')
			return response()->json(['status' => 0, 'message' => 'Yet to be developed'], 200);
		elseif($gateway == 'razorpay') {
			$api = new Api(env('RAZORPAY_LIVE_KEY'), env('RAZORPAY_LIVE_SECRET_KEY'));
			$payment = $api->payment->fetch($payment_id);

			$client_payment = ClientPayment::where('matchmaker_id', $matchmaker->id)
			->where('id', $id)
			->where('client_id', $client_id)
			->where('payment_status', 0)->first();
			if(!$client_payment) {
				$plan = Plans::where('id', $id)->first();
				try {
					$client_payment = ClientPayment::create([
						'client_id' => $client->id,
						'plan_id' => $plan->id,
						'matchmaker_id' => $matchmaker->id,
						'payment_for' => 'self',
						'payment_amount' => $plan->meeting_charge,
						'payment_gateway' => 'razorpay',
						'payment_status' => 0,
						'payment_gateway_status' => 'failure',
						'pay_mode' => 'online',
						'upfront_charge' => $plan->meeting_charge,
						'meeting_charge' => $plan->meeting_charge,
						'before_roka_charge' => $plan->before_roka_charge,
						'paid_on' => Carbon::now()->addSeconds(19800),
						'expire_on' => Carbon::now()->addSeconds(19800)->addYear(),
					]);
				} catch(\Exception $e) {
					return response()->json(['status' => 0, 'message' => 'Some error Occurred'], 200);
				}
			}

			if($payment['status'] == 'captured' && $client_payment->upfront_charge == $payment['amount']/100) {
				$update_payment = ClientPayment::where('matchmaker_id', $matchmaker->id)
				->where('id', $client_payment->id)
				->where('client_id', $client_id)
				->where('payment_status', 0)
				->update([
					'payment_gateway' => 'razorpay',
					'payment_status' => 1,
					'payment_gateway_status' => 'success',
					'payment_id' => $payment_id,
					'pay_mode' => 'online',
				]);
				$client = ClientProfile::where('id', $client_id)->first();
				if($update_payment) {
					$client->payment_status = 1;
					$client->save();
					$this->pushNotification("Payment","Payment Successfully done by ".$client->name." mobile ".$client->phone_number, $matchmaker->device_id);
					return response()->json(['status' => 1, 'message' => 'Successfully Paid'], 200);
				}
				else
					return response()->json(['status' => 0, 'message' => 'Some Error Occurred'], 200);
			}
			else
				return response()->json(['status' => 0, 'message' => 'Details Mismatched'], 200);
		}
		else
			return response()->json(['status' => 0, 'message' => 'Payment Gateway not Found'], 200);
	}

	public function pushNotification($message_title, $message, $reg_id)
	{
		//API URL of FCM
		$url = 'https://fcm.googleapis.com/fcm/send';
		$api_key = env('FIREBASE_APIKEY');

		$fields = array (
			'to' => $reg_id,
			'notification' => array (
				"title" => $message_title,
				"body" => $message
			)
		);

    //header includes Content type and api key
		$headers = array(
			'Content-Type:application/json',
			'Authorization:key='.$api_key
		);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
		$result = curl_exec($ch);
		if ($result === FALSE) {
			// die('FCM Send Error: ' . curl_error($ch));
		}
		curl_close($ch);
		return $result;
	}

	public function appUpdate(Request $request)
	{
		$version = $request->version;
		$content = file(storage_path('version.txt'));

		$app_version = explode("\n", $content[0])[0];
		$force_update = $content[1];
		if($force_update == 1)
			$force_update = true;
		else
			$force_update = false;

		if($version == $app_version)
			return response()->json(['status' => 1, 'version' => $app_version, 'message' => 'App Updated'], 200);
		else
			return response()->json(['status' => 0, 'force_update' => $force_update, 'version' => $app_version, 'message' => 'Force Update'], 200);

	}

	public function newApp(Request $request)
	{
		$user = app('App\Http\Controllers\ApiTokenController')->authenticate($request);
		if($user == null)
			return response()->json(['message' => 'Token Invalid'], 200);

		$version = $request->version;
		$force_update = $request->force_update;
		try {
			if($user->is_superuser) {
				$content = $version."\n".$force_update;
				$file = file_put_contents(storage_path('version.txt'), $content);
				return response()->json(['status' => 1, 'message' => 'Version Updated', 'version' => $version], 200);
			}
			else
				return response()->json(['status' => 0, 'message' => 'Not Authorized'], 200);
		} catch(\Exception $e) {
			return response()->json(['status' => 0, 'message' => 'Try Again!'], 200);
		}
	}

	public function searchClient(Request $request)
	{
		$user = app('App\Http\Controllers\ApiTokenController')->authenticate($request);
		if($user == null)
			return response()->json(['message' => 'Token Invalid'], 200);

		$search = $request->search;
		try {
			if($search) {
				$clients = ClientProfile::where('matchmaker_id', $user->id)->where('name', 'LIKE', $search)
				->orWhere('phone_number', 'LIKE', $search)
				->get();
			}
			else
				$clients = ClientProfile::where('matchmaker_id', $user->id)->get();
			if($clients->count() > 0)
				return response($clients, 200);
			else
				return response()->json(['status' => 0, 'message' => 'No clients Found']);
		} catch (\Exception $e) {
			return response()->json(['status' => 0, 'message' => 'Some Error Occurred'], 200);
		}
	}

	public function createPlanSubscription($plan, $upfront_charge, $meeting_charge, $payment_id)
	{
		$api = new Api(env('RAZORPAY_LIVE_KEY'), env('RAZORPAY_LIVE_SECRET_KEY'));
		$description = "Services: \nShare 2-3 profiles per week.\nArrange Meetings.\nMeeting Coordination.\nMeeting Feedback.\nProfile Counselling and Feedback.";
		$razorpay_plan = $api->plan->create(
			array(
				'period' => 'monthly',
				'interval' => 1,
				'item' => array(
					'name' => $plan->plan_name,
					'description' => $description,
					'amount' => $meeting_charge*100,
					'currency' => "INR"
				)
			)
		);
		if($upfront_charge > $meeting_charge) {
			$subscription = $api->subscription->create(
				array(
					'plan_id' => $razorpay_plan->id,
					'total_count' => 12,
					'addons' => array(
						array(
							'item' =>  array(
								'name' => 'Upfront Charge',
								'amount' => ($upfront_charge - $meeting_charge)*100,
								'currency' => 'INR'
							)
						)
					),
					'customer_notify' => 0,
					'expire_by' => strtotime(Carbon::now()->addSeconds(19800)->addYear())
				)
			);
		}
		else {
			$subscription = $api->subscription->create(
				array(
					'plan_id' => $razorpay_plan->id,
					'total_count' => 12,
					'customer_notify' => 0,
					'expire_by' => strtotime(Carbon::now()->addSeconds(19800)->addYear())
				)
			);
		}

		ClientPayment::where('id', $payment_id)->update([
			'razorpay_plan_id' => $razorpay_plan->id,
			'subscription_id' => $subscription->id,
			'subscription_link' => $subscription->short_url,
		]);
		return $subscription->short_url;
	}
}
