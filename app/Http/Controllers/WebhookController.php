<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\ClientProfile;
use App\Compatibility;
use App\Caste;
use App\Preference;
use App\PreferenceCaste;
use App\MatchmakerUser;
use App\Degree;

use Illuminate\Support\Facades\DB;

class WebhookController extends Controller
{
	protected $max_profile = 6;
	public $type = 0;
	public $buttons = '';

	public function sendMessages(Request $request) {

		$data = json_decode($request->data);
		if ($data->event == "INBOX") {
			$recievedMessage = $data->text;
			$profile = ClientProfile::where('whatsapp_number', 'LIKE', '%'.substr($data->from, -10))
			->orWhere('phone_number', substr($data->from, -10))
			->orderby('id', 'desc')->first();
			// If user is not in our database
			if($profile == null) {
				$message = " *माफ़ कीजिये।*, आप हमारे साथ रजिस्टर्ड नहीं है।\n\n कृपया अपना रजिस्टर्ड नंबर इस्तेमाल करे या यहाँ https://hansmatrimony.com/ रजिस्टर करे।\n\nया मदद के लिए हमारे ग्राहक सेवा नंबर 📞9697989697 पे संपर्क करे।";
				$this->buttons = 'Register';
			}

			else {
				if(isset($data->channel_name)) {
					if ($data->channel_name != 'facebook') {
						$profile->bot_language = 'Hindi';
						$profile->save();
					}
				}

				//choose language for the first time
				if ($profile->bot_language == null && $profile->wantBot == 0) {
					if (strpos(strtoupper($recievedMessage), 'SHOW') !== False) {
						$message = "👉 अपने सुविधा अनुसार भाषा चुने : \n\n 1⃣ *English* के लिए 1 लिखे। \n\n 2⃣ *Hindi* के लिए2 लिखे। \n\n 👉 कृपया *कहे अनुसार* जवाब दे।";
						if(isset($data->channel_name)) {
							$message = "👉 कृपया बेहतर बातचीत के लिए भाषा चुने।";
							$this->buttons = 'English,Hindi';
						}
						$compatibility = Compatibility::where('client_id', $profile->id)->first();
						if($compatibility != null) {
							$compatibility->current = -1; 
						//waiting state corresponding to choose language message
							$compatibility->save();
						}
					}
					elseif (strpos(strtoupper($recievedMessage), '1') !== False) {
						$profile->bot_language = 'English';
						$profile->wantBot = 1;
						$profile->save();
						$compatibility = Compatibility::where('client_id', $profile->id)->first();
						if($compatibility != null) {
							//changing state to send profiles
							$compatibility->current = 0;
							$compatibility->save();
						}
					}
					elseif (strpos(strtoupper($recievedMessage), '2') !== False) {
						$profile->bot_language = 'Hindi';
						$profile->wantBot = 1;
						$profile->save();
						$compatibility = Compatibility::where('client_id', $profile->id)->first();
						if($compatibility != null) {
							// changing state to send profiles
							$compatibility->current = 0;
							$compatibility->save();
						}
					}
					else {
						$message = "👉 रिश्ते देखने के लिए *SHOW* लिखे।";
						$this->buttons = 'Show';
					}
				}
				else {
					//use SHOW to make wantBot=1
					if (strpos(strtoupper($recievedMessage), 'SHOW') !== False && $profile->wantBot == 0) {
						$profile->wantBot = 1;
						$profile->save();
					}
					elseif (strpos(strtoupper($recievedMessage), 'SHOW') === False && $profile->wantBot == 0) {
						$message = "👉 *Please* message *SHOW* to us to see profiles. \n \n *Please* ignore this message if you have already seen ".$this->max_profile." profiles today. ";
						if($profile->bot_language == 'Hindi')
							$message = "👉 *कृपया* रिश्ते देखने के लिए SHOW* लिख कर भेजे। \n\n *कृपया* इस मैसेज को नज़रंदाज़ करे अगर आपने 6 रिश्ते देख लिए हो।";
						$this->buttons = 'Show';
					}
				}
				// for users who opted for Chatbot
				if($profile->wantBot == 1) {
					$compatibility = Compatibility::where('client_id', $profile->id)->first();
					if (isset($data->channel_name)) {
						if ($data->channel_name != 'facebook')
							$compatibility->read_message = 1;
					}
					if($compatibility != null && $compatibility->read_message == 1) {
						$user = DB::table('compatibilities')
						->where('client_id', $compatibility->client_id)
						->join('client_clientprofile', 'compatibilities.client_id', '=', 'client_clientprofile.id')
						->first();
						$current = $user->current;
						$profile_status = json_decode($compatibility->profile_status);
						$client_id = $compatibility->client_id;
						$daily_quota = $compatibility->daily_quota;

						//if daily quota is filled, current will remain '6' for the day
						if($current == 6 && $daily_quota > $this->max_profile) {
							$message = "We have already shared *".$this->max_profile." profiles* with you.\n \n Come back tommorrow and type *SHOW* to see more profiles";
							if($profile->bot_language == 'Hindi')
								$message = "👉 हम आपको ".$this->max_profile." रिश्ते दिखा चुके हैं। \n\n👉कृपया कल पुनः *SHOW* लिख कर भेजे। ";
						}
						else {
							//send a new profile
							if( $current == null || $current == 0) {
								$message = $this->sendFirstProfiles($user, $data);
								$user->current = 1;
							}
							// take action on different waiting states
							elseif ($current == 1 || $current == 4 || $current == 5 || $current == 6) {
								$message = $this->processState($recievedMessage, $user, $data);
							}
						}
					}
					elseif ($compatibility == null) {
						$message = "👉 We are looking for compatible profiles for you.\n\n *Please* come back after a day and write *SHOW* to us. \n\n *Thank You!* \n *Hans Matrimony!*";
						if($profile->bot_language == 'Hindi')
							$message = "👉 हम आपके लिए योग्य रिश्ते खोज रहे है। \n\n 👉 कृपया एक दिन बाद पुनः *SHOW* लिख कर भेजे। \n\n *धन्यवाद् !* \n *हंस मॅट्रिमोनी !*";
						$this->calculate($profile->id);

					}
				}
			}
			$response = new \StdClass();
			if(isset($data->channel_name)) {
				if($this->type == 1)
					$response->type = 'profile';
				else
					$response->type = 'message';
				$response->buttons = $this->buttons;

				if($data->channel_name == 'facebook')
					$response->message = $message;
				else
					$response->apiwha_autoreply = $message;
			}
			else
				$response->apiwha_autoreply = $message;
			echo json_encode($response);
		}
		elseif ($data->event=="MESSAGEPROCESSED") {
			$profile = ClientProfile::where('whatsapp_number', 'LIKE', '%'.substr($data->to, -10))
			->orWhere('phone_number', substr($data->to, -10))
			->orderby('id', 'desc')->first();
			if ($profile) {
				$compatibility = Compatibility::where('client_id', $profile->id)->first();
				if($compatibility) {
					$compatibility->read_message = 1;
					$compatibility->save();
				}
			}
		}
	}

	public function sendFirstProfiles($user, $data) {
		$arr = json_decode($user->compatibility);
		$sent_profiles = json_decode($user->profile_status);

		//if the last profile in the compatibility has been sent
		if(sizeof($arr) < 1) {
			$this->calculate($user->client_id);
			$message = "We are looking for compatible profiles for you.\n\n Come back after 2 days and type *SHOW* to see more profiles";
			if($user->bot_language == 'Hindi')
				$message = "हम आपके लिए योग्य रिश्ते खोज रहे है। \n\n 👉 कृपया दो दिन बाद पुनः *SHOW* लिख कर भेजे। ";
			$this->buttons = 'Show';

			ClientProfile::where('id', $user->client_id)->update(['wantBot' => 0]);
		}

		else {
//		echo "Running ....";
			$compatibleId = $arr[0]->client_id;
			$profile = ClientProfile::whereRaw("(id ='". $compatibleId."') AND (gender != '".$user->gender."')")
			->first();

			//if profile is available corresponding to the compatible Id
			if($profile == null ) {
				array_splice($arr, 0, 1);

				Compatibility::where('client_id', $user->client_id)
				->update(['current' => 0, 'compatibility' => json_encode($arr)]);
					// again call the function to send the profile of available user
				return $this->sendFirstProfiles($user, $data);
			}
			else {
				//generating compatible profile for the user
				if(isset($data->channel_name)) {
					$profile->profiles_left = 5-$user->daily_quota;
					$profile->credits = $user->credits;
					$profile->language = $user->bot_language;
					$this->buttons = 'Yes,No';
					$matchmaker = MatchmakerUser::where('id', $user->matchmaker_id)->first();
					$profile->your_matchmaker = $matchmaker ? $matchmaker->first_name.' '. $matchmaker->last_name : null;
					$message = $profile;
				}
				else
					$message = $this->generateProfile($profile, $user->daily_quota, $user->bot_language, $user->matchmaker_id);
				$this->type = 1;
				$user->current = 1;
				array_splice($arr, 0, 1);

				Compatibility::where('client_id', $user->client_id)->update(['read_message' => 0, 'compatibility' => json_encode($arr)]);
					//updating table for the profile sent
				$this->updateCompatibleTable($user->client_id, $user->current, $user->profile_status, $compatibleId, $user->daily_quota + 1);

			}
		}
		return $message;
	}

	public function processState($recievedMessage, $user, $data) {
		//processing states at waiting states for the user depending upon the message recieved
		$flag = 0;
		if(strpos(strtoupper($recievedMessage), 'STOP') !== False) {
			$flag = 2;
			$profile_status = json_decode($user->profile_status);
			foreach ($profile_status as $key) {
				if($key->status == 'P') {
					$key->status = 'R';
				}
			}
			Compatibility::where('client_id', $user->client_id)->update(['current' => 0, 'profile_status' => json_encode($profile_status)]);
		}
		elseif ($user->current == 1 && strpos(strtoupper($recievedMessage), 'NO') !== False) {
			$user->current = 3;
		}
		elseif ($user->current == 1 && strpos(strtoupper($recievedMessage), 'YES') !== False)
			$user->current = 2;

		elseif ($user->current == 1 && strpos(strtoupper($recievedMessage), 'YES') === False && strpos(strtoupper($recievedMessage), 'NO') === False)
			$user->current = 11;

		if($flag != 2)
			$message = $this->sendResponse($user->current, $user, $user->active, $data);
		return $message;
	}

	public function sendResponse($state, $user, $compatibleId, $data) {
		$message = $this->createResponse($state, $user->client_id, $compatibleId, $user->daily_quota, $user->bot_language, $user->matchmaker_id, $data);
//dd($status);
		
		return $message;
	}

	public function createResponse($state, $client_id, $compatibleId, $daily_quota, $bot_language, $matchmaker_id, $data) {
		switch ($state) {
			case 101 :
			$response = "🙋‍♂ *Waiting* for your response.\n \n👉Please reply *YES* if interested or *NO* if not interested.\n \n And *STOP* if don't want to see profiles.";
			if($bot_language == 'Hindi')
				$response = "🙋‍♂ आपके जवाब का *इंतज़ार* कर रहे। \n\n कृपया *YES* लिखे अगर रूचि हो अथवा *NO* \n\n और *STOP* लिखे अगर रिश्ते न देखना चाहते हो। ";
			break;

			case 2 :
			$compatibility = Compatibility::where('client_id', $client_id)->first();
			$profile_status = json_decode($compatibility->profile_status);
			foreach ($profile_status as $key) {
				if($key->client_id == $compatibility->active)
					$key->status = 'C';
			}
			$client = ClientProfile::where('id', $client_id)->first();
			$shared_profile = ClientProfile::where('id', $compatibility->active)->first();
			$matchmaker = MatchmakerUser::where('id', $matchmaker_id)->first();

			if ($daily_quota < $this->max_profile) {
				if ($client->payment_status) {
					if ($bot_language == 'Hindi')
						$response = " *संपर्क करे* 📞".$shared_profile->phone_number."\n\n👉कृपया पुनः *SHOW* लिख कर भेजे अगर रूचि हो । \n🔎आज के लिए *". (6-$daily_quota) ." रिश्ते* और हैं।\n";
					else
						$response = " *Contact at* 📞".$shared_profile->phone_number."\n\n👉 Please reply *SHOW* if interested to see more profiles.\n\n🔎 *". (6-$daily_quota) ." profiles* left for today.";

					Compatibility::where('client_id', $client_id)->update(['current' => 0, 'profile_status' => json_encode($profile_status), 'read_message' => 0, 'reminder' => 0, 'credits' => $compatibility->credits-1]);
				}
				else {
					if($bot_language == 'Hindi')
						$response = "👉 कृपया मोबाइल नंबर के लिए अपने मैचमैकेर *".$matchmaker->first_name." ".$matchmaker->last_name."* से संपर्क करे। \n\n 👉कृपया पुनः *SHOW* लिख कर भेजे अगर रूचि हो ।  \n\n 🔎आज के लिए *". ($this->max_profile-$daily_quota) ." रिश्ते* और हैं।";
					else
						$response = "👉 Please contact your Matchmaker *".$matchmaker->first_name." ".$matchmaker->last_name."* for contact no.\n\n 👉 Please reply *SHOW* if interested to see more profiles.\n\n🔎 *". ($this->max_profile-$daily_quota) ." profiles* left for today. ";

					Compatibility::where('client_id', $client_id)->update(['current' => 0, 'profile_status' => json_encode($profile_status), 'read_message' => 0, 'reminder' => 0]);
				}

				ClientProfile::where('id', $client_id)->update(['wantBot' => 0]);
			}
			elseif ($daily_quota == $this->max_profile) {
				if ($client->payment_status) {
					if ($bot_language == 'Hindi')
						$response = " *संपर्क करे* 📞".$shared_profile->phone_number."\n\n🕓 हम आपको ६ रिश्ते दिखा चुके हैं। \n\n👉कृपया कल पुनः *SHOW* लिख कर भेजे। ";
					else
						$response = "*Contact at* 📞".$shared_profile->phone_number."\n\n🕓 You have seen 6 profiles. Please come back tommorrow and message *SHOW* to continue seeing more profiles.";

					Compatibility::where('client_id', $client_id)->update(['current' => 6, 'profile_status' => json_encode($profile_status), 'read_message' => 0, 'reminder' => 0, 'credits' => $compatibility->credits-1]);
				}
				else {
					if($bot_language == 'Hindi')
						$response = "👉 कृपया मोबाइल नंबर के लिए अपने मैचमैकेर *".$matchmaker->first_name." ".$matchmaker->last_name."* से संपर्क करे। \n\n 👉कृपया पुनः *SHOW* लिख कर भेजे अगर रूचि हो । \n\n 🕓 हम आपको ".$this->max_profile." रिश्ते दिखा चुके हैं। \n\n👉कृपया कल पुनः *SHOW* लिख कर भेजे। ";
					else
						$response = "👉 Please contact your Matchmaker *".$matchmaker->first_name." ".$matchmaker->last_name."* for contact no.\n\n 🕓 We have shared *". ($this->max_profile-$daily_quota) ." profiles* today.  \n\n 👉 Come back tommorrow and message *SHOW* to see more profiles.";

					Compatibility::where('client_id', $client_id)->update(['current' => 6, 'profile_status' => json_encode($profile_status), 'read_message' => 0, 'reminder' => 0]);
				}

				ClientProfile::where('id', $client_id)->update(['wantBot' => 0]);
			}

			//message to matchmaker for client response
			if (!$client->payment_status) {
				$message_to_matchmaker = "आपके क्लाइंट ".$client->name." (".$client->phone_number.") ने इस प्रोफाइल को पसंद किया है:\nName: ".$shared_profile->name."\nAge: ".$shared_profile->age."\n\n अपने क्लाइंट का रिस्पांस देखें  \nhttp://www.hansmatrimony.com/assisted?clientId=".$client->id."\nकृपया बात करके हमारे प्लांस बताए ।";
				$message_to_matchmaker = urlencode($message_to_matchmaker);
				app('App\Http\Controllers\WhatsAppController')->sendTextMessage($message_to_matchmaker, $matchmaker->phone_number);
				//message via sms
				$username = env('USER_NAME');
				$password = env('PASSWORD');
				$mobile = substr($matchmaker->phone_number, -10);
				$url = "https://dqell.api.infobip.com/sms/1/text/query?username=".$username."&password=".$password."&to=91".$mobile."&text=".$message_to_matchmaker;
				file_get_contents($url);
			}

			$this->buttons = 'Show';

			break;

			case 3 :
			$compatibility = Compatibility::where('client_id', $client_id)->first();
			$profile_status = json_decode($compatibility->profile_status);
			foreach ($profile_status as $key) {
				if($key->client_id == $compatibility->active)
					$key->status = 'R';
			}

			if ($daily_quota < $this->max_profile) {
				// $response = "Ok thank you! We will get back to you with more profiles. \n🕓 You have ". (5-$daily_quota) ." more profiles to see for today.";
				$this->type = 1;
				Compatibility::where('client_id', $client_id)->update(['current' => 0, 'read_message' => 0, 'reminder' => 0, 'profile_status' => json_encode($profile_status)]);
				$user = DB::table('compatibilities')
				->where('client_id', $client_id)
				->join('client_clientprofile', 'compatibilities.client_id', '=', 'client_clientprofile.id')
				->first();
				$response = $this->sendFirstProfiles($user, $data);
			}
			else {
				$response = "\nOk thank you. We share ".$this->max_profile." profiles a day. \n \n👉 Come back tommorrow and message *SHOW* to see more profiles. ";
				if($bot_language == 'Hindi')
					$response = "*धन्यवाद*। हम रोज़ाना ".$this->max_profile." रिश्ते भेजते हैं ।\n\n👉कृपया कल पुनः *SHOW* लिख कर भेजे। ";

				Compatibility::where('client_id', $client_id)->update(['current' => 6, 'read_message' => 0, 'reminder' => 0, 'profile_status' => json_encode($profile_status)]);
				ClientProfile::where('id', $client_id)->update(['wantBot' => 0]);
				$this->buttons = 'Show';
			}

			break;

			case 11 :
			$response = "👉 I am sorry, I dont understand.\n \n👉 Please reply *YES* if interested or *NO* if not interested.";
			if($bot_language == 'Hindi')
				$response = "माफ़ कीजिये। मुझे समझ नहीं आया। \n\n👉कृपया *YES* लिखे अगर रूचि हो अथवा *NO*";
			Compatibility::where('client_id', $client_id)->update(['read_message' => 0]);
			$this->buttons = 'Yes,No';

			break;

			case 6 :
			$response = "Thank you for responding. But we share ".$this->max_profile." profiles a day. \n \n👉 Come back tommorrow and message *SHOW* to see more profiles. ";
			if($bot_language == 'Hindi')
				$response = "*धन्यवाद*। लेकिन हम रोज़ाना ६ रिश्ते भेजते हैं ।\n\n👉कृपया कल पुनः *SHOW* लिख कर भेजे। ";

			Compatibility::where('client_id', $client_id)->update(['current' => 6, 'read_message' => 0]);
			ClientProfile::where('id', $client_id)->update(['wantBot' => 0]);
			$this->buttons = 'Show';

			break;
		}
		return $response;
	}

	public function generateProfile($profile, $daily_quota, $bot_language, $matchmaker_id) {
		$gender = ($profile->gender == 1)? "GIRL" : "BOY";
		$emoji = ($profile->gender == 1)? '👩' : '👦';
		$msg = "";
		$url = '';
		$matchmaker = MatchmakerUser::where('id', $matchmaker_id)->first();
		$url = $profile->profile_photo;

		if($bot_language == 'Hindi') {

			$msg = $msg .$emoji." *हंस मॅट्रिमोनी द्वारा आपके लिए अनुशंषित रिश्ता* \n\n *".$matchmaker->first_name ." ". $matchmaker->last_name." द्वारा* \n";
			if($profile->profile_photo != null && $profile->profile_photo != '')
				$msg = $msg ."\n *फोटो* : ".$url;
			$msg = $msg ."\n ____________________________ \n";
			$msg = $msg. "\n" . $emoji ."‍ *व्यक्तिगत विवरण* \n";
			if($profile->name != null && $profile->name !='')
				$msg = $msg. "नाम : ".$profile->name."\n";
			$today = date('Y-m-d', time());
			$birthDate = $profile->birth_date;
			$age = date('Y', strtotime($today)) - date('Y', strtotime($birthDate));
			if($age != null && $age != '')
				$msg = $msg ."उम्र : ".$age."\n";
			$height = floor($profile->height/12)."ft ".($profile->height%12)."in.";
			if($profile->height != null && $profile->height != '')
				$msg = $msg ."कद : ".$height."\n";
			if($profile->weight != null && $profile->weight != '')
				$msg = $msg. "वजन : ".$profile->weight."\n";
			if($profile->caste_id != null && $profile->caste_id != '') {
				$caste = Caste::where('id', $profile->caste_id)->first();
				$msg = $msg. "जाति : ".$caste->caste."\n";
			}
			if($profile->gotra != null && $profile->gotra != '')
				$msg = $msg. "गोत्र : ".$profile->gotra."\n";
			if($profile->food_choice != null){
				if($profile->food_choice == 0)
					$msg = $msg ."खान-पान : Vegeterain \n";
				elseif($profile->food_choice == 1)
					$msg = $msg ."खान-पान : Non-Vegeterain \n";
			}
			
			if($profile->marital_status != null) {
				if($profile->marital_status == 0)
					$msg = $msg . "वैवाहिक स्तिथि : Never Married\n";
				elseif($profile->marital_status == 1)
					$msg = $msg . "वैवाहिक स्तिथि : Divorcee\n";
				elseif($profile->marital_status == 2)
					$msg = $msg . "वैवाहिक स्तिथि : Widowed\n";
			}

			$msg = $msg ."\n ___________________________ \n";
			$msg = $msg ."\n ♋ *जन्मपत्रिका* \n";
			if($profile->birth_date != null && $profile->birth_date != '')
				$msg = $msg . "जन्म दिवस : ".date("Y-m-d", strtotime($profile->birth_date))."\n";
			if($profile->birth_time != null && $profile->birth_time != '')
				$msg = $msg . "जन्म का समय : ".$profile->birth_time."\n";
			if($profile->birth_place != null && $profile->birth_place != '')
				$msg = $msg . "जन्म स्थान : ".$profile->birth_place."\n";
			if($profile->manglik != null) {
				if($profile->manglik == 0)
					$msg = $msg . "मांगलिक : Non-Manglik \n";
				elseif($profile->manglik == 1)
					$msg = $msg . "मांगलिक : Manglik \n";
				elseif($profile->manglik == 2)
					$msg = $msg . "मांगलिक : Anshik Manglik \n";
			}

			$msg = $msg ."\n ____________________________ \n";
			$msg = $msg ."\n 📚 *शैक्षिक विवरण* \n";
			if($profile->education != null && $profile->education != '')
				$msg = $msg . "शिक्षा : ".$profile->education."\n";
			if($profile->degree_id != null && $profile->degree_id != '') {
				$degree = Degree::where('id', $profile->degree_id)->first();
				$msg = $msg . "डिग्री : ".$degree->name." \n";
			}
			if($profile->college != null && $profile->college != '')
				$msg = $msg . "कॉलेज : ".$profile->college."\n";

			$msg = $msg. "\n ____________________________ \n";
			$msg = $msg. "\n 💼 *नौकरी का विवरण* \n";
			if($profile->occupation != null) {
				if($profile->occupation == 0)
					$msg = $msg . "व्यसाय : Not Working \n";
				elseif($profile->occupation == 1)
					$msg = $msg . "व्यसाय : Private Company \n";
				elseif($profile->occupation == 2)
					$msg = $msg . "व्यसाय : Business/Self Employed \n";
				elseif($profile->occupation == 3)
					$msg = $msg . "व्यसाय : Government Job \n";
				elseif($profile->occupation == 4)
					$msg = $msg . "व्यसाय :  Doctor \n";
				elseif($profile->occupation == 5)
					$msg = $msg . "व्यसाय : Teacher \n";
			}
			if($profile->yearly_income != null && $profile->yearly_income != '') {
				if($profile->yearly_income > 100000)
					$msg = $msg . "वार्षिक आय : ".($profile->yearly_income)/100000 ." LPA \n";
				else
					$msg = $msg . "वार्षिक आय : ".($profile->yearly_income) ." LPA \n";
			}
			if($profile->current_city != null && $profile->current_city != '')
				$msg = $msg . "कार्य स्थान : ".$profile->current_city."\n";

			$msg = $msg ."\n ____________________________ \n";
			$msg = $msg ."\n👪 *पारिवारिक विवरण* \n";
			if($profile->house_type != null ) {
				if($profile->house_type == 0)
					$msg = $msg . "घर : Owned \n";
				elseif($profile->house_type == 1)
					$msg = $msg . "घर : Rented \n";
			}
			if($profile->family_type != null) {
				if($profile->family_type == 0)
					$msg = $msg . "परिवार : Nuclear \n";
				elseif($profile->family_type == 1)
					$msg = $msg . "परिवार : Joint \n";
			}
			if ($profile->religion != null) {
				if($profile->religion == 0)
					$msg = $msg . "धर्म : Hindu \n";
				elseif($profile->religion == 1)
					$msg = $msg . "धर्म : Muslim \n";
				elseif($profile->religion == 2)
					$msg = $msg . "धर्म : Christian \n";
				elseif($profile->religion == 1)
					$msg = $msg . "धर्म : Sikh \n";
				elseif($profile->religion == 1)
					$msg = $msg . "धर्म : Jain \n";
			}
			if($profile->family_income != null && $profile->family_income != '') {
				if($profile->family_income > 100000)
					$msg = $msg . "पारिवारिक आय : ".($profile->family_income)/100000 ." LPA \n";
				else
					$msg = $msg . "पारिवारिक आय : ".($profile->family_income) ." LPA \n";
			}
			if($profile->father_occupation != null ) {
				if($profile->father_occupation == 0)
					$msg = $msg . "पिता का व्यसाय : Not Working \n";
				elseif($profile->father_occupation == 1)
					$msg = $msg . "पिता का व्यसाय : Private Company \n";
				elseif($profile->father_occupation == 2)
					$msg = $msg . "पिता का व्यसाय : Business/Self Employed \n";
				elseif($profile->father_occupation == 3)
					$msg = $msg . "पिता का व्यसाय : Government Job \n";
				elseif($profile->father_occupation == 4)
					$msg = $msg . "पिता का व्यसाय :  Doctor \n";
				elseif($profile->father_occupation == 5)
					$msg = $msg . "पिता का व्यसाय : Teacher \n";
			}
			if($profile->mother_occupation != null ) {
				if($profile->mother_occupation == 0)
					$msg = $msg . "माता का व्यसाय : Not Working \n";
				elseif($profile->mother_occupation == 1)
					$msg = $msg . "माता का व्यसाय : Private Company \n";
				elseif($profile->mother_occupation == 2)
					$msg = $msg . "माता का व्यसाय : Business/Self Employed \n";
				elseif($profile->mother_occupation == 3)
					$msg = $msg . "माता का व्यसाय : Government Job \n";
				elseif($profile->mother_occupation == 4)
					$msg = $msg . "माता का व्यसाय :  Doctor \n";
				elseif($profile->mother_occupation == 5)
					$msg = $msg . "माता का व्यसाय : Teacher \n";
			}

			$msg = $msg . "\n👉कृपया *YES* लिखे अगर रूचि हो अथवा *NO*\n \n👉 अगर आप शादी-शुदा हैं या रिश्ते नहीं खोज रहे कृपया *STOP* लिखे। \n \n";

			if ($daily_quota < $this->max_profile) {
				$msg = $msg ."\n🔎आज के लिए *". ($this->max_profile-1-$daily_quota) ." रिश्ते* और हैं।\n";
			}
			else if ($daily_quota == $this->max_profile) {
				$msg = $msg . "\n🕓 हम आपको ".$this->max_profile." रिश्ते दिखा चुके हैं। \n\n👉कृपया कल पुनः *SHOW* लिख कर भेजे। ";
			}
		}
		else {
			$msg = $msg .$emoji." *RECOMMENDED PROFILE BY HANS MATRIMONY* \n\n *By ".$matchmaker->first_name ." ". $matchmaker->last_name."* \n";
			if($profile->profile_photo != null && $profile->profile_photo != '')
				$msg = $msg ."\n *Photo* : ".$url;
			$msg = $msg ."\n ____________________________ \n";
			$msg = $msg. "\n" . $emoji ."‍ *PERSONAL DETAILS* \n";
			if($profile->name != null && $profile->name !='')
				$msg = $msg. "Name : ".$profile->name."\n";
			$today = date('Y-m-d', time());
			$birthDate = $profile->birth_date;
			$age = date('Y', strtotime($today)) - date('Y', strtotime($birthDate));
			if($age != null && $age != '')
				$msg = $msg ."Age : ".$age."\n";
			$height = floor($profile->height/12)."ft ".($profile->height%12)."in.";
			if($profile->height != null && $profile->height != '')
				$msg = $msg ."Height : ".$height."\n";
			if($profile->weight != null && $profile->weight != '')
				$msg = $msg. "Weight : ".$profile->weight."\n";
			if($profile->caste_id != null && $profile->caste_id != '') {
				$caste = Caste::where('id', $profile->caste_id)->first();
				$msg = $msg. "Caste : ".$caste->caste."\n";
			}
			if($profile->gotra != null && $profile->gotra != '')
				$msg = $msg. "Gotra : ".$profile->gotra."\n";
			if($profile->food_choice != null && $profile->food_choice != '') {
				if($profile->food_choice == 0)
					$msg = $msg ."Food Choice : Vegeterain \n";
				elseif($profile->food_choice == 1)
					$msg = $msg ."Food Choice : Non-Vegeterain \n";
			}

			if($profile->marital_status != null && $profile->marital_status != '') {
				if($profile->marital_status == 0)
					$msg = $msg . "Marital Status : Never Married\n";
				elseif($profile->marital_status == 1)
					$msg = $msg . "Marital Status : Divorcee\n";
				elseif($profile->marital_status == 2)
					$msg = $msg . "Marital Status : Widowed\n";
			}

			$msg = $msg ." ___________________________ \n";
			$msg = $msg ."\n ♋ *HOROSCOPE DETAILS* \n";
			if($profile->birth_date != null && $profile->birth_date != '')
				$msg = $msg . "Birth Date : ".date("Y-m-d", strtotime($profile->birth_date))."\n";
			if($profile->birth_time != null && $profile->birth_time != '')
				$msg = $msg . "Birth Time : ".$profile->birth_time."\n";
			if($profile->birth_place != null && $profile->birth_place != '')
				$msg = $msg . "Birth Place : ".$profile->birth_place."\n";
			if($profile->manglik != null) {
				if($profile->manglik == 0)
					$msg = $msg . "Manglik Status : Non-Manglik \n";
				elseif($profile->manglik == 1)
					$msg = $msg . "Manglik Status : Manglik \n";
				elseif($profile->manglik == 2)
					$msg = $msg . "Manglik Status : Anshik Manglik \n";
			}

			$msg = $msg ." ____________________________ \n";
			$msg = $msg ."\n 📚 *EDUCATION DETAILS* \n";
			if($profile->education != null && $profile->education != '')
				$msg = $msg . "Education : ".$profile->education."\n";
			if($profile->degree_id != null && $profile->degree_id != '') {
				$degree = Degree::where('id', $profile->degree_id)->first();
				$msg = $msg . "Degree : ".$degree->name." \n";
			}
			if($profile->college != null && $profile->college != '')
				$msg = $msg . "College : ".$profile->college."\n";

			$msg = $msg. " ____________________________ \n";
			$msg = $msg. "\n 💼 *JOB DETAILS* \n";
			if($profile->occupation != null) {
				if($profile->occupation == 0)
					$msg = $msg . "Occupation : Not Working \n";
				elseif($profile->occupation == 1)
					$msg = $msg . "Occupation : Private Company \n";
				elseif($profile->occupation == 2)
					$msg = $msg . "Occupation : Business/Self Employed \n";
				elseif($profile->occupation == 3)
					$msg = $msg . "Occupation : Government Job \n";
				elseif($profile->occupation == 4)
					$msg = $msg . "Occupation : Doctor \n";
				elseif($profile->occupation == 5)
					$msg = $msg . "Occupation : Teacher \n";
				
			}
			if($profile->yearly_income != null && $profile->yearly_income != '') {
				if($profile->yearly_income > 100000)
					$msg = $msg . "Annual Income : ".($profile->yearly_income)/100000 ." LPA \n";
				else
					$msg = $msg . "Annual Income : ".($profile->yearly_income) ." LPA \n";
			}
			if($profile->current_city != null && $profile->current_city != '')
				$msg = $msg . "Working City : ".$profile->current_city."\n";

			$msg = $msg ." ____________________________ \n";
			$msg = $msg ."\n👪 *FAMILY DETAILS* \n";
			if($profile->house_type != null) {
				if($profile->house_type == 0)
					$msg = $msg . "House : Owned \n";
				elseif($profile->house_type == 1)
					$msg = $msg . "House : Rented \n";
			}
			if($profile->family_type != null) {
				if($profile->family_type == 0)
					$msg = $msg . "Family : Nuclear \n";
				elseif($profile->family_type == 1)
					$msg = $msg . "Family : Joint \n";
			}
			if ($profile->religion != null) {
				if($profile->religion == 0)
					$msg = $msg . "Religion : Hindu \n";
				elseif($profile->religion == 1)
					$msg = $msg . "Religion : Muslim \n";
				elseif($profile->religion == 2)
					$msg = $msg . "Religion : Christian \n";
				elseif($profile->religion == 1)
					$msg = $msg . "Religion : Sikh \n";
				elseif($profile->religion == 1)
					$msg = $msg . "Religion : Jain \n";
			}
			if($profile->family_income != null && $profile->family_income != '') {
				if($profile->family_income > 100000)
					$msg = $msg . "Family Income : ".($profile->family_income)/100000 ." LPA \n";
				else
					$msg = $msg . "Family Income : ".($profile->family_income) ." LPA \n";
			}
			if($profile->father_occupation != null) {
				if($profile->father_occupation == 0)
					$msg = $msg . "Father's Occupation : Not Working \n";
				elseif($profile->father_occupation == 1)
					$msg = $msg . "Father's Occupation : Private Company \n";
				elseif($profile->father_occupation == 2)
					$msg = $msg . "Father's Occupation : Business/Self Employed \n";
				elseif($profile->father_occupation == 3)
					$msg = $msg . "Father's Occupation : Government Job \n";
				elseif($profile->father_occupation == 4)
					$msg = $msg . "Father's Occupation : Doctor \n";
				elseif($profile->father_occupation == 5)
					$msg = $msg . "Father's Occupation : Teacher \n";
			}
			if($profile->mother_occupation != null) {
				if($profile->mother_occupation == 0)
					$msg = $msg . "Mother's Occupation : Not Working \n";
				elseif($profile->mother_occupation == 1)
					$msg = $msg . "Mother's Occupation : Private Company \n";
				elseif($profile->mother_occupation == 2)
					$msg = $msg . "Mother's Occupation : Business/Self Employed \n";
				elseif($profile->mother_occupation == 3)
					$msg = $msg . "Mother's Occupation : Government Job \n";
				elseif($profile->mother_occupation == 4)
					$msg = $msg . "Mother's Occupation : Doctor \n";
				elseif($profile->mother_occupation == 5)
					$msg = $msg . "Mother's Occupation : Teacher \n";
			}

			$msg = $msg . "\n👉 If interested please reply *YES*.\n \n👉 If not interested please reply *NO*\n ";

			if ($daily_quota < $this->max_profile) {
				$msg = $msg ."\n🔎 *". ($this->max_profile-1-$daily_quota) ." profiles* left for today.";
			}
			else if ($daily_quota == $this->max_profile) {
				$msg = $msg . "\n🕓 You have already seen ".$this->max_profile." profiles. Please come back tommorrow and message *SHOW* to continue seeing more profiles.";
			}
		}
		return str_replace('null', ' ', $msg);
	}

	public function updateCompatibleTable($client_id, $current, $profile_status, $compatibleId, $daily_quota) {
		$profile_status = ($profile_status == null || $profile_status == '')? [] : json_decode($profile_status);
		if($current == 1) {
			$elem = [];
			$elem['client_id'] = $compatibleId;
			$elem['status'] = 'P';
			array_push($profile_status, $elem);
			$profile_status = json_encode($profile_status);
			Compatibility::where('client_id', $client_id)->update(['current' => $current, 'profile_status' => $profile_status, 'active' => $compatibleId, 'daily_quota' => $daily_quota]);
		}
	}

	public function calculate($id){
		$my_profile = ClientProfile::where('id', $id)->first();
		if($my_profile == null)
			return;

		$height=$my_profile->height;
		$last_id = (DB::table('compatibilities')->where('client_id', $my_profile->id)->first() == null)? 0 : DB::table('compatibilities')->where('client_id', $my_profile->id)->first()->last_id;
		$last_id = $last_id != null ? $last_id : 0;
		$birth_date = $my_profile->birth_date;
		$myPreferences = Preference::where('client_id', $id)->first();
		if($myPreferences == null)
			return;

	        $my_profile->prefered_caste = explode(',', $myPreferences->caste_id);
		$my_profile->preferred_manglik = $myPreferences->manglik;
		$my_profile->preferred_min_income = $myPreferences->min_income;

		if($my_profile->preferred_manglik == null) {
			if($my_profile->manglik == 1 ) {
				$mangliks = [1, 2];
			}
			elseif ($my_profile->manglik == 0) {
				$mangliks = [0, 2];
			}
			else{
				$mangliks = [0, 1, 2];
			}
		}
		else
			$mangliks[] = $my_profile->preferred_manglik;
    // min. income for male profiles when calculating for female profiles
		if ($my_profile->preferred_min_income == null) {
			$min_income = $my_profile->yearly_income + 200000;
		}
		else 
			$min_income = $my_profile->preferred_min_income;

		$defaultCastes = 0;
		if($myPreferences->caste_id == null) {
			if(!($my_profile->caste_id == null)) {
				$my_profile_caste = Caste::where('id', $my_profile->caste_id)->first()->caste;
				$mapping_id = json_decode(file_get_contents('https://partner.hansmatrimony.com/api/caste_map_mm?caste='.$my_profile_caste));

				if($mapping_id != null && $mapping_id != 0) {
					$castes = file_get_contents('https://partner.hansmatrimony.com/api/caste_mapping');
					$castes = json_decode($castes)[$mapping_id-1];
					$my_profile->prefered_caste = explode(',', $castes->castes);

					foreach ($my_profile->prefered_caste as $caste) {
						if(Caste::where('caste', $caste)->exists())
							$preferred_caste_id[] = Caste::where('caste', $caste)->first()->id;
					}

				}
				else $defaultCastes = 1; 
			}
			else
				$defaultCastes = 1;
		}
		else {
			if ($myPreferences->caste_id == 0)
				$defaultCastes = 1;
			else
				$preferred_caste_id = $my_profile->prefered_caste;
		}

		$compatibility_exist = Compatibility::where('client_id', $id)->exists();
		if($compatibility_exist) {
			$compatibility = Compatibility::where('client_id', $id)->first();
			$extra = $compatibility->extra;
			$profile_status = json_decode($compatibility->profile_status) ? json_decode($compatibility->profile_status) : [];
			$sent_profiles_id = [];
			if($profile_status) {
				foreach ($profile_status as $key) {
					array_push($sent_profiles_id, $key->client_id);
				}
			}
		}

		if($my_profile->gender== 0){
			$gender= 1;
			if($defaultCastes == 1){
				$query = DB::table('client_clientprofile')
				->where('gender',$gender)
				->whereRaw("(birth_date >= '".$birth_date."' && birth_date < '".Carbon::parse($birth_date)->addYears(5)."')");
				if($compatibility_exist) {
					if($extra == 0)
						$query = $query->where('id','>=',$last_id);
					$query = $query->whereNotIn('id', $sent_profiles_id);
				}

				$query = $query->whereBetween('height',array($height-9,$height-1))
				->where('marital_status',$my_profile->marital_status)
				->where('yearly_income','<',$my_profile->yearly_income)
				->whereIN('manglik', $mangliks)
				->get();

			} else {
        //echo 'not_open_bar ';
				$query = DB::table('client_clientprofile')
				->where('gender',$gender)
				->whereRaw("(birth_date >= '".$birth_date."' && birth_date < '".Carbon::parse($birth_date)->addYears(5)."')");
				if($compatibility_exist) {
					if($extra == 0)
						$query = $query->where('id','>=',$last_id);
					$query = $query->whereNotIn('id', $sent_profiles_id);
				}

				$query = $query->whereBetween('height',array($height-9,$height-1))
				->where('marital_status',$my_profile->marital_status)
				->where('yearly_income','<',$my_profile->yearly_income)
				->whereIN('caste_id', $preferred_caste_id)
				->whereIN('manglik', $mangliks)
				->get();
			}
        // echo 'HF-'.$query->count().'<br>';
			if($query->count() == 0)
			{
        $query = DB::table('client_clientprofile') // Soft filter
        ->where('gender',$gender)
        ->where('id','>=',$last_id)
        ->whereRaw("(birth_date >= '".$birth_date."' && birth_date < '".Carbon::parse($birth_date)->addYears(5)."')");
        if($compatibility_exist) {
		if($extra == 0)
			$query = $query->where('id','>=',$last_id);
		$query = $query->whereNotIn('id', $sent_profiles_id);
	}
        $query = $query->where('marital_status',$my_profile->marital_status);
        if(isset($preferred_caste_id))
        	$query = $query->whereIN('caste_id', $preferred_caste_id);
        $query = $query->get();
        // echo 'SF-'.$query->count().'<br>';

      }
    }
    else if($my_profile->gender== 1){
    	$gender= 0;
    	if($defaultCastes == 1){
    		$query = DB::table('client_clientprofile')
    		->where('gender',$gender)
    		->whereRaw("(birth_date <= '".$birth_date."' && birth_date > '".Carbon::parse($birth_date)->subYears(5)."')");
    		if($compatibility_exist) {
			if($extra == 0)
				$query = $query->where('id','>=',$last_id);
			$query = $query->whereNotIn('id', $sent_profiles_id);
		}
    		$query = $query->where('height','>',$height+1)
    		->where('marital_status',$my_profile->marital_status)
    		->where('yearly_income','>',$min_income)
    		->whereIN('manglik', $mangliks)
    		->get();
    	} else {
        //echo 'not_open_bar ';
    		$query = DB::table('client_clientprofile')
    		->where('gender',$gender)
    		->whereRaw("(birth_date <= '".$birth_date."' && birth_date > '".Carbon::parse($birth_date)->subYears(5)."')");
    		if($compatibility_exist) {
			if($extra == 0)
				$query = $query->where('id','>=',$last_id);
			$query = $query->whereNotIn('id', $sent_profiles_id);
		}

    		$query = $query->where('height','>',$height+1)
    		->where('marital_status',$my_profile->marital_status)
    		->where('yearly_income','>',$min_income)
    		->whereIN('caste_id', $preferred_caste_id)
    		->whereIN('manglik', $mangliks)
    		->get();
    	}
      // echo 'HF-'.$query->count().'<br>';

      if($query->count() == 0) // Soft filter
      {
      	$query = DB::table('client_clientprofile')
      	->where('gender',$gender)
      	->whereRaw("(birth_date <= '".$birth_date."' && birth_date > '".Carbon::parse($birth_date)->subYears(5)."')");
      	if($compatibility_exist) {
		if($extra == 0)
			$query = $query->where('id','>=',$last_id);
		$query = $query->whereNotIn('id', $sent_profiles_id);
	}
      	$query = $query->where('marital_status',$my_profile->marital_status);
      	if(isset($preferred_caste_id))
      		$query = $query->whereIN('caste_id', $preferred_caste_id);
      	$query = $query->get();
        // echo 'SF-'.$query->count().'<br>';

      }

    }
    // $profile_status = json_decode($profile_status);
    //create a json object for blank compatibility
    $data = [];
    foreach ($query as $label)
    { 
    	$age_values =$this->age_compatibility($id,$label->id);
    	$hei_values =$this->height_compatibility($id,$label->id);
    	$bmi_values = $this->bmi_compatibility($id,$label->id);
    	$freshness = $this->freshness($id, $label->id);
      // echo "For= => ".$label->id."-----".number_format(110*$age_values+100*$hei_values+100*$bmi_values+$freshness/180, 2, '.', '')."----";
    	$data[] = [
    		'client_id' => $label->id,
    		'value' => number_format(110*$age_values+100*$hei_values+100*$bmi_values+$freshness/180, 2, '.', ''),
    	];

    }

    $c_array =  collect($data)->sortBy('value')->reverse()->take(300);
    $data = [];
    foreach ($c_array as $val)
    {
    	$data[] = [
    		'client_id' => $val['client_id'],
    		'value' => $val['value'],
    	];
    }
    $data =json_encode($data);
    $last_id = $query->max('id');
    if($query->count() > 300)
    	$extra = 1;
    else 
    	$extra = 0;

    if(DB::table('compatibilities')->where('client_id','=',$id)->exists()){
    	DB::table('compatibilities')->where('client_id',$id)->update([
    		'compatibility' => $data,
    		'last_id'=> $last_id,
    		'extra' => $extra,
    	]);
    }
    else{
    	DB::table('compatibilities')->insert([
    		'client_id' => $id,
    		'compatibility' =>$data,
    		'last_id'=> $last_id,
    		'extra' => $extra,
    	]);
    }
    // echo "Inserted!\n";
  }

  public function age_compatibility($my_id,$your_id){
  	$myage = ClientProfile::where('id', $my_id)->first();
  	$your_age = ClientProfile::where('id', $your_id)->first();
  	$myage1 = strtotime($myage->birth_date) ;
  	$yourage1 = strtotime($your_age->birth_date) ;
  	$age_diff = ($yourage1-$myage1)/86400;
  	if($age_diff < 0)
  		$age_diff = ($myage1 - $yourage1)/86400;
  	$points=0;
  	if($age_diff > 730 && $age_diff <1460){
  		$points = 3;
  	}
  	elseif($age_diff >365 && $age_diff < 1825){
  		$points = 2;
  	}
  	elseif ($age_diff >0 && $age_diff < 2190){
  		$points = 1;
  	}
  	else{
  		$points = 0;
  	}
    //normalisation
  	$points = number_format(((float)$points-2.2)/0.76, 2, '.', '');
  	return $points;
  }

  public function height_compatibility($my_id,$your_id){
  	$me = ClientProfile::where('id', $my_id)->first();
  	$you = ClientProfile::where('id', $your_id)->first();
  	$myheight=$me->height;
  	$yourheight = $you->height;
  	$h_diff = $myheight-$yourheight;
  	if ($h_diff < 0)
  		$h_diff = $yourheight - $myheight;
  	$points =0;
  	if($h_diff == 5){
  		$points=4;
  	}
  	elseif ($h_diff ==4 || $h_diff ==6){
  		$points = 3;
  	}
  	elseif ($h_diff==3 || $h_diff == 7){
  		$points = 2;
  	}
  	elseif ($h_diff == 2 || $h_diff ==8 || $h_diff == 1){
  		$points = 1;
  	}
  	else{
  		$points = 0;
  	}
  	$points = number_format(((float)$points-1.87)/1.34, 2, '.', '');
  	return $points;
  }

  public function bmi_compatibility($my_id,$your_id){
  	$me = ClientProfile::where('id', $my_id)->first();
  	$you = ClientProfile::where('id', $your_id)->first();
  	$myheight=$me->height;
  	$myweight = $me->weight;
  	$yourheight =$you->height;
  	$yourweight = $you->weight;

  	if($myheight == 0 || $yourheight==0)
  	{
  		$mybmi = 17;
  		$yourbmi = 20;
  	}
  	else{
  		$mybmi = $myweight/(($myheight*0.0254)*($myheight*0.0254));
  		$yourbmi = $yourweight/(($yourheight*0.0254)*($yourheight*0.0254));
  	}
  	if($me->gender == 0)
  		$bmi_diff = $mybmi-$yourbmi;
  	elseif ($me->gender == 1)
  		$bmi_diff = $yourbmi-$mybmi;
  	if ($bmi_diff >0 && $bmi_diff <1) {
  		$points = 1;
  	}
  	elseif ($bmi_diff >=1 && $bmi_diff<2){
  		$points =2;
  	}
  	elseif ($bmi_diff >=2 && $bmi_diff<3){
  		$points =3;
  	}
  	elseif ($bmi_diff >=3 && $bmi_diff<4){
  		$points =3;
  	}
  	elseif($bmi_diff>=4 && $bmi_diff <= 5){
  		$points =1;
  	}
  	else{
  		$points =0;
  	}
  	$points = number_format(((float)$points-0.08)/0.42, 2, '.', '');
  	return $points;
  }

  public function freshness($my_id, $your_id)
  {
  	$you = ClientProfile::where('id', $your_id)->first();
  	$diff = date_diff(date_create($you->created_at), date_create(date('Y-m-d')));
  	$y = -($diff->format("%R%a"));
  	$points = number_format(((float)$y + 80.07)/37.88, 2, '.', '');
  	return $points;
  }

}
