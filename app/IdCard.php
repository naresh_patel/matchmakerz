<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IdCard extends Model
{
    public $timestamps = false;

    protected $table = 'matchmaker_idcard';

    protected $fillable = ['id_card', 'matchmaker_id'];
}
