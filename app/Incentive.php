<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Incentive extends Model
{
    protected $table = 'matchmaker_incentives';

    public $timestamps = false;

    protected $fillable = ['Sale', 'Marriage_fixing'];
}
