<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IncompleteHiringLeads extends Model
{
	protected $fillable = ['call_time', 'call_duration', 'recording_link', 'area', 'connected_to', 'connected_person', 'status', 'parent_id', 'user_phone', 'channel', 'call_time_stamp'];

	public function getCallTimeAttribute($value)
	{
		return \Carbon\Carbon::parse($value)->format('d/m/Y h:i:s A');
	}

}
