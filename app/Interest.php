<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Interest extends Model
{
	protected $table = 'client_showinterest';

	protected $appends = ['matched_to', 'matched_for'];

	public function getMatchedToAttribute()
	{
		$matched_to = ClientProfile::where('id', $this->matched_to_id)->first();
		return $matched_to;
	}

	public function getMatchedForAttribute()
	{
		$matched_for = ClientProfile::where('id', $this->matched_for_id)->first();
		return $matched_for->id;
	}
}
