<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lead extends Model
{
	protected $table = 'matchmaker_lead';

	public $timestamps = false;

	protected $fillable = ['phone_number', 'leads', 'matchmaker_id', 'name', 'appoinment_date'];

	protected $appends = ['follow_update'];

	// public function getLeadsAttribute($leads)
	// {
	// 	if($leads) {
	// 		$leads = explode(',20', $leads);
	// 		for ($i=1; $i < sizeof($leads); $i++) {
	// 			$leads[$i] = "20".$leads[$i];
	// 		}
	// 		return $leads;
	// 	}
	// 	else
	// 		return ;
	// }

	public function getFollowUpdateAttribute()
	{
		try {
			$leads = explode(',20', $this->leads);
			$last_update = $leads[sizeof($leads) - 1];
			$follow_up_date = explode(':', $last_update)[0];
			return $follow_up_date;
		} catch(\Exception $e) {
			return ;
		}
	}
}
