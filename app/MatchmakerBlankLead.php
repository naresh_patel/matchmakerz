<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MatchmakerBlankLead extends Model
{
	public $timestamps = false;
	protected $table = 'matchmaker_blankleadmatchker';
	protected $fillable = ['leads', 'matchmaker_id', 'is_full'];
}
