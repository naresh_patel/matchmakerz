<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MatchmakerFollowup extends Model
{
	protected $fillable = ['matchmaker_id', 'comments', 'followup_date', 'assigned_to'];
}
