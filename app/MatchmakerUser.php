<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MatchmakerUser extends Model
{
	protected $table = 'matchmaker_user';

	protected $fillable = ['password', 'last_login', 'is_superuser', 'first_name', 'last_name', 'phone_number', 'unique_referral_code',
	'email', 'about', 'unique_about', 'specialization', 'profile_pic', 'gender', 'is_staff', 'is_active', 'otp', 'otp_expire', 'age',
	'is_deleted', 'latitude', 'longitude', 'experience', 'device_id', 'upfront_charge', 'referred_by', 'whatsapp_number', 'credits', 'matchmaker_type',
	'plan', 'review', 'validity', 'parent_id', 'child','working_status','client_bonus','withdrawlrequest','wallet'];

	protected $appends = ['incentives', 'matchmaker_id_card'];

	protected $attributes = [
		'credits' => 0,
		'profile_pic' => '',
		'otp' => '',
		'otp_expire' => '1990-01-01',
		'upfront_charge' => 3100,
		'is_deleted' => 0,
		'location' => 'Bali Nagar, New Delhi',
		'latitude' => '28.653584',
		'longitude' => '77.12926999999999',
		'plan' => 0,
		'review' => 0,
		'validity' => 30,
		'referred_by' => '',
		'matchmaker_type' => 0,
	];

	public function getProfilePicAttribute($profile_pic)
	{
		if($profile_pic)
			return "https://matchmakerz.s3.ap-south-1.amazonaws.com/".$profile_pic;
		else
			return ;
	}

	public function getMatchmakerIdCardAttribute()
	{
		try {
			$id_card = IdCard::where('matchmaker_id', $this->id)->first();
			return "https://matchmakerz.s3.ap-south-1.amazonaws.com/".$id_card->id_card;
		} catch(\Exception $e) {
			return "NA";
		}
	}

	public function getIncentivesAttribute()
	{
		$incentives = Incentive::first();
		$sale = explode(',', $incentives->Sale);
		$marriage_fix = explode(',', $incentives->Marriage_Fixing);

		$data['Sale_Split']['self'] = $sale[0];
		$data['Sale_Split']['first_level'] = $sale[1];
		$data['Sale_Split']['second_level'] = $sale[2];
		$data['Sale_Split']['total'] = $sale[0]+$sale[1]+$sale[2];

		$data['Marriage_Split']['self'] = $marriage_fix[0];
		$data['Marriage_Split']['first_level'] = $marriage_fix[1];
		$data['Marriage_Split']['second_level'] = $marriage_fix[2];
		$data['Marriage_Split']['total'] = $marriage_fix[0]+$marriage_fix[1]+$marriage_fix[2];

		return $data;
	}
}
