<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MatchmakersOnboarded extends Model
{
	protected $table = 'matchmakers_onboarded';
	protected $fillable = ['matchmaker_id', 'comments', 'followup_date'];
}
