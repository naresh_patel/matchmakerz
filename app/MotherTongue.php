<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MotherTongue extends Model
{
    protected $table = 'client_mothertongue';

    protected $fillable = ['name'];
}
