<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Otp extends Model
{
    protected $table = 'matchmaker_otp';
    protected $fillable = ['mobile_number', 'otp', 'otp_expire', 'otp_verify'];
}
