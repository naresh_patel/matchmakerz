<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Plans extends Model
{
    protected $table = 'matchmaker_plans';

    protected $fillable = ['matchmaker_id', 'plan_name', 'meeting_charge', 'before_roka_charge', 'meetings', 'validity', 'min_family_income', 'max_family_income'];

    protected $appends = ['matchmaker_name'];

    public function getMatchmakerNameAttribute()
    {
    	$matchmaker = MatchmakerUser::where('id', $this->matchmaker_id)->first();
    	if($matchmaker)
		return $matchmaker->first_name. ' '. $matchmaker->last_name;
	else return ;
    }

}
