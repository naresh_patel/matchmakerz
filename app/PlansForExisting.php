<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlansForExisting extends Model
{
    protected $table = 'matchmaker_plansforexisting';

    protected $fillable = ['plan_name', 'number_of_clients', 'description', 'credits', 'amount', 'validity', 'platforms', 'additional_features', 
    'referral_credits', 'referral_validity', 'review_credits', 'review_validity'
  ];

  protected $attributes = [
  	'amount' => 0,
  	'credits' => 0,
  	'validity' => 30,
  	'referral_credits' => 200,
  	'referral_validity' => 30,
  	'review_validity' => 30,
  	'review_credits' => 200,
  ];

    public $timestamps = false;
}
