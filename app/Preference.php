<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Preference extends Model
{
	protected $table = 'client_preferences';

	protected $fillable= ['min_age', 'max_age', 'min_income', 'max_income', 'min_height', 'max_height', 'marital_status', 'manglik', 'food_choice',
	'occupation', 'citizenship', 'client_id', 'caste'
];

protected $appends = ['caste_id', 'gender'];

protected $attributes = [
	'min_income' => 1000,
	'max_income' => 100000000,
	'marital_status' => 0,
	'manglik' => 0,
	'food_choice' => 1,
	'occupation' => 1,
	'citizenship' => 0,

];

public function getGenderAttribute()
{
	$client = ClientProfile::where('id', $this->client_id)->first();
	try {
		return $client->gender;
	} catch(\Exception $e) {
		return "NA";
	}
}

public function getCasteIdAttribute()
{
	if($this->caste != null && $this->caste->count() > 0) {
		if(sizeof($this->caste) > 100)
			return 0;
		else {
			$ids = $this->caste[0]->id;
			$i = 1;
			for ($i=1; $i < sizeof($this->caste); $i++) {
				$ids .= ','.$this->caste[$i]->id;
			}
			return $ids;
		}
	}
	else return ;
}

public function getCasteAttribute($caste)
{
	if($caste != null) {
		if($caste != 0) {
			$caste_ids = explode(',', $caste);
			$castes = Caste::whereIn('id', $caste_ids)->get();
			return $castes;
		}
		elseif($caste == 0) {
			$castes = Caste::all();
			return $castes;
		}
		return ;
	}
	else {
		$preferred_castes = PreferenceCaste::where('preferences_id', $this->id)->pluck('caste_id');
		$castes = Caste::whereIn('id', $preferred_castes)->get();
		return $castes;
	}
}

}
