<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PreferenceCaste extends Model
{
    protected $table = 'client_preferences_caste';

    protected $fillable = ['preferences_id', 'caste_id'];

    public $timestamps = false;
}
