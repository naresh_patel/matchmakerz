<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RazorpayPlan extends Model
{
	protected $fillable = ['plan_id', 'plan_name', 'amount', 'subscription_id', 'client_id', 'subscription_link'];
}
