<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SharedProfile extends Model
{
    //models for profiles shared on whatsapp by matchmaker
	protected $fillable = ['sender_matchmaker_id', 'shared_profile_id', 'shared_to_id'];

	protected $appends = ['shared_profile'];

	public function getSharedProfileAttribute()
	{
		return ClientProfile::where('id', $this->shared_profile_id)->first();
	}

}
