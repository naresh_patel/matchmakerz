<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shortlist extends Model
{
	protected $table = 'client_shortlist';

	protected $fillable = ['shortlist_by_matchmaker_id', 'shortlist_to_matchmaker_id', 'shortlist_to_id', 'shortlist_for_id', 'delete'];

	protected $attributes = [
		'delete' => false,
	];

	protected $appends = ['shortlist_to', 'shortlist_for', 'shortlist_to_matchmaker', 'shortlist_by_matchmaker'];

	public function getShortlistToAttribute()
	{
		return ClientProfile::where('id', $this->shortlist_to_id)->first()->makeHidden(['whatsapp_number', 'phone_number']);
	}

	public function getShortlistForAttribute()
	{
		return $this->shortlist_for_id;
	}

	public function getShortlistToMatchmakerAttribute()
	{
		return $this->shortlist_to_matchmaker_id;
	}

	public function getShortlistByMatchmakerAttribute()
	{
		return $this->shortlist_by_matchmaker_id;
	}

	public function getDeleteAttribute($delete)
	{
		if($delete == 1)
			return true;
		else
			return false;
	}
}
