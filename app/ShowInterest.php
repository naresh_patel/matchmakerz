<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShowInterest extends Model
{
	protected $table = 'client_showinterest';

	protected $fillable = ['approved_by_receiver', 'matched_for_id', 'matched_to_id', 'receiver_matchmaker_id', 'sender_matchmaker_id', 'delete', 'reconnect_limit', 'seen'];

	protected $appends = ['matched_to', 'matched_for', 'matched_to_pic', 'matched_for_pic', 'matched_for_name', 'matched_to_name', 'mesg'];

	protected $attributes = [
		'reconnect_limit' => 0,
		'approved_by_receiver' => 2,
		'delete' => false,
		'seen' => 0,

	];

	public function getMatchedToAttribute()
	{
		$matched_to = ClientProfile::where('id', $this->matched_to_id)->first();
		return $matched_to->id;
	}

	public function getDeleteAttribute($delete)
	{
		if($delete == 1)
			return true;
		else
			return false;
	}

	public function getMatchedForAttribute()
	{
		$matched_for = ClientProfile::where('id', $this->matched_for_id)->first();
		return $matched_for->id;
	}

	public function getMatchedToPicAttribute()
	{
		$matched_to = ClientProfile::where('id', $this->matched_to_id)->first();
		if($matched_to) {
			if($matched_to->profile_photo)
				return $matched_to->profile_photo;
			else
				return ;
		}
		else
			return ;
	}

	public function getMatchedForPicAttribute()
	{
		$matched_for = ClientProfile::where('id', $this->matched_for_id)->first();
		if($matched_for) {
			if($matched_for->profile_photo)
				return $matched_for->profile_photo;
			else
				return ;
		}
		else
			return ;
	}

	public function getMatchedToNameAttribute()
	{
		$matched_to = ClientProfile::where('id', $this->matched_to_id)->first();
		if($matched_to) {
			return $matched_to->name;
		}
		else
			return ;
	}

	public function getMatchedForNameAttribute()
	{
		$matched_for = ClientProfile::where('id', $this->matched_for_id)->first();
		if($matched_for) {
			return $matched_for->name;
		}
		else
			return ;
	}

	public function getMesgAttribute()
	{
		if($this->approved_by_receiver == 2)
			return explode(' ', $this->matched_for_name)[0]." wants to connect with ".explode(' ', $this->matched_to_name)[0];

		elseif($this->approved_by_receiver == 1)
			return explode(' ', $this->matched_to_name)[0]." accepted your request for ".explode(' ', $this->matched_for_name)[0];

		else
			return explode(' ', $this->matched_to_name)[0]." declined interest for ".explode(' ', $this->matched_for_name)[0];
	}
}
