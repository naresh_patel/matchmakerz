<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tip extends Model
{
    protected $table = 'tips';

    protected $fillable = ['id', 'tip_name','message'];
}

