<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Token extends Model
{
	public $timestamps = false;
	protected $table = 'authtoken_token';
	protected $fillable = ['user_id', 'key', 'created'];

}
