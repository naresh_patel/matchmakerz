<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Transaction extends Model
{
	protected $table = 'matchmaker_transections';

	protected $fillable = ['reffer_credits', 'review_Credits', 'buy_credit_id', 'credits', 'matchmaker_id'];

	protected $attributes = [
		'review_Credits' => 0,
		'credits' => 0,
	];

}
