<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tutorial extends Model
{
    protected $table = 'matchmaker_tut';

    protected $fillable = ['name'];

    protected $appends = ['tutorials'];

    public function getTutorialsAttribute()
    {
    	return TutorialVideo::where('tutorialSet_id', $this->id)->get();
    }

}
