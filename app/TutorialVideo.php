<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TutorialVideo extends Model
{
    protected $table = 'matchmaker_tutorials';

    protected $fillable = ['title', 'video_link', 'description', 'is_video', 'pdf', 'tutorialSet_id'];

    public function getPdfAttribute($pdf)
    {
    	if($pdf == "null" || $pdf == '')
    		return null;
    	else
    		return $pdf;
    }

}
