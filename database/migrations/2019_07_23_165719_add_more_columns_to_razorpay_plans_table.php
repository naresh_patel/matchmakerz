<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMoreColumnsToRazorpayPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('razorpay_plans', function (Blueprint $table) {
            $table->string('subscription_id')->nullable();
            $table->bigInteger('client_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('razorpay_plans', function (Blueprint $table) {
            $table->dropColumn('subscription_id');
            $table->dropColumn('client_id');
        });
    }
}
