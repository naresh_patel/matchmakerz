<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMatchmakersOnboardedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('matchmakers_onboarded', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('matchmaker_id', 50);
            $table->string('comments')->nullable();
            $table->date('followup_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('matchmakers_onboarded');
    }
}
