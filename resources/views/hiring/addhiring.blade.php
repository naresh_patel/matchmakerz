@extends('layouts.app')
@section('content')

<div class="container">
  <div class="row">
    <div class="col-md-6 col-md-offset-3"><br>
      @if (Session::has('message'))
      <div class="alert alert-warning">
        <ul>
          <li style="list-style: none">
            <span class="glyphicon glyphicon-warning-sign"></span>
            {!! Session::get('message') !!}
          </li>
        </ul>
      </div>
      @endif
      <form method="post" action="{{route('addhiringlead')}}">
        {{csrf_field()}}
        <div class="panel panel-primary">
          <div class="panel-heading"><b>Add Hiring Leads Here:</b></div>
          <div class="panel-body">

            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="name">Name</label>
                  <input type="text" class="form-control" name="name">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="name">Mobile</label>
                  <input type="text" class="form-control" minLength="10" name="mobile" required>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="name">Enquiry Date</label>
                  <input type="date" class="form-control" name="enquiry_date" value="{{date('Y-m-d',time())}}">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="name">Follow-up Call on</label>
                  <input type="date" class="form-control" name="followup_call_on" value="{{date('Y-m-d', time())}}">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="age">Gender</label>
                  <select id="gender" name="gender" class="form-control" >
                    <option>--Select--</option>
                    <option value="Male">Male</option>
                    <option value="Female">Female</option>
                  </select>
                </div>
              </div>
            </div>
              <div class="row">
                <div class="col-md-8">
                  <div class="form-group">
                    <label for="name">Comments</label>
                    <textarea class="form-control" name="comments"></textarea>
                  </div>
                </div>
              </div><input type="hidden" value="0" name="is_done">


            </div>
            <div class="panel-footer">
              <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-plus"></span>Add</button>
            </div>
          </div>
        </form>

      </div>
    </div>

  </div>
  <script>
    $('input[name="mobile"]').val(localStorage.getItem('mobile'));
    localStorage.removeItem('mobile');
  </script>
  @endsection