@extends('layouts.app')

@section('content')
<div class="row">
  <div class="col-md-2">
    <div id="wrapper">
      <!-- Navigation -->
      <nav class="navbar navbar-inverse" role="navigation">
        <div class="sidebar-header">
          <center><h4><a href="#logout" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle" style="text-decoration: none; color: #fff;">Admin Dashboard&nbsp;&nbsp;&nbsp;<i class="fa fa-caret-down" aria-hidden="true"></i></a></h4>
            <ul class="collapse list-unstyled" id="logout">
              <li>
                <a href="#" style="color: #fff;"><i class="fa fa-sign-out" aria-hidden="true"></i>&nbsp;Logout</a>
              </li>
            </ul>
          </center>
        </div>
        <ul class="nav navbar-nav side-nav" style="overflow-x: hidden;">
          <li class="navitems col-sm-12">
            <a href="{{ route('overview') }}" class="items"><i class="fa fa-bar-chart"></i> Dashboard Overview</a>
          </li>
          <li class="navitems col-sm-12 active">
            <a href="{{ route('hiring') }}" class="items"><i class="fa fa-users"></i> Facebook Hiring Leads </a>
          </li>
          <li class="navitems col-sm-12">
            <a href="{{ route('matchmakerz') }}" class="items"><i class="fa fa-fw fa-paper-plane-o"></i> Matchmakers Onboarded </a>
          </li>
          <li class="navitems col-sm-12">
            <a href="{{ route('firstClient') }}" class="items"><i class="fa fa-fw fa-user-plus"></i> Free Client Added</a>
          </li>
          <li class="navitems col-sm-12">
            <a href="{{ route('clientConverted') }}" class="items"><i class="fa fa-exchange" aria-hidden="true"></i> Paid Client Converted    </a>
          </li>
        </ul>
      </nav>
    </div>
  </div>
  <div class="col-md-10" style="padding-right: 45px;">
    <br/>
    <div class="row">
      <div class="col-sm-6">
        <form method="GET" action="">
          <label>Search in Open Hiring Leads :</label>
          <input type="text" class="form-control" name="search" placeholder="Search for assign by, or other details"/>
          <br/>
          <input type="submit" class="btn btn-primary btn-sm" value="Search" name="find" />
          <input type="reset" class="btn btn-danger btn-sm" value="Reset" name="reset" /><br><br>
        </form>
      </div>
      <div class="col-sm-6">
        <form method="GET" action="">
          <label>Search in Rejected Hiring Leads :</label>
          <input type="text" class="form-control" name="rejectedleads" placeholder="Search for assign by, or other details"/>
          <br>
          <input type="submit" class="btn btn-primary btn-sm" value="Search" name="find" />
          <input type="reset" class="btn btn-danger btn-sm" value="Reset" name="reset" />
        </form>
      </div>
    </div>
    <br/>
    <a href="{{route('hiring')}}"><button class="btn btn-success btn-sm">Clear Search</button></a>
    <div class="text-center">
      <a href="{{route('addhiringlead')}}">
        <button class="btn btn-success btn-sm"><span class="glyphicon glyphicon-plus"></span> Add Lead</button>
      </a>
      <a href="{{route('incompletehiringleads')}}">
        <button class="btn btn-danger btn-sm"><span class="glyphicon glyphicon-flash"></span></span>Incomplete Leads</button>
      </a>
      <input type="button" onclick="printDiv('printableArea')" class="btn pull-right" id="background" value="Print" />

    </div>
    <br/>

    <div class="row">
      <div class="panel" style="margin-top: 15px;">
        <div class="panel-heading" style="background-color: #343a40; color: #fff">
          <div class="row">
            <div class="col-sm-12" style="font-size: 18px;"><b>Convert Open Hiring Lead</b></div>
          </div>
          <div class="row" style="margin-top: 20px;">
            <div class="col-sm-4">
              <form action="{{ route('hiring')}}">
                {{csrf_field()}}
                <div class="form-group">
                  <label for="phone_number">Phone Number: </label>
                  <input type="text" name="phone_number" id="phone_number" value="<?php if(isset($phone_number)) echo $phone_number; ?>" class="form-control" minlength="10" maxlength="10" required>
                </div>
                <div class="form-group" style="margin-top: 25px;">
                  <input type="submit" name="submit" value="Search" class="btn btn-sm btn-success">
                </div>
              </form>
            </div>
            <div class="col-sm-4">
              <div class="form-group" style="font-size: 15px;">
                @if(isset($_GET['phone_number']))
                @if($matchmaker)
                <label>Name: {{$matchmaker->first_name}} {{$matchmaker->last_name}}</label><br>
                <label>Age: {{$matchmaker->age}}</label><br>
                <label>Phone Number: {{$matchmaker->phone_number}}</label><br>
                @if(!$open_lead)
                <label style="color: red">This number is not in open lead!</label>
                @else
                <form action="{{ route('deletehiringlead')}}" method="post">
                  {{csrf_field()}}
                  <input type="hidden" name="lead_id" value="{{$open_lead->id}}">
                  <div class="form-group" style="margin-top: 25px;">
                    <input type="submit" name="submit" value="Delete" class="btn btn-sm btn-danger">
                  </div>
                </form>
                @endif
                @else
                <label>No Matchmaker Found!</label>
                @endif
                @endif
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    @if(!empty($_GET['search']))
    @php
    $searched = $_GET['search'];
    if(strpos($searched, '/')) {
      $searched = date("Y-m-d", strtotime($searched));
    }
    $search_data = \App\Hiring::whereRaw("(( is_done= 0 ) AND (name LIKE '%".$searched."%'  OR mobile LIKE '%".$searched."%'  OR enquiry_date = '".$searched."'  OR followup_call_on = '".$searched."'   OR comments LIKE '%".$searched."%'))")->orderBy('followup_call_on', 'asc')->paginate(10);
    @endphp
    <center><h2>Open Hiring Leads based on your searched query</h2></center>
    <table class="table table-condensed table-striped table-bordered table-responsive">
      <tr class="active">
        <th>Whats App</th>
        <th>Mark as Converted</th>
        <th>Mark as Rejected</th>
        <th>Name</th>
        <th>Mobile</th>
        <th>Enquiry Date</th>
        <th>Follow-up Call On</th>
        <th>Comments</th>
        <th>Edit</th>
        {{-- @if(\Illuminate\Support\Facades\Auth::user()->temple_id=='admin') --}}
        <th>Delete</th>
        {{-- @endif --}}

      </tr>
      @foreach($search_data as $lead)
      <tr>
        <td>
          <center>
            <a target="_blank"
            href="https://web.whatsapp.com/send?phone=91{{$lead->mobile}}&text=">
            <i class="fa fa-whatsapp fa-lg" style="color: green" aria-hidden="true"></i>
          </a>
        </center>
      </td>

      <td class="text-center">
        <form method="post" action="{{route('markhiringlead')}}">
          {{csrf_field()}}
          <input type="hidden" value="{{$lead->id}}" name="lead_id">
          <div id="{{$lead->id}}_mark_div_input">
            <button type="submit" class="btn btn-sm btn-primary">Mark 
            </button>
          </div>
        </form>
      </td>

      <td class="text-center">
        <form method="post" action="{{route('rejecthiringlead')}}">
          {{csrf_field()}}
          <input type="hidden" value="{{$lead->id}}" name="lead_id">
          <button type="submit" class="btn btn-sm btn-danger"><span
            class="glyphicon glyphicon-remove"></span></button>
          </form>

        </td>
        <td id="name_{{$lead->id}}">{{$lead->name}}</td>
        <td id="mobile_{{$lead->id}}">{{$lead->mobile}}</td>
        <td id="enquiry_date_{{$lead->id}}">{{ \Carbon\Carbon::parse($lead->enquiry_date)->format('d/m/Y')}}</td>
        <td id="followup_call_on_{{$lead->id}}">
          @php
          if ($lead->followup_call_on=='')
            echo "";
          else
            echo \Carbon\Carbon::parse($lead->followup_call_on)->format('d/m/Y');
          @endphp
        </td>
        <td id="comments_{{$lead->id}}">
          @php
          $arr=explode(';',$lead->comments);
          foreach ($arr as $a){
            echo $a.'<br>';
          }
          @endphp
        </td>
        <td>
          <form method="post" action="{{route('edithiringlead')}}">
            <button type="button" onclick="control('{{$lead->assign_to}}','{{$lead->id}}')"
              data-toggle="modal" data-target="#myModal" class="btn btn-sm btn-info">Add Next
              Follow Up Date
            </button>
          </form>

        </td>

        {{-- @if(\Illuminate\Support\Facades\Auth::user()->temple_id=='admin') --}}
        <td>
          <form method="post" action="{{route('deletehiringlead')}}">
            {{csrf_field()}}
            <input type="hidden" value="{{$lead->id}}" name="lead_id">
            <button type="submit" class="btn btn-sm btn-danger"><span
              class="glyphicon glyphicon-trash"></span>
            </button>
          </form>
        </td>
        {{-- @endif --}}

      </tr>
      @endforeach
      <tr>
        <td colspan="14"><center>{{$search_data->appends(request()->query())->render()}}</center></td>
      </tr>
    </table>
    @endif

    @if(!empty($_GET['rejectedleads']))
    @php
    $searched = $_GET['rejectedleads'];

    if(strpos($searched, '/')){
      $searched = date("Y-m-d", strtotime($searched));
    }

    $rejected_search_data = \App\Hiring::whereRaw("(( is_done= 2 ) AND (name LIKE '%".$searched."%'  OR mobile LIKE '%".$searched."%'  OR enquiry_date = '".$searched."'  OR followup_call_on = '".$searched."'   OR comments LIKE '%".$searched."%'))")->orderBy('followup_call_on', 'asc')->paginate(10);
    @endphp
    <center><h2>Rejected Hiring Leads based on your searched query</h2></center>
    <table class="table table-condensed table-striped table-bordered table-responsive">
      <tr class="active">
        <th>Name</th>
        <th>Mobile</th>
        <th>Enquiry Date</th>
        <th>Follow-up Call On</th>
        <th>Comments</th>
        <th>Mark as Open</th>
        {{-- @if(\Illuminate\Support\Facades\Auth::user()->temple_id=='admin') --}}
        <th>Delete</th>
        {{-- @endif --}}
      </tr>
      @foreach($rejected_search_data as $lead)
      <tr>
        <td id="name_{{$lead->id}}">{{$lead->name}}</td>
        <td id="mobile_{{$lead->id}}">{{$lead->mobile}}</td>
        <td id="enquiry_date_{{$lead->id}}">{{ \Carbon\Carbon::parse($lead->enquiry_date)->format('d/m/Y')}}</td>
        <td id="followup_call_on_{{$lead->id}}">
          @php
          if ($lead->followup_call_on=='')
            echo "";
          else
            echo \Carbon\Carbon::parse($lead->followup_call_on)->format('d/m/Y');
          @endphp
        </td>
        <td id="comments_{{$lead->id}}">{{$lead->comments}}</td>
        <td class="text-center">
          <form method="post" action="{{route('rejectleadtoopen')}}">
            {{csrf_field()}}
            <input type="hidden" name="lead_id" value="{{$lead->id}}">
            <button type="submit" class="btn btn-sm btn-primary" value="{{$lead->id}}">Open</button>
          </form>
        </td>
        {{-- @if(\Illuminate\Support\Facades\Auth::user()->temple_id=='admin') --}}
        <td>
          <form method="post" action="{{route('deletehiringlead')}}">
            {{csrf_field()}}
            <input type="hidden" value="{{$lead->id}}" name="lead_id">
            <button type="submit" class="btn btn-sm btn-danger"><span
              class="glyphicon glyphicon-trash"></span>
            </button>
          </form>
        </td>
        {{-- @endif --}}
      </tr>
      @endforeach

    </table>
    <center>{{$rejected_search_data->appends(request()->query())->render()}}</center>
    @endif

    <div id="printableArea">
      <div class="row" style="margin-top: 50px;">
        <div class="text-center bg-info col-md-12">
          <h4><b>Open Hiring Leads</b>&nbsp;<span class="badge" style="background-color: #5cb85c;">{{$no_of_open_leads}}</span></h4>
        </div>
      </div>
      <table class="table table-condensed table-striped table-bordered table-responsive">
        <tr class="active">
          <th>Whats App</th>
          <th>Mark as Converted</th>
          <th>Mark as Rejected</th>
          <th>Name</th>
          <th>Mobile</th>
          <th>Enquiry Date</th>
          <th>Follow-up Call On</th>
          <th>Comments</th>
          <th>Edit</th>
          <th>Delete</th>
        </tr>
        @foreach($open_leads as $lead)
        <tr>
          <td>
            <center>
              <a target="_blank"
              href="https://web.whatsapp.com/send?phone=91{{$lead->mobile}}&text=">
              <i class="fa fa-whatsapp fa-lg" style="color: green" aria-hidden="true"></i>
            </a>
          </center>
        </td>

        <td class="text-center">
          <form method="post" action="{{route('markhiringlead')}}">
            {{csrf_field()}}
            <input type="hidden" value="{{$lead->id}}" name="lead_id">
            <div id="{{$lead->id}}_mark_div_input">
              <button type="submit" class="btn btn-sm btn-primary">Mark
              </button>
            </div>
          </form>
        </td>

        <td class="text-center">
          <form method="post" action="{{route('rejecthiringlead')}}">
            {{csrf_field()}}
            <input type="hidden" value="{{$lead->id}}" name="lead_id">
            <button type="submit" class="btn btn-sm btn-danger"><span
              class="glyphicon glyphicon-remove"></span></button>
            </form>

          </td>
          <td id="name_{{$lead->id}}">{{$lead->name}}</td>
          <td id="mobile_{{$lead->id}}">{{$lead->mobile}}</td>
          <td id="enquiry_date_{{$lead->id}}">{{ \Carbon\Carbon::parse($lead->enquiry_date)->format('d/m/Y')}}</td>
          <td id="followup_call_on_{{$lead->id}}">
            @php
            if ($lead->followup_call_on=='')
              echo "";
            else
              echo \Carbon\Carbon::parse($lead->followup_call_on)->format('d/m/Y');
            @endphp
          </td>
          <td id="comments_{{$lead->id}}">
            @php
            $arr=explode(';',$lead->comments);
            foreach ($arr as $a){
              echo $a.'<br>';
            }
            @endphp
          </td>
          <td>
            <button type="button" onclick="control('{{$lead->assign_to}}','{{$lead->id}}')"
              data-toggle="modal" data-target="#myModal" class="btn btn-sm btn-info">Add Next
              Follow Up Date
            </button>

          </td>
          <td>
            <form method="post" action="{{route('deletehiringlead')}}">
              {{csrf_field()}}
              <input type="hidden" value="{{$lead->id}}" name="lead_id">
              <button type="submit" class="btn btn-sm btn-danger"><span
                class="glyphicon glyphicon-trash"></span>
              </button>
            </form>

          </td>
          @endforeach
          <tr>
            <td colspan="14"><center>{{$open_leads->appends(request()->query())->render()}}</center></td>
          </tr>
        </table>

        <div class="row" style="margin-top: 50px;">
          <div class="text-center bg-info col-md-12 ">
            <h4><b>Converted Hiring Leads</b></h4>

          </div>
        </div>
        <table class="table table-condensed table-striped table-bordered table-responsive">
          <tr class="active">
            <th>Name</th>
            <th>Mobile</th>
            <th>Enquiry Date</th>
            <th>Follow-up Call On</th>
            <th>Comments</th>
            {{-- @if(\Illuminate\Support\Facades\Auth::user()->temple_id=='admin') --}}
            <th>Delete</th>
            {{-- @endif --}}

          </tr>
          @foreach($converted_leads as $lead)
          <tr>
            <td id="name_{{$lead->id}}">{{$lead->name}}</td>
            <td id="mobile_{{$lead->id}}">{{$lead->mobile}}</td>
            <td id="enquiry_date_{{$lead->id}}">{{ \Carbon\Carbon::parse($lead->enquiry_date)->format('d/m/Y')}}</td>
            <td id="followup_call_on_{{$lead->id}}">
              @php
              if ($lead->followup_call_on=='')
                echo "";
              else
                echo \Carbon\Carbon::parse($lead->followup_call_on)->format('d/m/Y');
              @endphp
            </td>
            <td id="comments_{{$lead->id}}">{{$lead->comments}}</td>
            {{-- @if(\Illuminate\Support\Facades\Auth::user()->temple_id=='admin') --}}
            <td>
              <form method="post" action="{{route('deletehiringlead')}}">
                {{csrf_field()}}
                <input type="hidden" value="{{$lead->id}}" name="lead_id">
                <button type="submit" class="btn btn-sm btn-danger"><span
                  class="glyphicon glyphicon-trash"></span></button>
                </form>

              </td>
              {{-- @endif --}}

            </tr>
            @endforeach

          </table>

          <div class="row" style="margin-top: 50px;">
            <div class="text-center bg-info col-md-12 ">
              <h4><b>Rejected Hiring Leads</b></h4>

            </div>
          </div>
          <table class="table table-condensed table-striped table-bordered table-responsive">
            <tr class="active">
              <th>Name</th>
              <th>Mobile</th>
              <th>Enquiry Date</th>
              <th>Follow-up Call On</th>
              <th>Comments</th>
              <th>Mark as Open</th>
              {{-- @if(\Illuminate\Support\Facades\Auth::user()->temple_id=='admin') --}}
              <th>Delete</th>
              {{-- @endif --}}

            </tr>
            @foreach($rejected_leads as $lead)
            <tr>
              <td id="name_{{$lead->id}}">{{$lead->name}}</td>
              <td id="mobile_{{$lead->id}}">{{$lead->mobile}}</td>
              <td id="enquiry_date_{{$lead->id}}">{{ \Carbon\Carbon::parse($lead->enquiry_date)->format('d/m/Y')}}</td>
              <td id="followup_call_on_{{$lead->id}}">
                @php
                if ($lead->followup_call_on=='')
                  echo "";
                else
                  echo \Carbon\Carbon::parse($lead->followup_call_on)->format('d/m/Y');
                @endphp
              </td>
              <td id="comments_{{$lead->id}}">{{$lead->comments}}</td>
              <td class="text-center">
                <form method="post" action="{{route('rejectleadtoopen')}}">
                  {{csrf_field()}}
                  <input type="hidden" name="lead_id" value="{{$lead->id}}">
                  <button type="submit" class="btn btn-sm btn-primary" value="{{$lead->id}}">Open</button>
                </form>
              </td>
              {{-- @if(\Illuminate\Support\Facades\Auth::user()->temple_id=='admin') --}}
              <td>
                <form method="post" action="{{route('deletehiringlead')}}">
                  {{csrf_field()}}
                  <input type="hidden" value="{{$lead->id}}" name="lead_id">
                  <button type="submit" class="btn btn-sm btn-danger"><span
                    class="glyphicon glyphicon-trash"></span>
                  </button>
                </form>

              </td>
              {{-- @endif --}}

            </tr>
            @endforeach

          </table>

        </div>

        <center>{{$rejected_leads->links()}}</center>

        <div id="myModal" class="modal fade" role="dialog">
          <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Follow Up Date Update</h4>
              </div>
              <div class="modal-body">
                <span id="assign_to" style="visibility: hidden">Some text in the modal.</span>
                <span id="edit_id" style="visibility: hidden">Some text in the modal.</span>
                <div class="row">
                  <div class="col-md-6 col-md-offset-3" id="modal_body">
                    <center>
                      <button class="btn btn-success" onclick="pickup()">Picked Up</button>
                      <button class="btn btn-primary" onclick="notpickup()">Not Picked Up</button>
                    </center>
                  </div>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div>

          </div>
        </div>


      </div>

      <style>
        body {
          overflow-x: hidden;
        }

        .items {
          font-size: 14px;
          font-weight: bold;
          padding-right: 30px !important;
          padding-left: 30px !important;
          padding-bottom: 30px !important;
          padding-top: 30px !important;
        }

        .sidebar-header {
          font-family: sans-serif;
          color: #fff !important;
          background-color: #000;
          padding-top: 10px;
          padding-bottom: 10px;
        }

        .navitems {
          margin-bottom: 5px;
          margin-top: 5px;
        }

        .navitems:hover {
          background-color: #000;
        }

        .navbar {
          height: -webkit-fill-available;
        }

        .navbar-static-top {
          height: 25px;
        }
      </style>

      <script>

        $.fn.serializeObject = function()
        {
          var o = {};
          var a = this.serializeArray();
          $.each(a, function() {
            if (o[this.name] !== undefined) {
              if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
              }
              o[this.name].push(this.value || '');
            } else {
              o[this.name] = this.value || '';
            }
          });
          return o;
        };

        function change(name, lead_id) {
          document.getElementById('reassign_lead_id').value = lead_id;
          if(name !== '')
            document.getElementById(name).selected = true;
          else
            document.getElementById('default').selected = true;

        }

        function control(assign_to, edit_id) {
          document.getElementById('assign_to').innerText = assign_to;
          document.getElementById('edit_id').innerText = edit_id;
        }

        function pickup() {
          var assign_to = document.getElementById('assign_to').innerHTML;
            // console.log(followed_up_by);
            var edit_id = document.getElementById('edit_id').innerHTML;
            document.getElementById('modal_body').innerHTML = '' +
            '<form action="/updatehiringlead" method="post">' +
            '{{csrf_field()}}' +
            '<label for="comments">Status Of Your Follow Up</label>' +
            '<textarea name="comments" id="comments" class="form-control"></textarea>' +
            '<br>' +
            '<input type="hidden" value="' + edit_id + '" name="lead_id">' +
            '<label for="rdates">Next FollowUp Date</label>' +
            '<input type="date" class="form-control" name="followup_call_on" id="rdates">' +
            '<br>' +
            '<input type="submit" value="save" class="btn btn-primary form-control">' +
            '</form>';
          }

          function notpickup() {
            var edit_id = document.getElementById('edit_id').innerHTML;
            document.getElementById('modal_body').innerHTML = '' +
            '<form action="/updatehiringlead" method="post">' +
            '{{csrf_field()}}' +
            '<input type="hidden" name="comments" value="Phone Not Picked Up" id="comments">' +
            '<br>' +
            '<input type="hidden" value="' + edit_id + '" name="lead_id">' +
            '<label for="rdates">Next FollowUp Date</label>' +
            '<input type="date" class="form-control" name="followup_call_on" id="rdates">' +
            '<br>' +
            '<input type="submit" value="save" class="btn btn-primary form-control">' +
            '</form>';
            var assign_to = document.getElementById('assign_to').innerHTML;
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function () {
              if (this.readyState == 4 && this.status == 200) {

              }
            };
            xhttp.open("GET", "/notpickupstatusset?edit_id=" + edit_id, true);
            xhttp.send();
          }
        </script>
        <script>
          function printDiv(divName) {
            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;
            document.body.innerHTML = printContents;
            window.print();
            document.body.innerHTML = originalContents;
          }
        </script>
        <script>
          function readCookie(name) {
            var nameEQ = name + "=";
            var ca = document.cookie.split(';');
            for(var i=0;i < ca.length;i++) {
              var c = ca[i];
              while (c.charAt(0)==' ') c = c.substring(1,c.length);
              if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
            }
            return null;
          }
        </script>

        @endsection


