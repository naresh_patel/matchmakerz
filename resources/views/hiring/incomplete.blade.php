@extends('layouts.app')
@section('content')
<div class="container">
  <div class="row" style="margin-top: 50px;">
    <div class="text-center bg-info col-md-12 ">
      <h4>Incomplete Leads</h4>
    </div>
  </div>
  {{-- @if(\Illuminate\Support\Facades\Auth::user()->role == 5 or \Illuminate\Support\Facades\Auth::user()->role == 9) --}}
  <div class="row" style="margin-top: 20px; margin-bottom:20px; padding:20px; width:80%; margin-left:auto;margin-right: auto; border:1px solid #eee; border-radius:20px;">
    <div class="text-centr col-md-12 ">
      <form method="GET" action="/incompletehiringleads">
        <div class="row">
          <div class="col-sm-6">
            <div class="input-group">
              <span class="input-group-addon" id="basic-addon1">Status</span>
              <select name="status" class="form-control" aria-describedby="basic-addon1">
                <option value="">Status</option>
                <option value="received"
                @if ($status === 'received')
                selected="selected"
                @endif>
              Received</option>
              <option value="missed"
              @if ($status === 'missed')
              selected="selected"
              @endif>
            Missed</option>
            <option value="blank"
            @if ($status === 'blank')
            selected="selected"
            @endif>
          Blank</option>
        </select>
      </div>
    </div>
  </div>
  <div class="row" style="padding:25px 0 10px; margin-left:2px;">
    <input type="submit" class="btn btn-success" value="Filter">
    <a href="{{url('/incompleteleads?status=&assign_to=&page=1')}}">
      <input type="button" class="btn btn-danger" style="margin-left:10px" value="Reset"/>
    </a>
  </div>
</form>
</div>
</div>
{{-- @endif --}}
<table class="table table-condensed table-bordered table-responsive">
  <tr class="active">
    <th>Sr. NO</th>
    <th>Phone</th>
    <th>Area</th>
    <th>Status</th>
    <th>Call Time</th>
    <th>Call Duration</th>
    <th>Channel</th>
    <th>Recording Link</th>
    <th>Edit</th>
    <th>Delete</th>
  </tr>
  @foreach($incomplete_leads as  $key => $lead)
  <tr>
    <td>
      {{($incomplete_leads->currentPage() * $incomplete_leads->perPage())-$incomplete_leads->perPage()+($key+1) }}
    </td>
    <td>{{$lead->user_phone}}</td>
    <td>{{$lead->area}}</td>
    <td class="{{$lead->status}}">{{ucfirst($lead->status)}}</td>
    <td>{{$lead->call_time}}</td>
    <td>{{$lead->call_duration}}</td>
    <td>{{$lead->channel}}</td>
    @if($lead->recording_link)
    <td align="center"><a href="https://developers.myoperator.co/recordings?&token={{$lead->token}}&file={{$lead->recording_link}}">File</a></td>
    @else
    <td align="center">N/A</td>
    @endif
    <td><button type="button" class="btn btn-info btn-sm" onclick="add({{$lead->user_phone}})">Complete Lead</button></td>
    <td>
      <form method="post" action="{{route('deleteIncompleteHiringLead')}}">
        {{csrf_field()}}
        <input type="hidden" value="{{$lead->parent_id}}" name="parent_id">
        <button type="submit" class="btn btn-sm btn-danger"><span
          class="glyphicon glyphicon-trash"></span>
        </button>
      </form>

    </td>
  </tr>
  @endforeach



</table>
<center>{{$incomplete_leads->appends($_GET)->links()}}</center>

</div>
<script>
  function add(mobile) {
    localStorage.setItem('mobile', mobile);
    window.location = '{{route('addhiringlead')}}';
  }
</script>
<style>
  .modal-dialog {
    min-height: calc(100vh - 60px);
    display: flex;
    flex-direction: column;
    justify-content: center;
    overflow: auto;
    @media(max-width: 768px) {
      min-height: calc(100vh - 20px);
    }
  }
  .modal {
    overflow-y:auto;
  }

  td.missed {
    background-color: crimson;
    color: white;
  }
  td.blank {
    background-color: black;
    color: white;
  }

  td.received {
    background-color: darkolivegreen;
    color: white;
  }
</style>
@endsection

