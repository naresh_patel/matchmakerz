@extends('layouts.app')

@section('content')
<div class="row">
	<div class="col-md-2">
		<div id="wrapper">
			<!-- Navigation -->
			<nav class="navbar navbar-inverse" role="navigation">
				<div class="sidebar-header">
					<center><h4><a href="#logout" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle" style="text-decoration: none; color: #fff;">Admin Dashboard&nbsp;&nbsp;&nbsp;<i class="fa fa-caret-down" aria-hidden="true"></i></a></h4>
						<ul class="collapse list-unstyled" id="logout">
							<li>
								<a href="#" style="color: #fff;"><i class="fa fa-sign-out" aria-hidden="true"></i>&nbsp;Logout</a>
							</li>
						</ul>
					</center>
				</div>
				<ul class="nav navbar-nav side-nav" style="overflow-x: hidden;">
					<li class="navitems col-sm-12">
						<a href="{{ route('overview') }}" class="items"><i class="fa fa-bar-chart"></i> Dashboard Overview</a>
					</li>
          <li class="navitems col-sm-12">
            <a href="{{ route('hiring') }}" class="items"><i class="fa fa-users"></i> Facebook Hiring Leads </a>
          </li>
					<li class="navitems col-sm-12">
						<a href="{{ route('matchmakerz') }}" class="items"><i class="fa fa-fw fa-paper-plane-o"></i> Matchmakers Onboarded </a>
					</li>
					<li class="navitems col-sm-12">
						<a href="{{ route('firstClient') }}" class="items"><i class="fa fa-fw fa-user-plus"></i> Free Client Added</a>
					</li>
					<li class="navitems col-sm-12 active">
						<a href="{{ route('clientConverted') }}" class="items"><i class="fa fa-exchange" aria-hidden="true"></i> Paid Client Converted    </a>
					</li>
				</ul>
			</nav>
		</div>
	</div>
	<div class="col-md-10" style="padding-right: 45px;">
		<div class="row">
			<div class="panel" style="margin-top: 15px;">
				<div class="panel-heading" style="background-color: #343a40; color: #fff">
					<div class="row">
						<div class="col-sm-12" style="font-size: 18px;"><b>Send Whatsapp Message</b></div>
					</div>
					<div class="row" style="margin-top: 20px;">
						<form action="{{ route('sendWhatsapp')}}" method="post">
							{{csrf_field()}}
							<div class="col-sm-4">
								<div class="form-group">
									<label for="phone_number">Phone Number: </label>
									<input type="text" name="phone_number" id="phone_number" class="form-control" minlength="10" maxlength="10" required>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="form-group">
									<label for="message">Message: </label>
									<textarea class="form-control" name="message" id="message" rows="3" required></textarea>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="form-group" style="margin-top: 25px;">
									<input type="submit" name="submit" value="Send" class="btn btn-success">
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="panel-group" id="accordion">
				<div class="panel">
					<div class="row change" style="margin-top: 15px; margin-left: 10px; margin-right: 10px;">
						<center>
							<div class="col-md-6 hover btn-link panel-heading" data-toggle="collapse" data-parent="#accordion" data-target="#verified"><b>Payment Verified</b></div>
						</center>
					</div>
					<div class="panel-collapse collapse in" id="verified">
						<div class="panel-body">
							<div class="row">
								<div class="col-sm-12">
									<table class="table table-striped table-bordered table-hover">
										<caption style="font-size: 20px;"><strong>List of Clients Whose Payment is Verified</strong></caption>
										<thead style="background-color: #343a40; color: #fff">
											<tr>
												<th>Name</th>
												<th>Gender</th>
												<th>Client Ph. No.</th>
												<th>Weight (Kg)</th>
												<th>Height</th>
												<th>Income (In Lacs)</th>
												<th>Payment</th>
												<th>Payment Status</th>
												<th>Paid On</th>
												<th>Matchmaker</th>
												<th>Matchmaker Ph. No.</th>
											</tr>
										</thead>
										<tbody>
											@foreach ($verified_payment_clients as $client)
											<tr>
												<td><b>{{ $client->name }}</b></td>
												<td>
													@if($client->gender == 0)
													Male
													@else
													Female
													@endif
												</td>
												<td>{{$client->phone_number}}</td>
												<td>{{$client->weight}}</td>
												<td>{{floor($client->height/12)}}'{{$client->height%12}}"</td>
												<td><i class="fa fa-inr" aria-hidden="true"></i>&nbsp;{{$client->yearly_income}}</td>
												<td><i class="fa fa-inr" aria-hidden="true"></i>&nbsp;{{$client->upfront_charge}}</td>
												<td>
													@if($client->payment_status == 0)
													<span class="badge danger">Not Verified</span>
													@else
													<span class="badge success">Verified</span>
													@endif
												</td>
												<td>{{date("Y-m-d h:i A", strtotime($client->paid_on))}}</td>
												<td>
													@php
													$client_matchmaker = App\MatchmakerUser::where('id', $client->matchmaker_id)->first();
													@endphp
													<b>{{$client_matchmaker->first_name}}&nbsp;&nbsp;{{$client_matchmaker->last_name}}</b>
												</td>
												<td>{{$client_matchmaker->phone_number}}</td>
											</tr>
											@endforeach
											<tr>
												<td colspan="14"><center>{{$verified_payment_clients->appends(request()->query())->render()}}</center></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<style>
	body {
		overflow-x: hidden;
	}
	.success {
		background-color: #52b053;
	}

	.danger {
		background-color: #e8130e;
	}

	.items {
		font-size: 14px;
		font-weight: bold;
		padding-right: 30px !important;
		padding-left: 30px !important;
		padding-bottom: 30px !important;
		padding-top: 30px !important;
	}

	.sidebar-header {
		font-family: sans-serif;
		color: #fff;
		background-color: #000;
		padding-top: 10px;
		padding-bottom: 10px;
	}

	.navitems {
		margin-bottom: 5px;
		margin-top: 5px;
	}

	.navitems:hover {
		background-color: #000;
	}
	.navbar {
		height: -webkit-fill-available;
	}
	li {
		width: 250px;
	}
	.navbar-static-top {
		height: 25px;
	}

	.panel-heading {
		background-color: #ddd;
	}

	.hover:hover {
		cursor: pointer;
		background-color: #ededed;
	}

	.hover {
		font-size: 18px;
		font-weight: bolder;
		border-right: 1px #ddd solid;
	}
</style>
<script>
	$(document).ready(function() {
		document.getElementsByClassName('hover')[0].style.background = "#343a40";
		document.getElementsByClassName('hover')[0].style.color = "#fff";
	})
	function change(no) {
		if(no == 0) {
			$('.hover').eq(0).css("background-color", "#343a40", "color", "#fff");
			$('.hover').eq(0).css("color", "#fff");
			$('.hover').eq(1).css("background-color", "#ddd", "color", "#337ab7");
			$('.hover').eq(1).css("color", "#337ab7");
		}
		else if (no == 1) {
			$('.hover').eq(1).css("background-color", "#343a40", "color", "#fff");
			$('.hover').eq(1).css("color", "#fff");
			$('.hover').eq(0).css("background-color", "#ddd", "color", "#337ab7");
			$('.hover').eq(0).css("color", "#337ab7");
		}
	}
</script>
@endsection
