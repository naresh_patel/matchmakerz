@extends('layouts.app')

@section('content')
<div class="row">
	<div class="col-md-2">
		<div id="wrapper">
			<!-- Navigation -->
			<nav class="navbar navbar-inverse" role="navigation">
				<div class="sidebar-header">
					<center><h4><a href="#logout" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle" style="text-decoration: none; color: #fff;">Admin Dashboard&nbsp;&nbsp;&nbsp;<i class="fa fa-caret-down" aria-hidden="true"></i></a></h4>
						<ul class="collapse list-unstyled" id="logout">
							<li>
								<a href="#" style="color: #fff;"><i class="fa fa-sign-out" aria-hidden="true"></i>&nbsp;Logout</a>
							</li>
						</ul>
					</center>
				</div>
				<ul class="nav navbar-nav side-nav" style="overflow-x: hidden;">
					<li class="navitems col-sm-12">
						<a href="{{ route('overview') }}" class="items"><i class="fa fa-bar-chart"></i> Dashboard Overview</a>
					</li>
          <li class="navitems col-sm-12">
            <a href="{{ route('hiring') }}" class="items"><i class="fa fa-users"></i> Facebook Hiring Leads </a>
          </li>
					<li class="navitems col-sm-12">
						<a href="{{ route('matchmakerz') }}" class="items"><i class="fa fa-fw fa-paper-plane-o"></i> Matchmakers Onboarded </a>
					</li>
					<li class="navitems active col-sm-12">
						<a href="{{ route('firstClient') }}" class="items"><i class="fa fa-fw fa-user-plus"></i> Free Client Added</a>
					</li>
					<li class="navitems col-sm-12">
						<a href="{{ route('clientConverted') }}" class="items"><i class="fa fa-exchange" aria-hidden="true"></i> Paid Client Converted    </a>
					</li>
				</ul>
			</nav>
		</div>
	</div>
	<div class="col-md-10">
		@if (Session::has('message'))
		<div class="row" style="margin-top: 10px; margin-right: 10px;">
			<div class="alert alert-success alert-dismissible" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><i class="fa fa-check" aria-hidden="true"></i>&nbsp;<b>{!! Session::get('message') !!}</b>
			</div>
		</div>
		@endif
		<div class="row" style="padding-right: 33px;">
			<div class="panel" style="margin-top: 15px;">
				<div class="panel-heading" style="background-color: #343a40; color: #fff">
					<div class="row">
						<div class="col-sm-12" style="font-size: 18px;"><b>Send Whatsapp Message</b></div>
					</div>
					<div class="row" style="margin-top: 20px;">
						<form action="{{ route('sendWhatsapp')}}" method="post">
							{{csrf_field()}}
							<div class="col-sm-4">
								<div class="form-group">
									<label for="phone_number">Phone Number: </label>
									<input type="text" name="phone_number" id="phone_number" class="form-control" minlength="10" maxlength="10" required>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="form-group">
									<label for="message">Message: </label>
									<textarea class="form-control" name="message" id="message" rows="3" required></textarea>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="form-group" style="margin-top: 25px;">
									<input type="submit" name="submit" value="Send" class="btn btn-success">
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<div class="row" id="onboarded" style="margin-top: 15px; margin-right: 15px;">
			<span style="font-size: 20px;"><strong>Matchmakers having at least One Free Client <span class="badge">{{$no_of_matchmakers}}</span></strong></span>
			<span style="font-size: 20px; float: right;"><strong> <span class="badge">{{$no_of_free_clients}}</span> Total No. of Free Clients</strong></span>
			<div class="panel-group" id="accordion">
				<div class="panel">
					<div class="panel-heading" style="background-color: #343a40; color: #fff">
						<div class="row">
							<form action="{{route('firstClient')}}" id="form1">
								<div class="col-md-2"><b>Name / Level</b>
									<input type="text" name="name" <?php if(isset($name)) echo 'value="'.$name.'"' ?> class="form-control input" onchange="$('#form1').submit();">
								</div>
								<div class="col-md-1"><b>Gender</b></div>
								<div class="col-md-1"><b>Age</b></div>
								<div class="col-md-1"><b>Phone Number</b>
									<input type="text" name="phone_number" <?php if(isset($phone_number)) echo 'value="'.$phone_number.'"' ?> class="form-control input" onchange="$('#form1').submit();">
								</div>
								<div class="col-md-3"><b>Follow-Up Date / Comments</b></div>
								<div class="col-md-2"><b>Assigned To</b>
									<input type="text" name="assigned_to" <?php if(isset($assigned_to)) echo 'value="'.$assigned_to.'"' ?> class="form-control input" onchange="$('#form1').submit();">
								</div>
								<div class="col-md-1"><b>Free Clients</b></div>
								<div class="col-md-1"><b>Wallet Amount</b></div>
							</form>
						</div>
					</div>
					@foreach($matchmakers as $matchmaker)
					<div class="panel">
						<div class="panel-heading">

							<div class="row">
								@php
								$paid_clients = App\ClientProfile::where('matchmaker_id', $matchmaker->id)->where('payment_status', 1)
								->where('amount_fix', 1)->count();
								@endphp
								<div class="col-md-2">
									<b>{{ $matchmaker->first_name }} {{$matchmaker->last_name}},
										@if($paid_clients > 2) {{2}}
										@elseif($matchmaker->no_of_clients > 2) {{1}}
										@else {{0}}
										@endif
									</b>
								</div>
								<div class="col-md-1">
									@if($matchmaker->gender == 0)
									Male
									@else
									Female
									@endif
								</div>
								<div class="col-md-1">{{$matchmaker->age}}</div>
								<div class="col-md-1">{{$matchmaker->phone_number}}</div>
								
								<div class="col-md-3" id="followup_comments{{$matchmaker->id}}">
									<b>Follow-Up Date : {{$matchmaker->followup_date}}</b><br>
									@php
									$comments = explode(';', $matchmaker->comments);
									for ($i=0; $i<sizeof($comments); $i++) {
										if($i == sizeof($comments)-1)
											echo $comments[$i];
										else
											echo $comments[$i]."<br>";
									}
									@endphp
									<button type="button" class="btn btn-sm btn-link" data-toggle="modal" data-target="#update" onclick="update({{$matchmaker}})">Update</button>
								</div>
								<div class="col-md-2">
									<form method="post" action="{{route('assignTo')}}">
										<div class="form-group">
											{{csrf_field()}}
											<input type="hidden" name="matchmaker_id" value="{{$matchmaker->id}}">
											<input type="text" name="assign_to" placeholder="Assign To" value="{{$matchmaker->assigned_to}}" class="form-control" required>
											<button class="btn-success btn-xs" style="margin-top: 5px; float: right;"><i class="fa fa-check"></i></button>
										</div>
									</form>
								</div>
								<div class="col-md-1">
									<a data-toggle="collapse" data-parent="#accordion" href="#client_{{$matchmaker->id}}" title="Click to see Clients">{{$matchmaker->no_of_clients}} Free Clients</a>
								</div>
								<div class="col-md-1">
									{{$matchmaker->wallet}}
								</div>
							</div>
						</div><hr>
					</div>
					<div id="client_{{$matchmaker->id}}" class="panel-collapse collapse">
						<div class="panel-body">
							<div class="row">
								<div class="col-md-offset-1 col-md-10"> 
									@php
									$clients = App\ClientProfile::where('matchmaker_id', $matchmaker->id)->where('amount_fix', 0)->get();
									@endphp
									<caption style="font-size: 20px;"><strong><span class="badge">{{$matchmaker->no_of_clients}}</span> Free Clients of {{ $matchmaker->first_name }} {{$matchmaker->last_name}}</strong>
									</caption>
									<table class="table table-striped table-bordered table-hover">
										<thead style="background-color: #343a40; color: #fff">
											<tr>
												<th>Name</th>
												<th>Gender</th>
												<th>Age (Yrs)</th>
												<th>Client Ph. No.</th>
												<th>Weight (Kg)</th>
												<th>Height</th>
												<th>Income (In Lacs)</th>
												<th>Paid</th>
												<th>Amount</th>
												<th>Payment Status</th>
												<th>WhatsApp Assistance</th>
												<th>Shared Profiles</th>
											</tr>
										</thead>
										<tbody>
											@foreach ($clients as $client)
											@php
											$client_payment = App\ClientPayment::where('client_id', $client->id)->where('payment_status', 0)->first();
											$interested=[];
											$i=0;$r=0;$a=0;
											$rejected=[];
											$awaiting=[];
											$compatibility = App\Compatibility::where('client_id', $client->id)->first();
											if($compatibility) {
												$profiles_sent = json_decode($compatibility->profile_status);
												if(is_array($profiles_sent) || is_object($profiles_sent))
												foreach ($profiles_sent as $data) {
													$profile = App\ClientProfile::where('id', $data->client_id)->first();
													if($data->status == 'P'){
													array_push($awaiting, $profile);
													$a++;
													}
													if($data->status == 'R'){
														array_push($rejected,$profile);
														$r++;
													}
													if($data->status=='C'){
														array_push($interested,$profile);
														$i++;
													}
												}
	
											}
											@endphp
											<tr>
												<td><b>{{ $client->name }}</b></td>
												<td>
													@if($client->gender == 0)
													Male
													@else
													Female
													@endif
												</td>
												@php
												$age = date("Y", time()) - date("Y", strtotime($client->birth_date));
												@endphp
												<td>{{$age}}</td>
												<td>{{$client->phone_number}}</td>
												<td>{{$client->weight}}</td>
												<td>{{floor($client->height/12)}}'{{$client->height%12}}"</td>
												<td><i class="fa fa-inr" aria-hidden="true"></i>&nbsp;{{$client->yearly_income}}</td>
												@if($client_payment)
												<td>{{date("Y-m-d h:i A", strtotime($client_payment->paid_on))}}</td>
												<td><i class="fa fa-inr" aria-hidden="true"></i>&nbsp;{{$client_payment->upfront_charge}}</td>
												<td>
													@if($client_payment->payment_status == 0)
													<span class="badge danger">Not Verified</span>
													@else
													<span class="badge success">Verified</span>
													@endif
												</td>
												@else
												<td><span class="badge" style="background-color: #337ab7">Not Yet</span></td>
												<td> -- </td>
												<td> -- </td>
												<td>Interested: {{$i}}<br>Rejected: {{$r}} <br> Awaiting: {{$a}}</td>
												<td><form action="{{route('shared')}}" method="get">
													{{csrf_field()}}
													<input type="hidden" value="{{$client->id}}" name="client_id">
													<input type="hidden" value="{{$matchmaker->id}}" name="matchmaker_id">
													<button type="submit">View</button>
												</form>
												</td>
												@endif
											</tr>
											@endforeach
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
					@endforeach
				</div>
			</div>
			<tr>
				<td colspan="14"><center>{{$matchmakers->appends(request()->query())->render()}}</center></td>
			</tr>
		</div>
	</div>
</div>

<div id="update" class="modal fade" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Follow Up Date Update</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-offset-1 col-md-10" id="modal_body">
						<label id="matchmaker_name" style="font-size: 18px;"></label>
						<center>
							<button class="btn btn-primary" onclick="notpickup()">Not Picked Up</button>
						</center>
						<form action="{{route('updateFollowUp')}}">
							{{csrf_field()}}
							<input type="hidden" name="matchmaker_id" value="">
							<input type="hidden" name="type" value="firstClient">
							<div class="form-group">
								<label for="comment">Comment :</label>
								<textarea id="comment" name="comment" class="form-control" rows="3" required=""></textarea>
							</div>
							<div class="form-group">
								<label for="followup_date">Follow-Up Date :</label>
								<input type="date" id="followup_date" name="followup_date" min="{{date("Y-m-d", time())}}" class="form-control" required="">
							</div>
							<div class="form-group">
								<center><input type="submit" name="submit" class="btn btn-success" value="Update"></center>
							</div>
						</form>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<style>
	body {
		overflow-x: hidden;
	}
	.success {
		background-color: #52b053;
	}

	.danger {
		background-color: #e8130e;
	}

	.items {
		font-size: 14px;
		font-weight: bold;
		padding-right: 30px !important;
		padding-left: 30px !important;
		padding-bottom: 30px !important;
		padding-top: 30px !important;
	}

	.sidebar-header {
		font-family: sans-serif;
		color: #fff;
		background-color: #000;
		padding-top: 10px;
		padding-bottom: 10px;
	}

	.navitems {
		margin-bottom: 5px;
		margin-top: 5px;
	}

	.navitems:hover {
		background-color: #000;
	}

	.navbar {
		height: -webkit-fill-available;
	}

	.navbar-static-top {
		height: 25px;
	}

	.border {
		border: 1px solid #ddd;
	}
	
	.input {
		width: 110px;
		height: 32px;
		font-size: 12px;
		font-weight: bold;
		margin-top: 5px;"
	}
</style>
<script>
	function update(matchmaker) {
		document.getElementById('matchmaker_name').innerHTML = "Matchmaker: "+ matchmaker.first_name + ' ' + matchmaker.last_name;
		$('input[name="matchmaker_id"]').attr('value', matchmaker.id);
	}

	function notpickup() {
		$('#comment').html('Phone Not Picked Up');
	}
</script>
@endsection
