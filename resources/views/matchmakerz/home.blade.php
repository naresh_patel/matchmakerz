@extends('layouts.app')

@section('content')
<div class="row">
	<div class="col-md-2">
		<div id="wrapper">
			<!-- Navigation -->
			<nav class="navbar navbar-inverse" role="navigation">
				<div class="sidebar-header">
					<center><h4><a href="#logout" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle" style="text-decoration: none; color: #fff;">Admin Dashboard&nbsp;&nbsp;&nbsp;<i class="fa fa-caret-down" aria-hidden="true"></i></a></h4>
						<ul class="collapse list-unstyled" id="logout">
							<li>
								<a href="#" style="color: #fff;"><i class="fa fa-sign-out" aria-hidden="true"></i>&nbsp;Logout</a>
							</li>
						</ul>
					</center>
				</div>
				<ul class="nav navbar-nav side-nav" style="overflow-x: hidden;">
					<li class="navitems col-sm-12">
						<a href="{{ route('overview') }}" class="items"><i class="fa fa-bar-chart"></i> Dashboard Overview</a>
					</li>
          <li class="navitems col-sm-12">
            <a href="{{ route('hiring') }}" class="items"><i class="fa fa-users"></i> Facebook Hiring Leads </a>
          </li>
					<li class="navitems col-sm-12 active">
						<a href="{{ route('matchmakerz') }}" class="items"><i class="fa fa-fw fa-paper-plane-o"></i> Matchmakers Onboarded </a>
					</li>
					<li class="navitems col-sm-12">
						<a href="{{ route('withdrawl') }}" class="items"><i class="fa fa-fw fa-user-plus"></i> Withdrawl Requests</a>
					</li>
					<li class="navitems col-sm-12">
						<a href="{{ route('firstClient') }}" class="items"><i class="fa fa-fw fa-user-plus"></i> Free Client Added</a>
					</li>
					<li class="navitems col-sm-12">
						<a href="{{ route('clientConverted') }}" class="items"><i class="fa fa-exchange" aria-hidden="true"></i> Paid Client Converted    </a>
					</li>
				</ul>
			</nav>
		</div>
	</div>
	<div class="col-md-10" style="padding-right: 45px;">
		@if (Session::has('message'))
		<div class="row" style="margin-top: 10px;">
			<div class="alert alert-success alert-dismissible" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><i class="fa fa-check" aria-hidden="true"></i>&nbsp;<b>{!! Session::get('message') !!}</b>
			</div>
		</div>
		@endif
		<div class="row">
			<div class="panel" style="margin-top: 15px;">
				<div class="panel-heading" style="background-color: #343a40; color: #fff">
					<div class="row">
						<div class="col-sm-12" style="font-size: 18px;"><b>Send Whatsapp Message</b></div>
					</div>
					<div class="row" style="margin-top: 20px;">
						<form action="{{ route('sendWhatsapp')}}" method="post">
							{{csrf_field()}}
							<div class="col-sm-4">
								<div class="form-group">
									<label for="phone_number">Phone Number: </label>
									<input type="text" name="phone_number" id="phone_number" class="form-control" minlength="10" maxlength="10" required>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="form-group">
									<label for="message">Message: </label>
									<textarea class="form-control" name="message" id="message" rows="3" required></textarea>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="form-group" style="margin-top: 25px;">
									<input type="submit" name="submit" value="Send" class="btn btn-success">
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<div class="row" id="onboarded">
			<table class="table table-striped table-bordered table-hover">
				<caption style="font-size: 20px;"><strong>Matchmakers Onboarded&nbsp;</strong><span class="badge">{{$no_of_matchmakers_onboarded}}</span></caption>
				<thead style="background-color: #343a40; color: #fff">
					<tr>
						<form action="{{route('matchmakerz')}}" id="form1">
							<th>Name<br>
								<input type="text" name="name" <?php if(isset($name)) echo 'value="'.$name.'"' ?> class="form-control input" onchange="$('#form1').submit();">
							</th>
							<th>Gender</th>
							<th>Age (Yrs)</th>
							<th>Phone Number<br>
								<input type="text" name="phone_number" <?php if(isset($phone_number)) echo 'value="'.$phone_number.'"' ?> class="form-control input" onchange="$('#form1').submit();">
							</th>
							<th>Is Approved ?<br>
								<select name="is_approved" class="form-control" style="width: 110px; height: 32px; font-size: 12px; margin-top: 5px;" onchange="$('#form1').submit();">
									<option value="1" <?php if($is_approved == 1) echo "selected"; ?>>Approved</option>
									<option value="0" <?php if($is_approved == 0) echo "selected"; ?>>Not Approved</option>
								</select>
							</th>
							<th>Is Active ?<br>
								<select name="working_status" class="form-control" style="width: 110px; height: 32px; font-size: 12px; margin-top: 5px;" onchange="$('#form1').submit();">
									<option value="1" <?php if($working_status == 1) echo "selected"; ?>>Active</option>
									<option value="0" <?php if($working_status == 0) echo "selected"; ?>>Not Active</option>
								</select>
							</th>
							<th>Withdrawl amount</th>
							<th>Is Staff ?<br>
								<select name="is_staff" class="form-control" style="width: 90px; height: 32px; font-size: 12px; margin-top: 5px;" onchange="$('#form1').submit();">
									<option value="1" <?php if($is_staff == 1) echo "selected"; ?>>Staff</option>
									<option value="0" <?php if($is_staff == 0) echo "selected"; ?>>Not a Staff</option>
								</select>
							</th>
						</form>
						<th>Follow-Up Date / Comments</th>
					</tr>
				</thead>
				<tbody>
					@foreach ($matchmakers as $matchmaker)
					<tr>
						<td><b>{{ $matchmaker->first_name }} {{$matchmaker->last_name}}</b></td>
						<td>
							@if($matchmaker->gender == 0)
							Male
							@else
							Female
							@endif
						</td>
						<td>{{$matchmaker->age}}</td>
						<td id="phone_{{$matchmaker->id}}">{{$matchmaker->phone_number}}</td>
						<td>
							@if($matchmaker->is_active == 1)
							<span class="badge success">Approved</span>
							@else
							<span class="badge danger">Not Approved</span>
							@endif
						</td>
						<td>
							@if($matchmaker->working_status == 1)
							<button class="badge success" onclick="updateActive({{$matchmaker->id,1}})">Active</button>
							@else
							<button class="badge danger" onclick="updateActive({{$matchmaker->id,0}})">Not Active</button>
							@endif
						</td>

						<td>
						 {{$matchmaker->wallet}}
						</td>
						<td>
							@if($matchmaker->is_staff == 1)
							<span class="badge success">Staff</span>
							@else
							<span class="badge danger">Not a staff</span>
							@endif
						</td>
						<td>
							@php
							if($matchmaker->followup_date)
								echo "<b>Follow-Up Date : ".$matchmaker->followup_date."</b><br>";
							elseif ($matchmaker->created_at)
								echo "<b>Follow-Up Date : ".date("Y-m-d", strtotime($matchmaker->created_at))."</b><br>";
							else
								echo "<b>Follow-Up Date : </b><br>";
							
							$comments = explode(';', $matchmaker->comments);
							for ($i=0; $i<sizeof($comments); $i++) {
								if($i == sizeof($comments)-1)
									echo $comments[$i];
								else
									echo $comments[$i]."<br>";
							}
							@endphp
							<button type="button" class="btn btn-sm btn-link" data-toggle="modal" data-target="#update" onclick="update({{$matchmaker}})">Update</button>
						</td>
					</tr>
					@endforeach
					<tr>
						<td colspan="14"><center>{{$matchmakers->appends(request()->query())->render()}}</center></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>

<div id="update" class="modal fade" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Follow Up Date Update</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-offset-1 col-md-10" id="modal_body">
						<label id="matchmaker_name" style="font-size: 18px;"></label>
						<center>
							<button class="btn btn-primary" onclick="notpickup()">Not Picked Up</button>
						</center>
						<form action="{{route('updateFollowUp')}}" method="get">
							{{csrf_field()}}
							<input type="hidden" name="matchmaker_id" value="">
							<input type="hidden" name="type" value="onboarded">
							<div class="form-group">
								<label for="comment">Comment :</label>
								<textarea id="comment" name="comment" class="form-control" rows="3" required=""></textarea>
							</div>
							<div class="form-group">
								<label for="followup_date">Follow-Up Date :</label>
								<input type="date" id="followup_date" name="followup_date" min="{{date("Y-m-d", time())}}" class="form-control" required="">
							</div>
							<div class="form-group">
								<center><input type="submit" name="submit" class="btn btn-success" value="Update"></center>
							</div>
						</form>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<style>
	body {
		overflow-x: hidden;
	}
	.success {
		background-color: #52b053;
	}

	.danger {
		background-color: #e8130e;
	}

	.items {
		font-size: 14px;
		font-weight: bold;
		padding-right: 30px !important;
		padding-left: 30px !important;
		padding-bottom: 30px !important;
		padding-top: 30px !important;
	}

	.sidebar-header {
		font-family: sans-serif;
		color: #fff !important;
		background-color: #000;
		padding-top: 10px;
		padding-bottom: 10px;
	}

	.navitems {
		margin-bottom: 5px;
		margin-top: 5px;
	}

	.navitems:hover {
		background-color: #000;
	}

	.navbar {
		height: -webkit-fill-available;
	}

	.navbar-static-top {
		height: 25px;
	}

	.input {
		width: 110px;
		height: 32px;
		font-size: 12px;
		font-weight: bold;
		margin-top: 5px;"
	}
</style>
<script>
	function update(matchmaker) {
		document.getElementById('matchmaker_name').innerHTML = "Matchmaker: "+ matchmaker.first_name + ' ' + matchmaker.last_name;
		$('input[name="matchmaker_id"]').attr('value', matchmaker.id);
	}

	function notpickup() {
		$('#comment').html('Phone Not Picked Up');
	}
	function updateActive(id,working_status){
    	 	var id=id;
		var working_status=working_status;
		console.log(id,working_status);
		      $.ajax({
        type: "GET",
        url: "/updateActive",
        data: {
	     id:id,
	     working_status:working_status
	 },
        success: function(msg){
          alert( "Data Saved: " + msg );
	  window.location.reload();
        }
      });
	}
</script>
@endsection
