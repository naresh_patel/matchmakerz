<div id="wrapper">
	<!-- Navigation -->
	<nav class="navbar navbar-inverse" role="navigation">
		<ul class="nav navbar-nav side-nav" style="overflow-x: hidden;">
			<li class="navitems active">
				<a href="#onboarded" class="items"><i class="fa fa-fw fa-paper-plane-o"></i> Matchmakers Onboarded </a>
			</li>
			<li class="navitems">
				<a href="#firstclient" class="items"><i class="fa fa-fw fa-user-plus"></i> First Client Added</a>
			</li>
			<li class="navitems">
				<a href="#converted" class="items"><i class="fa fa-exchange" aria-hidden="true"></i> Paid Client Converted    </a>
			</li>
			<li class="navitems">
				<a href="#services" class="items"><i class="fa fa-fw fa-star"></i> Services Going on</a>
			</li>
		</ul>
	</nav>
</div>