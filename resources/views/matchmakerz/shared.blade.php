@extends('layouts.app')

@section('content')

  <div class="container">
    @foreach($shared_profiles as $personal)
     <div class="panel panel-default" style="width: 80%; margin-left:  10%; ">
          <div class="panel-heading">
            <h4>Identity No  {{$personal['id']}}</h4>
          </div>
          <div class="panel-body">

            <h4>Personal Details</h4>
            <hr>
            <div class="row">
              <div class="col-md-3  active"><b>Profile Picture</b></div>
              <div class="col-md-3"><img class="img-rounded" src={{$personal['profile_photo']}} width="50px" height="50px">
              </div>
              <div class="col-md-3  active"><b>Name</b></div>
              <div class="col-md-3"> {{$personal['name']}} 
                  <span style="color:green">{{$personal['phone_number']}}</span>
                </div>
              </div>
              <div class="row">
                <div class="col-md-3  active"><b>Gender</b></div>
                    <div class="col-md-3">{{$personal['gender']}}</div>
                <div class="col-md-3  active"><b>DOB</b></div>
                <div class="col-md-3"> {{$personal['birth_date']}} </div>
              </div>
              <div class="row">
                <div class="col-md-3  active"><b>Income</b></div>
                <div class="col-md-3">₹ {{$personal['yearly_income']}} L</div>

                <div class="col-md-3  active"><b>Occupation</b></div>
                <div class="col-md-3">{{$personal['occupation']}} </div>
              </div>
              <hr>
              <br>

              </div>
	  </div>

            @endforeach
        </div>

        <style>
        .row {
          margin-top: 10px;
        }
        form {
          margin-top: 7px;
        }
        </style>
      @endsection


