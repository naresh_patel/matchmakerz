@extends('layouts.app')

@section('content')
<div class="row">
	<div class="col-md-2">
		<div id="wrapper">
			<!-- Navigation -->
			<nav class="navbar navbar-inverse" role="navigation">
				<div class="sidebar-header">
					<center><h4><a href="#logout" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle" style="text-decoration: none; color: #fff;">Admin Dashboard&nbsp;&nbsp;&nbsp;<i class="fa fa-caret-down" aria-hidden="true"></i></a></h4>
						<ul class="collapse list-unstyled" id="logout">
							<li>
								<a href="#" style="color: #fff;"><i class="fa fa-sign-out" aria-hidden="true"></i>&nbsp;Logout</a>
							</li>
						</ul>
					</center>
				</div>
				<ul class="nav navbar-nav side-nav" style="overflow-x: hidden;">
					<li class="navitems col-sm-12 active">
						<a href="{{ route('overview') }}" class="items"><i class="fa fa-bar-chart"></i> Dashboard Overview</a>
					</li>
          <li class="navitems col-sm-12">
            <a href="{{ route('hiring') }}" class="items"><i class="fa fa-users"></i> Facebook Hiring Leads </a>
          </li>
					<li class="navitems col-sm-12">
						<a href="{{ route('matchmakerz') }}" class="items"><i class="fa fa-fw fa-paper-plane-o"></i> Matchmakers Onboarded </a>
					</li>
					<li class="navitems col-sm-12">
						<a href="{{ route('firstClient') }}" class="items"><i class="fa fa-fw fa-user-plus"></i> Free Client Added</a>
					</li>
					<li class="navitems col-sm-12">
						<a href="{{ route('clientConverted') }}" class="items"><i class="fa fa-exchange" aria-hidden="true"></i> Paid Client Converted    </a>
					</li>
				</ul>
			</nav>
		</div>
	</div>
	<div class="col-md-10" style="padding-right: 45px;">
		@if (Session::has('message'))
		<div class="row" style="margin-top: 10px;">
			<div class="alert alert-danger alert-dismissible" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><i class="fa fa-cross" aria-hidden="true"></i>&nbsp;<b>{!! Session::get('message') !!}</b>
			</div>
		</div>
		@endif


		<div class="row">
			<div class="panel" style="margin-top: 15px;">
				<div class="panel-heading" style="background-color: #343a40; color: #fff">
					<div class="row">
						<div class="col-sm-12" style="font-size: 18px;"><b>Change Hashcode</b></div>
					</div>
					<div class="row" style="margin-top: 20px;">
						<form action="{{ route('hashcode')}}" method="post">
							{{csrf_field()}}
							<div class="col-sm-4">
								<div class="form-group">
									<label for="phone_number">Hashcode: </label>
									<input type="text" name="hash" id="hash" class="form-control" required>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="form-group" style="margin-top: 25px;">
									<input type="submit" name="submit" value="Send" class="btn btn-success">
								</div>
							</div>
						</form>

						        <form action="{{route('imageupload')}}" method="POST" enctype="multipart/form-data">

            @csrf

            <div class="row">

  

                <div class="col-md-6">

                    <input type="file" name="image" class="form-control">

                </div>

   

                <div class="col-md-6">

                    <button type="submit" class="btn btn-success">Upload</button>

                </div>

   

            </div>

        </form>
					</div>
				</div>
			</div>
		</div>


		<div class="row">
			<div class="panel" style="margin-top: 15px;">
				<div class="panel-heading" style="background-color: #343a40; color: #fff">
					<div class="row">
						<div class="col-sm-12" style="font-size: 18px; font-style: italic;"><b>Statistics</b></div>
					</div>
					<div class="row" style="margin-top: 20px;">
						<form class="form-inline">
							<div class="form-group">
								<div class="col-sm-4">
									<label for="from">From: </label>
									<input type="date" id="from" name="from" value="{{date("Y-m-d", strtotime($start))}}" max="{{date("Y-m-d", time()+19800)}}" class="form-control">
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-4">
									<label for="to">To: </label>
									<input type="date" id="to" name="to" value="{{date("Y-m-d", strtotime($end))}}" max="{{date("Y-m-d", time()+19800)}}" class="form-control">
								</div>
							</div>
							<div class="form-group" style="margin-top: 25px;">
								<div class="col-sm-4">
									<input type="submit" name="submit" value="Submit" class="btn btn-success">
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>



		<div class="row">
			<div class="panel-group" id="accordion">
				<div class="panel">
					<span style="font-size: 20px;"><strong>Date-Wise Statistics</strong></span>
					<div class="panel-heading" style="background-color: #343a40; color: #fff">
						<div class="row">
							<div class="col-md-4"><b>Date</b></div>
							<div class="col-md-2"><b>Matchmakers Onbaorded</b></div>
							<div class="col-md-2"><b>Matchmakers added First Client</b></div>
							<div class="col-md-2"><b>Clients Added</b></div>
							<div class="col-md-2"><b>Clients Converted</b></div>
						</div>
					</div>
					@php
					$i = 1;
					@endphp
					@foreach($stats as $date_wise_data)
					<div class="panel">
						<div class="panel-heading">

							<div class="row">
								<div class="col-md-4"><b>{{date("Y-m-d", strtotime($date_wise_data['date']))}}</b></div>
								<div class="col-md-2 hover btn-link" title="Click to expand" data-toggle="collapse" data-parent="#accordion" data-target="#matchmaker{{$i}}"><b>{{$date_wise_data['matchmakers_onboarded']['count']}}</b></div>
								<div class="col-md-2 hover btn-link" title="Click to expand" data-toggle="collapse" data-parent="#accordion" data-target="#first_client{{$i}}"><b>{{$date_wise_data['matchmakers_first_client']['count']}}</b></div>
								<div class="col-md-2 hover btn-link" title="Click to expand" data-toggle="collapse" data-parent="#accordion" data-target="#clients_added{{$i}}"><b>{{$date_wise_data['clients_added']['count']}}</b></div>
								<div class="col-md-2 hover btn-link" title="Click to expand" data-toggle="collapse" data-parent="#accordion" data-target="#clients_converted{{$i}}"><b>{{$date_wise_data['clients_converted']['count']}}</b></div>
							</div>
						</div>
					</div>
					{{-- matchmakers onboarded --}}
					<div id="matchmaker{{$i}}" class="panel-collapse collapse">
						<div class="panel-body">
							<div class="row">
								<div class="col-md-offset-1 col-md-10">
									<table class="table table-striped table-bordered table-hover">
										<caption style="font-size: 20px;"><strong>List of Matchmakers Onboarded</strong></caption>
										<thead style="background-color: #343a40; color: #fff">
											<tr>
												<th>Name</th>
												<th>Gender</th>
												<th>Age (Yrs)</th>
												<th>Phone Number</th>
												<th>Is Approved ?</th>
												<th>Is Staff ?</th>
												<th>Follow-Up Date</th>
											</tr>
										</thead>
										<tbody>
											@foreach ($date_wise_data['matchmakers_onboarded']['data'] as $matchmaker)
											<tr>
												<td><b>{{ $matchmaker->first_name }} {{$matchmaker->last_name}}</b></td>
												<td>
													@if($matchmaker->gender == 0)
													Male
													@else
													Female
													@endif
												</td>
												<td>{{$matchmaker->age}}</td>
												<td id="phone_{{$matchmaker->id}}">{{$matchmaker->phone_number}}</td>
												<td>
													@if($matchmaker->is_active == 1)
													<span class="badge success">Approved</span>
													@else
													<span class="badge danger">Not Approved</span>
													@endif
												</td>
												<td>
													@if($matchmaker->is_staff == 1)
													<span class="badge success">Staff</span>
													@else
													<span class="badge danger">Not a staff</span>
													@endif
												</td>
												<td>{{$matchmaker->followup_date}}</td>
											</tr>
											@endforeach
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>

					{{-- matchmakers who added first client --}}
					<div id="first_client{{$i}}" class="panel-collapse collapse">
						<div class="panel-body">
							<div class="row">
								<div class="col-md-offset-1 col-md-10">
									<table class="table table-striped table-bordered table-hover">
										<caption style="font-size: 20px;"><strong>List of Matchmakers Who Added First Client</strong></caption>
										<thead style="background-color: #343a40; color: #fff">
											<tr>
												<th>Name</th>
												<th>Gender</th>
												<th>Age (Yrs)</th>
												<th>Phone Number</th>
												<th>Is Approved ?</th>
												<th>Is Staff ?</th>
												<th>Follow-Up Date</th>
											</tr>
										</thead>
										<tbody>
											@foreach ($date_wise_data['matchmakers_first_client']['data'] as $matchmaker)
											<tr>
												<td><b>{{ $matchmaker->first_name }} {{$matchmaker->last_name}}</b></td>
												<td>
													@if($matchmaker->gender == 0)
													Male
													@else
													Female
													@endif
												</td>
												<td>{{$matchmaker->age}}</td>
												<td id="phone_{{$matchmaker->id}}">{{$matchmaker->phone_number}}</td>
												<td>
													@if($matchmaker->is_active == 1)
													<span class="badge success">Approved</span>
													@else
													<span class="badge danger">Not Approved</span>
													@endif
												</td>
												<td>
													@if($matchmaker->is_staff == 1)
													<span class="badge success">Staff</span>
													@else
													<span class="badge danger">Not a staff</span>
													@endif
												</td>
												<td>{{$matchmaker->followup_date}}</td>
											</tr>
											@endforeach
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>

					{{-- clients added --}}
					<div id="clients_added{{$i}}" class="panel-collapse collapse">
						<div class="panel-body">
							<div class="row">
								<div class="col-md-offset-1 col-md-10">
									<table class="table table-striped table-bordered table-hover">
										<caption style="font-size: 20px;"><strong>List of Clients Added by Matchmakers</strong></caption>
										<thead style="background-color: #343a40; color: #fff">
											<tr>
												<th>Name</th>
												<th>Gender</th>
												<th>Age (Yrs)</th>
												<th>Client Ph. No.</th>
												<th>Weight (Kg)</th>
												<th>Height</th>
												<th>Income (In Lacs)</th>
												<th>Matchmaker</th>
											</tr>
										</thead>
										<tbody>
											@foreach ($date_wise_data['clients_added']['data'] as $client)
											<tr>
												<td><b>{{ $client->name }}</b></td>
												<td>
													@if($client->gender == 0)
													Male
													@else
													Female
													@endif
												</td>
												@php
												$age = date("Y", time()) - date("Y", strtotime($client->birth_date));
												@endphp
												<td>{{$age}}</td>
												<td>{{$client->phone_number}}</td>
												<td>{{$client->weight}}</td>
												<td>{{floor($client->height/12)}}'{{$client->height%12}}"</td>
												<td><i class="fa fa-inr" aria-hidden="true"></i>&nbsp;{{$client->yearly_income}}</td>
												<td><b>{{$client->first_name}} {{$client->last_name}}</b></td>
											</tr>
											@endforeach
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>

					{{-- clients who have converted to paid from free --}}
					<div id="clients_converted{{$i}}" class="panel-collapse collapse">
						<div class="panel-body">
							<div class="row">
								<div class="col-md-offset-1 col-md-10">
									<table class="table table-striped table-bordered table-hover">
										<caption style="font-size: 20px;"><strong>List of Clients Who have Opted For a Plan</strong></caption>
										<thead style="background-color: #343a40; color: #fff">
											<tr>
												<th>Name</th>
												<th>Gender</th>
												<th>Age (Yrs)</th>
												<th>Client Ph. No.</th>
												<th>Weight (Kg)</th>
												<th>Height</th>
												<th>Income (In Lacs)</th>
												<th>Payment</th>
												<th>Payment Status</th>
												<th>Paid On</th>
												<th>Matchmaker</th>
											</tr>
										</thead>
										<tbody>
											@foreach ($date_wise_data['clients_converted']['data'] as $client)
											<tr>
												<td><b>{{ $client->name }}</b></td>
												<td>
													@if($client->gender == 0)
													Male
													@else
													Female
													@endif
												</td>
												@php
												$age = date("Y", time()) - date("Y", strtotime($client->birth_date));
												@endphp
												<td>{{$age}}</td>
												<td>{{$client->phone_number}}</td>
												<td>{{$client->weight}}</td>
												<td>{{floor($client->height/12)}}'{{$client->height%12}}"</td>
												<td><i class="fa fa-inr" aria-hidden="true"></i>&nbsp;{{$client->yearly_income}}</td>
												<td><i class="fa fa-inr" aria-hidden="true"></i>&nbsp;{{$client->upfront_charge}}</td>
												<td>
													@if($client->payment_status == 0)
													<span class="badge danger">Not Verified</span>
													@else
													<span class="badge success">Verified</span>
													@endif
												</td>
												<td>{{date("Y-m-d h:i A", strtotime($client->paid_on))}}</td>
												<td><b>{{$client->first_name}} {{$client->last_name}}</b></td>
											</tr>
											@endforeach
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
					@php $i++; @endphp
					@endforeach
				</div>
			</div>
		</div>
	</div>
</div>


<style>
	body {
		overflow-x: hidden;
	}
	.success {
		background-color: #52b053;
	}

	.danger {
		background-color: #e8130e;
	}

	.items {
		font-size: 14px;
		font-weight: bold;
		padding-right: 30px !important;
		padding-left: 30px !important;
		padding-bottom: 30px !important;
		padding-top: 30px !important;
	}

	.hover:hover {
		cursor: pointer;
	}

	.sidebar-header {
		font-family: sans-serif;
		color: #fff !important;
		background-color: #000;
		padding-top: 10px;
		padding-bottom: 10px;
	}

	.navitems {
		margin-bottom: 5px;
		margin-top: 5px;
	}

	.navitems:hover {
		background-color: #000;
	}
	.navbar {
		height: -webkit-fill-available;
	}
	.navbar-static-top {
		height: 25px;
	}
</style>
@endsection
