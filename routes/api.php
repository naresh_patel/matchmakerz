<?php
header('Access-Control-Allow-Origin: *');
header( 'Access-Control-Allow-Headers: Authorization, Content-Type' );
header('Access-Control-Allow-Methods: POST, PUT, GET, DELETE, OPTIONS');
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//========================== Scripts API============================================//
Route::get('/matchmaker/once','DashboardController@once');

// ========================= Matchmaker's Controller Start=============================================================

Route::get('/matchmaker/customercare','MatchmakerController@phoneNumberBasedOnLevel');
Route::get('/matchmaker/numberCheck', 'MatchmakerController@numberCheck');
Route::get('/matchmaker/emailCheck', 'MatchmakerController@emailCheck');
Route::post('/matchmaker/register', 'MatchmakerController@createMatchmaker');
Route::get('/matchmaker/loginotp', 'MatchmakerController@loginOtp');
Route::post('/matchmaker/login', 'MatchmakerController@login');
Route::post('/matchmaker/truelogin','MatchmakerController@trueLogin');
Route::get('/matchmaker/referralcodecheck/', 'MatchmakerController@referralCodeCheck');
Route::get('/matchmaker/logout', 'MatchmakerController@logout');
Route::get('/matchmaker/profile', 'MatchmakerController@getProfile');
Route::post('/matchmaker/profile', 'MatchmakerController@updateProfile');
Route::get('/matchmaker/generateIdCard','MatchmakerController@generateIdCard');
Route::delete('/matchmaker/profile', 'MatchmakerController@deleteProfile');
Route::get('/matchmaker/totalclients', 'MatchmakerController@totalClients');
Route::get('/matchmaker/tutorials', 'MatchmakerController@tutorials');
Route::post('/matchmaker/updatelocation', 'MatchmakerController@updateLocation');
Route::post('/matchmaker/uploadProfilePic', 'MatchmakerController@uploadProfilePic');
Route::get('/matchmaker/plans_for_existing', 'MatchmakerController@getPlansForExisting');
Route::post('/matchmaker/plans_for_existing', 'MatchmakerController@createPlansForExisting');
Route::put('/matchmaker/plans_for_existing', 'MatchmakerController@updatePlansForExisting');
// for matchmaker_type == 0
Route::get('/matchmaker/plans', 'MatchmakerController@getPlans');
Route::post('/matchmaker/plans', 'MatchmakerController@createPlans');
Route::put('/matchmaker/plans', 'MatchmakerController@updatePlans');

// Route::get('/matchmaker/offers','MatchmakerController@getOffers');

Route::get('/matchmaker/accountStatus', 'MatchmakerController@checkSuspension');
Route::get('matchmaker/outstanding_amount', 'MatchmakerController@outstandingAmount');

Route::post('/matchmaker/addPayment', 'MatchmakerController@addPayment');
Route::post('/matchmaker/paymentconfirmation', 'MatchmakerController@paymentConfirmation');
Route::get('/matchmaker/buy-plan-for-existing-confirmation', 'MatchmakerController@buyPlanConfirmation');
Route::post('/matchmaker/client-payment-confirmation', 'MatchmakerController@clientPaymentConfirmation');
Route::get('/matchmaker/wallet', 'MatchmakerController@wallet');
Route::get('/matchmaker/addbonus','MatchmakerController@addbonus');
Route::post('/matchmaker/updateDeviceID', 'MatchmakerController@updateDeviceID');
Route::get('/matchmaker/myreferrals', 'MatchmakerController@myReferrals');
Route::get('/matchmaker/review_credits', 'MatchmakerController@reviewCredits');
Route::get('/matchmaker/data_user', 'MatchmakerController@userData');

//Matchmaker Blank Leads apis
Route::get('/matchmaker/get-blank-lead', 'MatchmakerController@getBlankLeads');
Route::delete('/matchmaker/get-blank-lead', 'MatchmakerController@deleteBlankLead');
Route::post('/matchmaker/get-blank-lead', 'MatchmakerController@createLead');
Route::get('/matchmaker/matchmaker-profile', 'MatchmakerController@matchmakerProfile');

Route::get('/matchmaker/get-my-leads', 'MatchmakerController@getMyLeads');
Route::post('/matchmaker/get-my-leads', 'MatchmakerController@createMyLead');

// routes for particular lead
Route::get('/matchmaker/get-my-lead/{lead_id}', 'MatchmakerController@getLead');
Route::delete('/matchmaker/get-my-lead/{lead_id}', 'MatchmakerController@deleteLead');
Route::post('/matchmaker/get-my-lead/{lead_id}', 'MatchmakerController@updateAppointmentInLead');
Route::put('/matchmaker/get-my-lead/{lead_id}', 'MatchmakerController@updateLead');

//Routes for incentives
Route::get('matchmaker/get-incentives', 'MatchmakerController@getIncentives');
Route::post('matchmaker/get-incentives', 'MatchmakerController@updateIncentive');
Route::get('/matchmaker/up', 'MatchmakerController@updatePlan');
Route::get('/matchmaker/get-fresher-matchmaker', 'MatchmakerController@freshMatchmaker');

Route::post('/matchmaker/offline-payment', 'MatchmakerController@offlinePayment');

Route::get('/matchmaker/get-chain', 'MatchmakerController@chain');
Route::get('/matchmaker/matchmaker-profile', 'MatchmakerController@matchmakerProfile');

Route::get('/matchmaker/distribute', 'MatchmakerController@blankLeadDistribute');
Route::get('/matchmaker/appversion', 'MatchmakerController@appUpdate');
Route::get('/matchmaker/newApp', 'MatchmakerController@newApp');

Route::get('/matchmaker/searchclient', 'MatchmakerController@searchClient');

Route::get('/matchmaker/bankdetails','MatchmakerController@getbankdetails');
Route::post('/matchmaker/bankdetails','MatchmakerController@bankdetails');
Route::post('matchmaker/updatebankdetails','MatchmakerController@updatebankdetails');
Route::get('/matchmaker/withdrawwallet','MatchmakerController@withdrawlrequest');

Route::get('/matchmaker/offers','MatchmakerController@getOffers');
Route::get('/matchmaker/tips','MatchmakerController@getTips');

// =====================Matchmaker's Controller ends ==================================================

//=======================Client Controller Starts ======================================================

Route::get('/client/castes', 'ClientController@getCastes');
Route::get('/client/degree', 'ClientController@getDegrees');
Route::get('/client/languages', 'ClientController@languages');
Route::post('/client/registerClient', 'ClientController@register');
Route::post('/client/visit', 'ClientController@visit');
Route::post('/client/updateclient', 'ClientController@updateClient');
Route::post('/client/client-social-update', 'ClientController@clientSocialUpdate');
Route::post('/client/client-career-update', 'ClientController@clientCareerUpdate');
Route::post('/client/client-family-update', 'ClientController@clientFamilyUpdate');

Route::get('/playstation/findmatches/', 'DashboardController@findMatches');

Route::get('/client/client-preferences', 'ClientController@getClientPreference');
Route::post('/client/deleteClient', 'ClientController@deleteClient');
Route::get('/client/emailCheck', 'ClientController@checkEmail');
Route::get('/client/phoneCheck', 'ClientController@checkPhone');
Route::get('/client/list', 'ClientController@clientList');
Route::get('/client/seen','ClientController@seenprofile');
Route::get('/client/profile', 'ClientController@clientProfile');

Route::post('/client/updateclientpref/', 'ClientController@updateClientPreference');
Route::put('/client/updateclientpref/', 'ClientController@createClientPreference');

Route::post('/client/uploadProfilePic', 'ClientController@uploadProfilePic');
Route::get('/client/filterMatches', 'ClientController@filterMatches');

Route::get('/client/showInterest', 'ClientController@getShowInterestData');
Route::post('/client/showInterest', 'ClientController@createShowInterest');

Route::get('/client/shortList', 'ClientController@getShortlistData');
Route::post('/client/shortList', 'ClientController@createShortlist');
Route::put('/client/shortList', 'ClientController@deleteShortlistData');

Route::get('/client/statusaccept-interest', 'ClientController@statusAcceptInterest');
Route::get('/client/statusdecline-interest', 'ClientController@statusDeclineInterest');
Route::get('/client/reconnect', 'ClientController@reconnectInterest');
Route::get('/client/incoming-interest', 'ClientController@incomingInterest');
Route::get('/client/awaited-interest', 'ClientController@awaitedInterest');
Route::get('/client/sendmessage','ClientController@sendMessage');
Route::get('/client/connected-interest', 'ClientController@connectedInterest');
Route::get('/client/declined-interest', 'ClientController@declinedInterest');
Route::get('/client/declined-interest-incoming', 'ClientController@declinedInterestIncoming');

Route::get('/client/matchmaker-contact', 'ClientController@matchmakerContact');

Route::get('/client/total-shortlist', 'ClientController@totalShortlist');
Route::get('/client/notifications', 'ClientController@notification');
Route::get('/client/notificationseen', 'ClientController@notificationSeen');
Route::get('/client/get-matchmaker', 'ClientController@getMatchmaker');

Route::get('/client/subscribe', 'ClientController@subscribe');
Route::get('/client/nearest-matchmaker', 'ClientController@nearestMatchmaker');
Route::post('/client/get-in-touch', 'ClientController@getInTouch');
Route::get('/client/assistantinterestedprofiles', 'ClientController@whatsappAssistantInterestedProfiles');
Route::get('/client/assistantrejectedprofiles', 'ClientController@whatsappAssistantRejectedProfiles');
Route::get('/client/assistantawaititngprofiles', 'ClientController@whatsappAssistantAwaitingProfiles');
Route::get('/client/sharedbyself', 'ClientController@profilesSharedBySelf');

Route::get('/client/shareprofiles', 'ClientController@shareOnWhatsapp');

// =========================== ClientController ends here ==============================================

// ==================================DashboardController starts here =======================================
// for matchmakers
Route::post('/playstation/login', 'DashboardController@login');
Route::get('/playstation/logout', 'DashboardController@logout');
Route::get('/playstation/profile', 'DashboardController@profile');
Route::get('/playstation/matchmakers', 'DashboardController@matchmakers');

Route::get('/playstation/matchmaker/', 'DashboardController@getMatchmaker');
Route::put('/playstation/matchmaker/', 'DashboardController@updateMatchmaker');
Route::post('/playstation/matchmaker/', 'DashboardController@createMatchmaker');
Route::delete('/playstation/matchmaker/', 'DashboardController@deleteMatchmaker');
Route::post('/playstation/Profile-matchmaker', 'DashboardController@uploadProfilePic');

//for clients
Route::post('/playstation/client-profilepic', 'DashboardController@uploadClientProfilePic');
Route::get('/playstation/clients', 'DashboardController@clients');

Route::get('playstation/client/', 'DashboardController@getClient');
Route::put('/playstation/client/', 'DashboardController@updateClient');
Route::post('/playstation/client/', 'DashboardController@createClient');
Route::delete('/playstation/client/', 'DashboardController@deleteClient');
Route::put('/playstation/socialupdate-client', 'DashboardController@clientSocialUpdate');
Route::put('/playstation/careerupdate-client', 'DashboardController@clientCareerUpdate');
Route::put('/playstation/familyupdate-client', 'DashboardController@clientFamilyUpdate');
Route::get('/playstation/client-preferences', 'DashboardController@getClientPreference');
Route::post('/playstation/client-preferences', 'DashboardController@updateClientPreference');

Route::get('/playstation/client-interactions/', 'DashboardController@getShowInterestData');
Route::put('/playstation/client-interactions/', 'DashboardController@updateInterest');
Route::post('/playstation/client-interactions/', 'DashboardController@createInterest');

Route::get('/playstation/client-shortlist/', 'DashboardController@getShortlistData');
Route::put('/playstation/client-shortlist/', 'DashboardController@updateShortist');
Route::post('/playstation/client-shortlist/', 'DashboardController@createShortlist');

Route::get('/playstation/plans/', 'DashboardController@plans');
Route::get('/playstation/single-plan', 'DashboardController@getPlan');
Route::put('/playstation/single-plan', 'DashboardController@updatePlan');
Route::post('/playstation/single-plan', 'DashboardController@createPlan');
Route::delete('/playstation/single-plan', 'DashboardController@deletePlan');

Route::get('/playstation/grouped-plans/', 'DashboardController@getGroupedPlans');
Route::put('/playstation/grouped-plans/', 'DashboardController@createPlanForMM');

Route::get('/playstation/tutorial-set', 'DashboardController@getTutorialSet');
Route::post('/playstation/tutorial-set', 'DashboardController@createTutorialSet');
Route::put('/playstation/tutorial-set', 'DashboardController@updateTutorialSet');
Route::delete('/playstation/tutorial-set', 'DashboardController@deleteTutorialSet');
Route::get('/playstation/tutorials', 'DashboardController@getAllTutorials');
Route::post('/playstation/tutorials', 'DashboardController@createTutorial');
Route::put('/playstation/tutorials', 'DashboardController@updateTutorial');
Route::delete('/playstation/tutorials', 'DashboardController@deleteTutorial');
Route::get('/playstation/single-tutorial/', 'DashboardController@singleTutorial');
Route::get('/playstation/payment', 'DashboardController@payment');
Route::get('/playstation/plan-for-existing', 'DashboardController@existingMatchmakersPlan');

Route::get('/playstation/wallet-amount', 'DashboardController@wallet');
Route::get('/playstation/single-macthmaker-plan', 'DashboardController@singleMatchmakerPlan');
Route::post('/playstation/getstats', 'DashboardController@getstats');

Route::get('/playstation/approvematchmaker', 'DashboardController@approveMatchmaker');
Route::get('/playstation/approveclient', 'DashboardController@approveClient');

Route::post('/playstation/sendapplink', 'DashboardController@sendAppLink');

Route::get('/playstation/get-blank-lead', 'DashboardController@getBlankLeads');
Route::get('/playstation/getmatchmakerleads', 'DashboardController@getMatchmakerLeads');

Route::get('/playstation/sharedbymatchmaker', 'DashboardController@sharedByMatchmaker');


// ======================= Whatsapp Webhook Controller ====================================
Route::post('/sendMessages', 'WebhookController@sendMessages');

// =======================Messenger Bot Controller ========================================
Route::get('/checkNumber', 'FbBotController@checkNumber');
Route::get('/credits', 'FbBotController@credits');
Route::get('/sendOtp', 'FbBotController@sendOtp');
Route::get('/reset', 'BotController@reset');
Route::post('/getUrl', 'BotController@getUrlWithToken');
Route::post('/getUrl', 'BotController@getUrl');
Route::get('/auth', 'BotController@checkRegistered');
Route::get('/language', 'BotController@changeLanguage');
