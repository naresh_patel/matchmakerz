<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Support\Facades\App;

Route::get('/', function () {
    return view('welcome');
});

Route::get('/overview', 'ManageMatchmakerController@overview')->name('overview');
Route::get('/matchmaker', 'ManageMatchmakerController@matchmakersOnboarded')->name('matchmakerz');
Route::get('/firstClient', 'ManageMatchmakerController@firstClient')->name('firstClient');
Route::get('/clientConverted', 'ManageMatchmakerController@clientConverted')->name('clientConverted');
Route::get('/services', 'ManageMatchmakerController@services')->name('services');
Route::post('/sendWhatsapp', 'ManageMatchmakerController@sendWhatsapp')->name('sendWhatsapp');
Route::get('/updateFollowUp', 'ManageMatchmakerController@updateFollowUp')->name('updateFollowUp');
Route::get('/updateActive','ManageMatchmakerController@updateActive')->name('updateActive');
Route::post('/assignTo', 'ManageMatchmakerController@assignTo')->name('assignTo');
Route::get('/withdrawl','ManageMatchmakerController@requestedWithdrawl')->name('withdrawl');
Route::get('/updatewallet','ManageMatchmakerController@updatewallet')->name('updatewallet');
Route::post('/hashcode','ManageMatchmakerController@hashcode')->name('hashcode');
Route::post('/imageupload','ManageMatchmakerController@imageupload')->name('imageupload');
Route::get('/shared','ManageMatchmakerController@profilesSharedBySelf')->name('shared');

// Route for Hiring Leads
Route::get('/hiring', 'HiringController@index')->name('hiring');
Route::get('/addhiringlead', function() {
    return view('hiring.addhiring');
})->name('addhiringlead');
Route::post('/addhiringlead', 'HiringController@store')->name('addhiringlead');
Route::get('/incompletehiringleads', 'HiringController@incomplete')->name('incompletehiringleads');
Route::post('/rejecthiringlead', 'HiringController@rejectLead')->name('rejecthiringlead');
Route::post('/rejectleadtoopen', 'HiringController@rejectToOpenLead')->name('rejectleadtoopen');
Route::post('/markhiringlead', 'HiringController@markLeadDone')->name('markhiringlead');
Route::post('/edithiringlead', 'HiringController@editLead')->name('edithiringlead');
Route::post('/updatehiringlead', 'HiringController@updateLead')->name('updatehiringlead');
Route::post('/deletehiringlead', 'HiringController@deleteLead')->name('deletehiringlead');
Route::post('/deleteIncompleteHiringLead', 'HiringController@deleteIncompleteLead')->name('deleteIncompleteHiringLead');
